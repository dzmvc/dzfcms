<?php
if(!file_exists('./config/config_global.php')){
    header('location: install/index.php');
}
if(isset($_SERVER["HTTP_ORIGIN"])) {
    //支持跨域获取COOKIE问题
    header('Access-Control-Allow-Origin:'.$_SERVER["HTTP_ORIGIN"]);
    header('Access-Control-Allow-Credentials:true');
    header('Access-Control-Allow-Methods:*');
    header('Access-Control-Allow-Headers:x-requested-with');
    header('Access-Control-Max-Age:2592000');
}else{
   header('Access-Control-Allow-Origin:*'); 
}
require_once './dz_framework/init.php';
//调试输出错误
if($_G['debug']==2){
    ini_set("display_errors","On");
    error_reporting(E_ALL);
}
//production 未设置1之前 简单校验增删改 开始
$check_do = array('add','edit','delete','index','import');
if(empty($_G['user_id']) && in_array($_G['gp_do'], $check_do)){
    $return = array(
        'code'=>'404',
        'msg'=>'您访问的页面不存在或无权限访问',
        'data'=>array(),
    );
    @header("Content-type: application/json");//头部输出
    echo json_encode($return);die;
}
//DEBUG 基于URL用户权限校验 暂放入口页面 具体根据实际业务逻辑使用
if($_G['config']['production']==1){
    ext::auth_check();
}
//DEBUG 访问量统计
stat_visit();
//production 未设置1之前 简单校验增删改 结束
//DEBUG 接收对象 动作
//$obj=isset($_REQUEST['obj']) ? $_REQUEST['obj']:'index'; //DEBUG 备用对象入口参数
$mod=isset($_REQUEST['mod']) ? $_REQUEST['mod']:'open'; //DEBUG 对应 source/module 下文件夹名
$action=isset($_REQUEST['action']) ? $_REQUEST['action']:'site'; //DEBUG 对应 source/module/{$mod}_{$action}.php 文件名
$do=isset($_REQUEST['do']) ? $_REQUEST['do']:'portal'; //DEBUG 对应 source/module/{$mod}_{$action}.php 文件内动作 (其他参数可在各入口模块内接收)
if($_G['gp_debug']==2 || $_G['config']['debug_log']==1){
   @file_put_contents(SITE_ROOT.'data/log/info_'.$mod.'_'.$action.'_'.$do.'.log.txt', date('Y-m-d h:i:s',TIMESTAMP).' : '. json_encode($_REQUEST).PHP_EOL,FILE_APPEND);
}
//DEBUG 调度处理 开始
define('MAD', $mod.'_'.$action.'_'.$do);
//DEBUG 加载数据处理驱动
$action_class_path = libfile($mod.'/'.$action, 'class','..');
if(file_exists($action_class_path)){
    require_once $action_class_path;
    //$action_class_name = $mod.'_'.$action;
    //$action_class = new $action_class_name();
}else{
    //TODO 不存在class文件的特殊文件处理
}
//DEBUG 加载业务逻辑驱动 并 处理请求
require_once libfile($mod.'/'.$action, 'module','..');
if($mod && $action){
    $mod_action_class = 'ctrl_'.$mod.'_'.$action;
    if(class_exists($mod_action_class)){
        $mod_action = new $mod_action_class();
        $ctrl_name='do_'.$do;
        $mod_action->$ctrl_name();
    }else{
        //DEBUG 调度处理 结束
        //require libfile($mod.'/'.$action, 'module','..');
    }
}