/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50156
Source Host           : localhost:3306
Source Database       : dzf2018

Target Server Type    : MYSQL
Target Server Version : 50156
File Encoding         : 65001

Date: 2018-05-06 11:47:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pre_common_menu
-- ----------------------------
DROP TABLE IF EXISTS `pre_common_menu`;
CREATE TABLE `pre_common_menu` (
  `menu_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '菜单编号',
  `menu_pid` int(10) DEFAULT '0' COMMENT '上级菜单编号',
  `menu_icon` varchar(255) DEFAULT '' COMMENT '菜单未选中图标',
  `menu_icon_current` varchar(255) DEFAULT '' COMMENT '菜单已选中图标',
  `position` varchar(64) DEFAULT '' COMMENT '路由代码',
  `sub_position` varchar(64) DEFAULT '' COMMENT '子路由代码',
  `name_var` varchar(64) DEFAULT '' COMMENT '菜单名称',
  `url` varchar(255) DEFAULT '' COMMENT '全路由路径',
  `sort` tinyint(4) DEFAULT '0' COMMENT '菜单排序',
  `self_style` varchar(64) DEFAULT '' COMMENT '菜单样式',
  `enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `cm_mod` varchar(32) DEFAULT '' COMMENT '模块名称',
  `cm_action` varchar(32) DEFAULT '' COMMENT '模块功能',
  `cm_do` varchar(32) DEFAULT '' COMMENT '模块动作',
  `related_menu_url` varchar(500) DEFAULT '' COMMENT '关联菜单',
  `menu_isopen` tinyint(1) DEFAULT '0' COMMENT '是否开放访问',
  `menu_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示菜单',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`menu_id`),
  KEY `position` (`position`),
  KEY `sub_position` (`sub_position`),
  KEY `name_var` (`name_var`),
  KEY `enable` (`enable`,`isdelete`),
  KEY `sort` (`sort`),
  KEY `cm_mod` (`cm_mod`),
  KEY `cm_action` (`cm_action`),
  KEY `cm_do` (`cm_do`),
  KEY `menu_isopen` (`menu_isopen`),
  KEY `menu_display` (`menu_display`)
) ENGINE=InnoDB AUTO_INCREMENT=1641 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_common_setting
-- ----------------------------
DROP TABLE IF EXISTS `pre_common_setting`;
CREATE TABLE `pre_common_setting` (
  `cst_id` int(10) NOT NULL AUTO_INCREMENT,
  `skey` varchar(255) NOT NULL DEFAULT '' COMMENT '设置参数英文名称',
  `skey_name` varchar(255) DEFAULT NULL COMMENT '设置参数中文名称',
  `svalue` text COMMENT '设置参数值',
  `skey_discription` varchar(255) DEFAULT NULL COMMENT '设置参数说明描述',
  `modify_dateline` varchar(32) DEFAULT NULL,
  `create_dateline` varchar(32) DEFAULT NULL,
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  PRIMARY KEY (`cst_id`),
  UNIQUE KEY `skey` (`skey`),
  KEY `skey_name` (`skey_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_common_syscache
-- ----------------------------
DROP TABLE IF EXISTS `pre_common_syscache`;
CREATE TABLE `pre_common_syscache` (
  `csc_id` int(10) NOT NULL AUTO_INCREMENT,
  `cname` varchar(32) NOT NULL,
  `cname_description` varchar(255) DEFAULT NULL COMMENT '缓存描述',
  `ctype` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '缓存类型 1 数据库 2 文件',
  `dateline` int(10) unsigned NOT NULL COMMENT '缓存更新时间',
  `data` longtext NOT NULL COMMENT '缓存数据',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  PRIMARY KEY (`csc_id`),
  UNIQUE KEY `cname` (`cname`)
) ENGINE=MyISAM AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_index_index
-- ----------------------------
DROP TABLE IF EXISTS `pre_index_index`;
CREATE TABLE `pre_index_index` (
  `ii_id` int(10) NOT NULL AUTO_INCREMENT,
  `ii_name` varchar(64) DEFAULT '' COMMENT '数据模块名称',
  `ii_mod_setting` text COMMENT '首页模块参数设置',
  `ii_mod_data` text COMMENT '数据缓存',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`ii_id`),
  KEY `ii_name` (`ii_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_server_node
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_node`;
CREATE TABLE `pre_server_node` (
  `sn_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '服务器节点编号',
  `sn_ipv4_intranet` char(15) NOT NULL COMMENT '服务器内部IP',
  `sn_ipv4_chinanet` char(15) DEFAULT NULL COMMENT '电信服务器节点IPV4',
  `sn_ipv4_unicom` char(15) DEFAULT NULL COMMENT '联通服务器节点IPV4',
  `sn_ipv4_mobile` char(15) DEFAULT NULL COMMENT '联通服务器节点IPV4',
  `sn_type` tinyint(4) DEFAULT '1' COMMENT '服务器节点类型, 0 未选择分类服务器节点  1 学生直播节点 2 主讲直播节点  3 socket节点 ',
  `sn_group_id` int(11) DEFAULT '0' COMMENT '服务器默认组',
  `sn_connections_max` smallint(2) unsigned DEFAULT '0' COMMENT '节点最大连接数',
  `sn_connections_used` smallint(2) unsigned DEFAULT '0',
  `sn_connections_left` smallint(2) unsigned DEFAULT '0' COMMENT '节点最大连接数',
  `create_dateline` int(10) DEFAULT NULL,
  `modify_dateline` int(10) DEFAULT NULL,
  `isdelete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`sn_id`),
  KEY `sn_ipv4_intranet` (`sn_ipv4_intranet`),
  KEY `sn_ipv4_intranet_sn_type` (`sn_ipv4_intranet`,`sn_type`),
  KEY `sn_type` (`sn_type`),
  KEY `sn_connections_left_sn_type_sn_group_id` (`sn_connections_left`,`sn_type`,`sn_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_server_node_cate
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_node_cate`;
CREATE TABLE `pre_server_node_cate` (
  `snc_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `snc_name` varchar(64) DEFAULT NULL COMMENT '服务器分类名称',
  `create_dateline` int(10) DEFAULT NULL,
  `modify_dateline` int(10) DEFAULT NULL,
  `isdelete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`snc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_server_node_connect_log
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_node_connect_log`;
CREATE TABLE `pre_server_node_connect_log` (
  `snc_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_ip` char(16) DEFAULT '0' COMMENT '节点IP',
  `node_connect` smallint(6) DEFAULT '0' COMMENT '节点连接数',
  `create_dateline` int(10) DEFAULT '0' COMMENT '数据生成时间',
  `create_dateline_format` varchar(20) DEFAULT '0' COMMENT '数据生成时间格式化',
  `create_dateline_day` int(10) DEFAULT '0' COMMENT '数据生成日期',
  `node_merge` smallint(6) DEFAULT '1' COMMENT '是否是合并多线路连接数数据 1 非合并数据  2 合并后的数据',
  PRIMARY KEY (`snc_id`),
  KEY `node_ip` (`node_ip`),
  KEY `create_dateline` (`create_dateline`),
  KEY `create_dateline_day` (`create_dateline_day`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_server_node_load
-- ----------------------------
DROP TABLE IF EXISTS `pre_server_node_load`;
CREATE TABLE `pre_server_node_load` (
  `snl_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '记录编号',
  `snl_sn_id` int(10) DEFAULT '0' COMMENT '服务器编号',
  `snl_course_hour_id` int(11) DEFAULT '0' COMMENT '课时编号',
  `snl_load` smallint(2) DEFAULT '0' COMMENT '课时占用节点服务器连接数',
  `sn_release_time` int(10) DEFAULT '0' COMMENT '服务器连接数占用释放时间(即被占用的截止时间)',
  PRIMARY KEY (`snl_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_tools_module
-- ----------------------------
DROP TABLE IF EXISTS `pre_tools_module`;
CREATE TABLE `pre_tools_module` (
  `tm_id` int(10) NOT NULL AUTO_INCREMENT,
  `tm_name` varchar(128) DEFAULT '' COMMENT '名称',
  `tm_description` varchar(255) DEFAULT '' COMMENT '描述',
  `tm_position` tinyint(1) DEFAULT '1' COMMENT '所属位置',
  `tm_menu_pid` int(10) DEFAULT '5' COMMENT '上级菜单',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`tm_id`),
  KEY `tm_name` (`tm_name`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_tools_module_table
-- ----------------------------
DROP TABLE IF EXISTS `pre_tools_module_table`;
CREATE TABLE `pre_tools_module_table` (
  `tmt_id` int(10) NOT NULL AUTO_INCREMENT,
  `tm_id` int(10) DEFAULT '0' COMMENT '所属模块',
  `tmt_table_name` varchar(64) DEFAULT '' COMMENT '表名称',
  `tmt_name` varchar(64) DEFAULT '' COMMENT '备注',
  `tmt_description` tinytext COMMENT '描述',
  `tmt_allowdelete` tinyint(1) DEFAULT '1' COMMENT '是否允许删除',
  `tmt_allowadd` tinyint(1) DEFAULT '1' COMMENT '是否允许添加',
  `tmt_allowedit` tinyint(1) DEFAULT '1' COMMENT '是否允许修改',
  `tmt_allowexport` tinyint(1) DEFAULT '1' COMMENT '允许导出',
  `tmt_allowimport` tinyint(1) DEFAULT '1' COMMENT '允许导入',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `tmt_api_index_desc` text COMMENT '列表接口说明',
  `tmt_api_add_desc` text COMMENT '添加接口说明',
  `tmt_api_edit_desc` text COMMENT '编辑接口说明',
  `tmt_api_delete_desc` text COMMENT '删除接口说明',
  `tmt_api_detail_desc` text COMMENT '详情接口说明',
  `tmt_api_other_desc` text COMMENT '其他接口说明',
  PRIMARY KEY (`tmt_id`),
  KEY `tmt_table_name` (`tmt_table_name`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_tools_module_field
-- ----------------------------
DROP TABLE IF EXISTS `pre_tools_module_field`;
CREATE TABLE `pre_tools_module_field` (
  `tmf_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '字段编号',
  `tm_id` int(10) NOT NULL COMMENT '字段所属模块',
  `tmt_id` int(10) NOT NULL DEFAULT '0' COMMENT '字段所属表数据表',
  `tmf_field_name` varchar(64) DEFAULT NULL COMMENT '字段名称',
  `tmf_filed_key` tinyint(1) DEFAULT '0' COMMENT '是否主键 1 表示自增主键 0 表示一般数据字段',
  `tmf_filed_search` tinyint(1) DEFAULT '0' COMMENT '是否关键字搜索字段 1 表示是 0 表示不是',
  `tmf_filed_search_ext` tinyint(1) DEFAULT '0' COMMENT '是否扩展搜索字段 1 表示是 0 表示不是',
  `tmf_filed_search_ext_type` tinyint(1) DEFAULT '0' COMMENT '是否扩展搜索字段 1 表示全匹配搜索 0 表示模糊搜索',
  `tmf_field_type` varchar(32) DEFAULT 'varchar' COMMENT 'MYSQL 字段类型 tinyint,smallint等',
  `tmf_field_length` varchar(16) DEFAULT '0' COMMENT '字段长度',
  `tmf_field_default` varchar(64) DEFAULT '' COMMENT '字段默认值',
  `tmf_field_list` tinyint(1) DEFAULT '1' COMMENT '字段是否在列表中显示 0 表示不显示 1表示显示',
  `tmf_name` varchar(64) DEFAULT NULL COMMENT '字段名称注释',
  `tmf_type` varchar(64) DEFAULT 'input' COMMENT '字段展示方式 1 input 文本框 2 textarea 文本编辑框 3 select 下拉框 4 radio 单选框 4 checkbox 多选框 5 upoadfile 上传文件 6 uploadimg 上传图片 7 tree 树形元素',
  `tmf_description` text COMMENT '字段描述',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `tmf_filed_index` tinyint(1) DEFAULT '0' COMMENT '是否字段加索引字段 1 表示是 0 表示不是',
  `tmf_type_value` text COMMENT '选项字段值',
  `tmf_require` tinyint(1) DEFAULT '0' COMMENT '是否必填',
  `tmf_sort` int(10) DEFAULT '0' COMMENT '字段列表 和 form 显示排序 1234 小数优先',
  `tmf_form_hidden` tinyint(1) DEFAULT '0' COMMENT '字段是否在编辑修改表单隐藏 0显示 1隐藏',
  `foreign_link` varchar(200) DEFAULT NULL COMMENT '外键链接',
  `tmf_readonly` tinyint(1) DEFAULT '0' COMMENT '是否只读 0 否 1是',
  `tmf_disabled` tinyint(1) DEFAULT '0' COMMENT '禁止编辑 0 否 1是',
  `tmf_display` tinyint(1) DEFAULT '0' COMMENT '是否隐藏不显示 0 否 1是',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `tmf_type_ext` varchar(255) DEFAULT '' COMMENT '展示参数',
  `tmf_type_group` varchar(64) DEFAULT '' COMMENT '展示分组',
  PRIMARY KEY (`tmf_id`)
) ENGINE=MyISAM AUTO_INCREMENT=580 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_tools_module_apis
-- ----------------------------
DROP TABLE IF EXISTS `pre_tools_module_apis`;
CREATE TABLE `pre_tools_module_apis` (
  `tma_id` int(10) NOT NULL DEFAULT '0' COMMENT '#',
  `tma_name` varchar(128) DEFAULT '' COMMENT '接口项目名称',
  `tma_master` varchar(64) DEFAULT '' COMMENT '接口负责人',
  `tma_brief` varchar(255) DEFAULT '' COMMENT '接口模块描述',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`tma_id`),
  KEY `tma_master` (`tma_master`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='接口文档管理';

-- ----------------------------
-- Table structure for pre_user
-- ----------------------------
DROP TABLE IF EXISTS `pre_user`;
CREATE TABLE `pre_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) DEFAULT '',
  `user_realname` varchar(32) DEFAULT '',
  `user_password` varchar(64) DEFAULT '',
  `user_group_id` mediumint(8) DEFAULT '2',
  `user_role_id` mediumint(8) DEFAULT '2',
  `user_level_id` mediumint(8) DEFAULT '1' COMMENT '1 基层用户 其他为上层用户 基层用户数据列表where会有user_id 限制',
  `user_detail_id` mediumint(8) DEFAULT '0',
  `user_score` int(10) DEFAULT '0' COMMENT '用户积分',
  `user_phone` varchar(32) DEFAULT '',
  `user_cardno` varchar(32) DEFAULT '' COMMENT '身份证',
  `phone_validation` int(1) DEFAULT '0' COMMENT '0手机未验证，1已经验证',
  `parent_user_id` int(10) DEFAULT '0',
  `user_nickname` varchar(32) DEFAULT '' COMMENT '用户昵称',
  `user_avatar` varchar(255) DEFAULT '' COMMENT '头像地址',
  `user_email` varchar(64) DEFAULT '' COMMENT '用户邮箱',
  `email_validation` tinyint(1) DEFAULT '0' COMMENT '邮箱验证',
  `email_token` varchar(50) DEFAULT '' COMMENT '帐号激活码',
  `email_token_exptime` int(10) DEFAULT '0' COMMENT '激活码有效期',
  `ischeck` tinyint(1) DEFAULT '1' COMMENT '用户是否审核 1 已审核 2 未审核',
  `address` varchar(100) DEFAULT '' COMMENT '家庭地址',
  `gender` int(1) DEFAULT '0' COMMENT '1男2女',
  `about` text COMMENT '自我介绍',
  `qq` varchar(15) DEFAULT '' COMMENT 'QQ',
  `openid` varchar(50) DEFAULT '' COMMENT '绑定微信ID号',
  `user_birthday` varchar(12) DEFAULT '0000-00-00' COMMENT '用户生日',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `user_name` (`user_name`),
  KEY `user_realname` (`user_realname`),
  KEY `user_role_id` (`user_role_id`),
  KEY `user_group_id` (`user_group_id`),
  KEY `user_level_id` (`user_level_id`),
  KEY `user_score` (`user_score`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_user
-- ----------------------------
DROP TABLE IF EXISTS `pre_user`;
CREATE TABLE `pre_user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(64) NOT NULL,
  `user_realname` varchar(32) DEFAULT '',
  `user_password` varchar(64) DEFAULT '',
  `user_group_id` mediumint(8) DEFAULT '2',
  `user_role_id` mediumint(8) DEFAULT '7' COMMENT '用户角色',
  `user_level_id` mediumint(8) DEFAULT '1' COMMENT '1 基层用户 其他为上层用户 基层用户数据列表where会有user_id 限制',
  `user_detail_id` mediumint(8) DEFAULT '0',
  `user_score` int(10) DEFAULT '0' COMMENT '用户积分',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '区域编号',
  `user_phone` varchar(32) DEFAULT '',
  `user_cardno` varchar(32) DEFAULT '' COMMENT '身份证',
  `phone_validation` int(1) DEFAULT '0' COMMENT '0手机未验证，1已经验证',
  `parent_user_id` int(10) DEFAULT '0',
  `user_nickname` varchar(32) DEFAULT '' COMMENT '用户昵称',
  `user_avatar` varchar(255) DEFAULT '' COMMENT '头像地址',
  `user_email` varchar(64) DEFAULT '' COMMENT '用户邮箱',
  `email_validation` tinyint(1) DEFAULT '0' COMMENT '邮箱验证',
  `email_token` varchar(50) DEFAULT '' COMMENT '帐号激活码',
  `email_token_exptime` int(10) DEFAULT '0' COMMENT '激活码有效期',
  `ischeck` tinyint(1) DEFAULT '1' COMMENT '用户是否审核 1 已审核 2 未审核',
  `address` varchar(100) DEFAULT '' COMMENT '家庭地址',
  `gender` int(1) DEFAULT NULL COMMENT '1男2女',
  `about` text COMMENT '自我介绍',
  `qq` varchar(15) DEFAULT '' COMMENT 'QQ',
  `openid` varchar(50) DEFAULT '' COMMENT '绑定微信ID号',
  `user_birthday` varchar(12) DEFAULT '0000-00-00' COMMENT '用户生日',
  `webchatopenid` varchar(50) DEFAULT '' COMMENT '绑定微信ID号',
  `user_level` smallint(3) DEFAULT '1' COMMENT '普通用户1, 2VIP用户，3企业VIP用户',
  `user_abalance` decimal(16,2) DEFAULT '0.00' COMMENT '账户余额',
  `user_dcash` decimal(16,2) DEFAULT '0.00' COMMENT '可提现金额',
  `user_dqsj` varchar(64) DEFAULT '' COMMENT '会员到期时间',
  `user_alipay_account` varchar(255) DEFAULT '' COMMENT '支付宝账户',
  PRIMARY KEY (`user_id`),
  KEY `user_name` (`user_name`) USING BTREE,
  KEY `user_realname` (`user_realname`) USING BTREE,
  KEY `user_role_id` (`user_role_id`) USING BTREE,
  KEY `user_group_id` (`user_group_id`) USING BTREE,
  KEY `user_level_id` (`user_level_id`) USING BTREE,
  KEY `user_score` (`user_score`) USING BTREE,
  KEY `webchatopenid` (`webchatopenid`)
) ENGINE=MyISAM AUTO_INCREMENT=322 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_user_access
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_access`;
CREATE TABLE `pre_user_access` (
  `access_id` int(12) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `access_mod` varchar(32) DEFAULT NULL,
  `access_action` varchar(32) DEFAULT NULL,
  `access_do` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_user_group
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_group`;
CREATE TABLE `pre_user_group` (
  `group_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `group_name` varbinary(64) DEFAULT NULL,
  `group_description` text,
  `group_role` int(11) DEFAULT NULL,
  `group_create_time` varchar(32) DEFAULT NULL,
  `group_modify_time` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_user_level
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_level`;
CREATE TABLE `pre_user_level` (
  `level_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `level_name` varchar(64) DEFAULT NULL,
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_user_role
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_role`;
CREATE TABLE `pre_user_role` (
  `role_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(64) DEFAULT '' COMMENT '类型名称',
  `role_desc` varchar(255) DEFAULT '' COMMENT '类型描述',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`role_id`),
  KEY `role_name` (`role_name`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='用户类型';

-- ----------------------------
-- Table structure for pre_user_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_role_menu`;
CREATE TABLE `pre_user_role_menu` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) DEFAULT '0' COMMENT '菜单唯一编号',
  `role_id` tinyint(4) DEFAULT '0' COMMENT '角色菜单对应的角色id',
  `user_id` int(10) DEFAULT '0' COMMENT '用户菜单对应的用户uid,若登陆用户的用户菜单为空则取该用户角色菜单作为用户登录后的初始化菜单,无角色用户不允许登陆,uid=0为游客菜单',
  PRIMARY KEY (`id`),
  KEY `menu_id` (`menu_id`),
  KEY `role_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10615 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_index
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_index`;
CREATE TABLE `pre_sns_index` (
  `si_id` int(10) NOT NULL AUTO_INCREMENT,
  `si_english_name` varchar(128) DEFAULT '0' COMMENT '板块英文名',
  `si_chinese_name` varchar(255) DEFAULT '' COMMENT '板块中文名',
  `si_position` smallint(3) DEFAULT '0' COMMENT '显示位置',
  `si_content` text COMMENT '板块缓存',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`si_id`),
  KEY `si_chinese_name` (`si_chinese_name`),
  KEY `si_english_name` (`si_english_name`),
  KEY `si_position` (`si_position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info`;
CREATE TABLE `pre_sns_info` (
  `si_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sic_id` int(10) DEFAULT '0' COMMENT '信息分类编号',
  `si_name` varchar(255) DEFAULT '' COMMENT '信息名称',
  `si_content` longtext COMMENT '信息内容',
  `si_img` varchar(128) DEFAULT '' COMMENT '信息封面',
  `si_slider` smallint(3) DEFAULT '0' COMMENT '是否轮播',
  `si_type` smallint(3) DEFAULT '1' COMMENT '信息类别',
  `si_sort` smallint(3) DEFAULT '999' COMMENT '信息排序',
  `si_best` smallint(3) DEFAULT '0' COMMENT '是否精华',
  `si_top` smallint(3) DEFAULT '0' COMMENT '是否置顶',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `user_id` int(10) DEFAULT '0' COMMENT '发布用户',
  `si_commend` tinyint(3) DEFAULT '0' COMMENT '是否推荐 0否 1是',
  PRIMARY KEY (`si_id`),
  KEY `si_name` (`si_name`),
  KEY `sic_id` (`sic_id`),
  KEY `si_slider` (`si_slider`),
  KEY `si_type` (`si_type`),
  KEY `si_sort` (`si_sort`),
  KEY `si_best` (`si_best`),
  KEY `si_top` (`si_top`),
  KEY `si_commend` (`si_commend`)
) ENGINE=MyISAM AUTO_INCREMENT=1936 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info_cate
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info_cate`;
CREATE TABLE `pre_sns_info_cate` (
  `sic_id` int(10) NOT NULL AUTO_INCREMENT,
  `sic_pid` int(10) DEFAULT '0' COMMENT '分类上级编号',
  `sic_name` varchar(128) DEFAULT '' COMMENT '分类名称',
  `sic_description` varchar(255) DEFAULT '' COMMENT '分类描述',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`sic_id`),
  KEY `sic_name` (`sic_name`),
  KEY `sic_pid` (`sic_pid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info_comment
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info_comment`;
CREATE TABLE `pre_sns_info_comment` (
  `sic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sic_content` text COMMENT '评论内容',
  `si_id` int(10) DEFAULT '0' COMMENT '信息编号',
  `user_id` int(10) DEFAULT '0' COMMENT '发布人',
  `sic_accept` smallint(3) DEFAULT '0' COMMENT '是否采纳',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`sic_id`),
  KEY `si_id` (`si_id`),
  KEY `user_id` (`user_id`),
  KEY `sic_accept` (`sic_accept`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for pre_sns_index
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_index`;
CREATE TABLE `pre_sns_index` (
  `si_id` int(10) NOT NULL AUTO_INCREMENT,
  `si_english_name` varchar(128) DEFAULT '0' COMMENT '板块英文名',
  `si_chinese_name` varchar(255) DEFAULT '' COMMENT '板块中文名',
  `si_position` smallint(3) DEFAULT '0' COMMENT '显示位置',
  `si_content` text COMMENT '板块缓存',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`si_id`),
  KEY `si_chinese_name` (`si_chinese_name`),
  KEY `si_english_name` (`si_english_name`),
  KEY `si_position` (`si_position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info`;
CREATE TABLE `pre_sns_info` (
  `si_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sic_id` int(10) DEFAULT '0' COMMENT '信息分类编号',
  `si_name` varchar(255) DEFAULT '' COMMENT '信息名称',
  `si_content` longtext COMMENT '信息内容',
  `si_img` varchar(128) DEFAULT '' COMMENT '信息封面',
  `si_slider` smallint(3) DEFAULT '0' COMMENT '是否轮播',
  `si_type` smallint(3) DEFAULT '1' COMMENT '信息类别',
  `si_sort` smallint(3) DEFAULT '999' COMMENT '信息排序',
  `si_best` smallint(3) DEFAULT '0' COMMENT '是否精华',
  `si_top` smallint(3) DEFAULT '0' COMMENT '是否置顶',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `user_id` int(10) DEFAULT '0' COMMENT '发布用户',
  `si_commend` tinyint(3) DEFAULT '0' COMMENT '是否推荐 0否 1是',
  PRIMARY KEY (`si_id`),
  KEY `si_name` (`si_name`),
  KEY `sic_id` (`sic_id`),
  KEY `si_slider` (`si_slider`),
  KEY `si_type` (`si_type`),
  KEY `si_sort` (`si_sort`),
  KEY `si_best` (`si_best`),
  KEY `si_top` (`si_top`),
  KEY `si_commend` (`si_commend`)
) ENGINE=MyISAM AUTO_INCREMENT=1936 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info_cate
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info_cate`;
CREATE TABLE `pre_sns_info_cate` (
  `sic_id` int(10) NOT NULL AUTO_INCREMENT,
  `sic_pid` int(10) DEFAULT '0' COMMENT '分类上级编号',
  `sic_name` varchar(128) DEFAULT '' COMMENT '分类名称',
  `sic_description` varchar(255) DEFAULT '' COMMENT '分类描述',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`sic_id`),
  KEY `sic_name` (`sic_name`),
  KEY `sic_pid` (`sic_pid`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_sns_info_comment
-- ----------------------------
DROP TABLE IF EXISTS `pre_sns_info_comment`;
CREATE TABLE `pre_sns_info_comment` (
  `sic_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sic_content` text COMMENT '评论内容',
  `si_id` int(10) DEFAULT '0' COMMENT '信息编号',
  `user_id` int(10) DEFAULT '0' COMMENT '发布人',
  `sic_accept` smallint(3) DEFAULT '0' COMMENT '是否采纳',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  PRIMARY KEY (`sic_id`),
  KEY `si_id` (`si_id`),
  KEY `user_id` (`user_id`),
  KEY `sic_accept` (`sic_accept`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_ad_main
-- 广告管理 系统默认模块
-- ----------------------------
DROP TABLE IF EXISTS `pre_ad_main`;
CREATE TABLE `pre_ad_main` (
  `am_id` int(11) NOT NULL AUTO_INCREMENT,
  `am_name` varchar(255) DEFAULT '' COMMENT '广告标题',
  `am_img` varchar(255) DEFAULT '' COMMENT '广告图片',
  `am_status` smallint(3) DEFAULT '0' COMMENT '是否启用',
  `am_position` smallint(3) DEFAULT '0' COMMENT '广告位置',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`am_id`),
  KEY `am_name` (`am_name`),
  KEY `am_status` (`am_status`),
  KEY `am_position` (`am_position`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='广告管理';

-- ----------------------------
-- Table structure for pre_stat_sqlslow
-- 统计中心 - 慢查询分析
-- ----------------------------
DROP TABLE IF EXISTS `pre_stat_sqlslow`;
CREATE TABLE `pre_stat_sqlslow` (
    `sss_ld` BIGINT(20) NOT NULL AUTO_INCREMENT,
    `sss_date` VARCHAR(32)  DEFAULT '' COMMENT '执行日期',
    `sss_time` VARCHAR(32)  DEFAULT '' COMMENT '执行时间',
    `sss_query_time` VARCHAR(32)  DEFAULT '' COMMENT '执行消耗时间',
    `sss_lock_time` VARCHAR(32)  DEFAULT '' COMMENT '执行锁时间',
    `sss_rows_sent` INT(10)  DEFAULT 0 COMMENT '返回行数',
    `sss_rows_examined` INT(10)  DEFAULT 0 COMMENT '读取行数',
    `sss_short` VARCHAR(255)  DEFAULT '' COMMENT '查询语句前缀',
    `sss_content` TEXT COMMENT 'SQL语句内容',
    `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
    `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
    `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
    `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
    `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
    `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
    `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
    `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
    `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
    `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
    `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
    PRIMARY KEY (`sss_ld`),
    KEY `sss_date` (`sss_date`),
    KEY `sss_time` (`sss_time`),
    KEY `sss_query_time` (`sss_query_time`),
    KEY `sss_lock_time` (`sss_lock_time`),
    KEY `sss_rows_sent` (`sss_rows_sent`),
    KEY `sss_rows_examined` (`sss_rows_examined`),
    KEY `sss_short` (`sss_short`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='慢查询分析';

 ALTER TABLE `pre_stat_sqlslow` COMMENT='慢查询分析';

-- ----------------------------
-- Table structure for pre_user_failedlogin
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_failedlogin`;
CREATE TABLE `pre_user_failedlogin` (
  `ufl_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ufl_ip` varchar(32) DEFAULT '' COMMENT '访问者IP',
  `ufl_user_name` varchar(32) DEFAULT '' COMMENT '登录用户名',
  `ufl_count` smallint(3) DEFAULT '0' COMMENT '错误次数',
  `ufl_lastupdate` varchar(32) DEFAULT '' COMMENT '最后登录时间',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`ufl_id`),
  KEY `ufl_user_name` (`ufl_user_name`),
  KEY `ufl_ip` (`ufl_ip`),
  KEY `ufl_count` (`ufl_count`),
  KEY `ufl_lastupdate` (`ufl_lastupdate`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户登录失败';


-- ----------------------------
-- Table structure for pre_tools_module_git
-- 代码仓库自动更新设置表
-- ----------------------------
DROP TABLE IF EXISTS `pre_tools_module_git`;
CREATE TABLE `pre_tools_module_git` (
  `tmg_id` int(10) NOT NULL AUTO_INCREMENT,
  `tmg_name` varchar(64) DEFAULT '' COMMENT '仓库中文名称',
  `tmg_url` varchar(128) DEFAULT '' COMMENT '仓库网址',
  `tmg_path` varchar(128) DEFAULT '' COMMENT '更新目录',
  `tmg_branch` varchar(64) DEFAULT '' COMMENT '更新分支',
  `tmg_code` varchar(64) DEFAULT '' COMMENT '仓库英文名称',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`tmg_id`),
  KEY `tmg_name` (`tmg_name`),
  KEY `tmg_branch` (`tmg_branch`),
  KEY `tmg_code` (`tmg_code`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='项目代码管理';

-- ----------------------------
-- Table structure for pre_common_sql
-- 软件版本对应数据库更新SQL数据表
-- ----------------------------
DROP TABLE IF EXISTS `pre_common_sql`;
CREATE TABLE `pre_common_sql` (
  `cs_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `cs_name` varchar(128) DEFAULT '' COMMENT '版本号',
  `cs_db` varchar(64) NOT NULL DEFAULT '' COMMENT '数据库名',
  `cs_sqlstr` text COMMENT 'SQL语句',
  `cs_other` varchar(128) DEFAULT '' COMMENT '备注',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`cs_id`),
  KEY `cs_db` (`cs_db`)
) ENGINE=MyISAM AUTO_INCREMENT=10109 DEFAULT CHARSET=utf8 COMMENT='执行SQL';

-- ----------------------------
-- Table structure for pre_component_ticket
-- ----------------------------
DROP TABLE IF EXISTS `pre_component_ticket`;
CREATE TABLE `pre_component_ticket` (
  `ct_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '#',
  `ct_type` smallint(2) DEFAULT '1' COMMENT '问题类型',
  `ct_client` smallint(2) DEFAULT '0' COMMENT '问题终端',
  `ct_title` varchar(255) DEFAULT '' COMMENT '问题描述',
  `ct_files` text COMMENT '问题附件',
  `ct_status` smallint(2) DEFAULT '0' COMMENT '处理状态',
  `ct_star` smallint(2) DEFAULT '0' COMMENT '评价星级',
  `ct_star_content` varchar(128) DEFAULT '' COMMENT '评价说明',
  `ct_master` varchar(32) NOT NULL DEFAULT 'bzkf10' COMMENT '工单负责人',
  `ct_zycd` varchar(32) DEFAULT '4' COMMENT '重要程度',
  `ct_wtzh` varchar(32) DEFAULT '' COMMENT '问题帐号',
  `ct_cxqk` varchar(32) DEFAULT '2' COMMENT '出现情况',
  `ct_fxwtbz` varchar(255) DEFAULT '' COMMENT '复现问题步骤',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`ct_id`),
  KEY `ct_type` (`ct_type`),
  KEY `ct_client` (`ct_client`),
  KEY `ct_zycd` (`ct_zycd`),
  KEY `ct_wtzh` (`ct_wtzh`),
  KEY `ct_cxqk` (`ct_cxqk`),
  KEY `ct_master` (`ct_master`)
) ENGINE=MyISAM AUTO_INCREMENT=73 DEFAULT CHARSET=utf8 COMMENT='工单系统';

-- ----------------------------
-- Table structure for pre_component_ticket_log
-- ----------------------------
DROP TABLE IF EXISTS `pre_component_ticket_log`;
CREATE TABLE `pre_component_ticket_log` (
  `ctl_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ct_id` bigint(20) DEFAULT '0' COMMENT '所属工单',
  `ctl_content` varchar(255) DEFAULT '' COMMENT '回复描述',
  `ctl_files` text COMMENT '回复文件',
  `ct_master` varchar(32) NOT NULL DEFAULT 'bzkf10' COMMENT '工单负责人',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`ctl_id`),
  KEY `ctl_content` (`ctl_content`),
  KEY `ct_id` (`ct_id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COMMENT='工单日志';

-- ----------------------------
-- 应用管理与授权模块
-- Table structure for pre_component_main
-- ----------------------------
DROP TABLE IF EXISTS `pre_component_main`;
CREATE TABLE `pre_component_main` (
  `cpm_id` int(10) NOT NULL AUTO_INCREMENT,
  `cpm_status` smallint(3) DEFAULT '0' COMMENT '是否启用',
  `cpm_type` smallint(3) DEFAULT '2' COMMENT '应用类型',
  `cpm_name` varchar(64) DEFAULT '' COMMENT '应用名称',
  `cpm_icon` varchar(128) DEFAULT '' COMMENT '应用图标',
  `cpm_link` varchar(128) DEFAULT '' COMMENT '应用链接',
  `cpm_access_level` varchar(64) DEFAULT '' COMMENT '访问级别',
  `cpm_sort` tinyint(5) DEFAULT '1' COMMENT '用于排序',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`cpm_id`),
  KEY `cpm_name` (`cpm_name`),
  KEY `cpm_status` (`cpm_status`),
  KEY `cpm_access_level` (`cpm_access_level`),
  KEY `cpm_sort` (`cpm_sort`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='应用中心';

-- ----------------------------
-- Table structure for pre_component_main_access
-- ----------------------------
DROP TABLE IF EXISTS `pre_component_main_access`;
CREATE TABLE `pre_component_main_access` (
  `cma_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '#',
  `cma_name` varchar(128) DEFAULT '' COMMENT '授权名称',
  `cma_area_id1` int(10) DEFAULT '0' COMMENT '授权省ID',
  `cma_area_id2` int(10) DEFAULT '0' COMMENT '授权市ID',
  `cma_area_id3` int(10) DEFAULT '0' COMMENT '授权县ID',
  `cma_area_id4` int(10) DEFAULT '0' COMMENT '授权镇ID',
  `cma_sm_id` int(10) DEFAULT '0' COMMENT '授权校ID',
  `cpm_id` int(10) DEFAULT '0' COMMENT '授权应用ID',
  `cma_area_id1_name` varchar(64) NOT NULL DEFAULT '' COMMENT '授权省 ',
  `cma_area_id2_name` varchar(64) NOT NULL DEFAULT '' COMMENT '授权市 ',
  `cma_area_id3_name` varchar(64) NOT NULL DEFAULT '' COMMENT '授权县 ',
  `cma_area_id4_name` varchar(64) NOT NULL DEFAULT '' COMMENT '授权镇 ',
  `cma_sm_id_name` varchar(128) NOT NULL DEFAULT '' COMMENT '授权校 ',
  `cpm_id_name` varchar(64) NOT NULL DEFAULT '' COMMENT '授权应用 ',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`cma_id`),
  KEY `cma_name` (`cma_name`),
  KEY `cma_area_id1` (`cma_area_id1`),
  KEY `cma_area_id2` (`cma_area_id2`),
  KEY `cma_area_id3` (`cma_area_id3`),
  KEY `cma_area_id4` (`cma_area_id4`),
  KEY `cma_sm_id` (`cma_sm_id`),
  KEY `cpm_id` (`cpm_id`),
  KEY `cma_area_id1_name` (`cma_area_id1_name`),
  KEY `cma_area_id2_name` (`cma_area_id2_name`),
  KEY `cma_area_id3_name` (`cma_area_id3_name`),
  KEY `cma_area_id4_name` (`cma_area_id4_name`),
  KEY `cma_sm_id_name` (`cma_sm_id_name`),
  KEY `cpm_id_name` (`cpm_id_name`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COMMENT='应用授权';

-- ----------------------------
-- Table structure for pre_user_org 组织机构表
-- ----------------------------
DROP TABLE IF EXISTS `pre_user_org`;
CREATE TABLE `pre_user_org` (
  `uo_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `uo_pid` int(10) DEFAULT '0' COMMENT '上级编号',
  `uo_name` varchar(128) DEFAULT '' COMMENT '机构名称',
  `uo_sort` int(16) DEFAULT '0' COMMENT '机构排序',
  `uo_user_ids` text COMMENT '组织用户',
  `uo_path` varchar(255) DEFAULT '' COMMENT '绝对路径',
  `uo_user_name` varchar(64) NOT NULL DEFAULT '' COMMENT '机构登录用户名',
  `uo_display_name` varchar(64) NOT NULL DEFAULT '' COMMENT '机构用户显示名',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`uo_id`),
  KEY `uo_pid` (`uo_pid`),
  KEY `uo_sort` (`uo_sort`),
  KEY `uo_path` (`uo_path`),
  KEY `uo_user_name` (`uo_user_name`),
  KEY `uo_display_name` (`uo_display_name`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COMMENT='组织与用户';

-- ----------------------------
-- Table structure for pre_news_center 消息中心主表
-- ----------------------------
DROP TABLE IF EXISTS `pre_news_center`;
CREATE TABLE `pre_news_center` (
  `nc_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `nc_title` varchar(128) DEFAULT '' COMMENT '信息标题',
  `nc_url` varchar(128) DEFAULT '' COMMENT '链接地址',
  `nc_dzbp` smallint(3) DEFAULT '2' COMMENT '是否班牌显示',
  `nc_uo_id` varchar(512) DEFAULT '' COMMENT '发布单位',
  `nc_uo_id_name` varchar(255) NOT NULL DEFAULT '' COMMENT '发布部门显示名称',
  `nc_user_id` varchar(512) DEFAULT '' COMMENT '发送用户',
  `nc_user_id_name` varchar(255) NOT NULL DEFAULT '' COMMENT '发送用户显示名称',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`nc_id`),
  KEY `nc_uo_id` (`nc_uo_id`(333)),
  KEY `nc_user_id` (`nc_user_id`(333)),
  KEY `nc_uo_id_name` (`nc_uo_id_name`),
  KEY `nc_user_id_name` (`nc_user_id_name`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='信息中心';

-- ----------------------------
-- Table structure for pre_news_center_content 消息中心内容表
-- ----------------------------
DROP TABLE IF EXISTS `pre_news_center_content`;
CREATE TABLE `pre_news_center_content` (
  `ncc_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `nc_id` int(10) DEFAULT '0' COMMENT '信息编号',
  `nc_content` text COMMENT '信息内容',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`ncc_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='信息中心内容';

-- ----------------------------
-- Table structure for pre_news_info 信息门户
-- ----------------------------
DROP TABLE IF EXISTS `pre_news_info`;
CREATE TABLE `pre_news_info` (
  `ni_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `ni_title` varchar(128) DEFAULT '' COMMENT '信息标题',
  `ni_tag` varchar(32) DEFAULT '' COMMENT '标签分类',
  `ni_img` varchar(128) DEFAULT '' COMMENT '封面图片',
  `ni_content` text COMMENT '信息内容',
  `ni_fbr` varchar(64) DEFAULT '' COMMENT '发布人',
  `ni_cate` smallint(2) NOT NULL DEFAULT '1' COMMENT '信息分类',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '县编号',
  `nic_id` varchar(128) DEFAULT '' COMMENT '所属分类',
  PRIMARY KEY (`ni_id`),
  KEY `ni_title` (`ni_title`),
  KEY `ni_cate` (`ni_cate`),
  KEY `sm_id` (`sm_id`),
  KEY `user_id` (`user_id`),
  KEY `isdelete` (`isdelete`),
  KEY `nic_id` (`nic_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10000 DEFAULT CHARSET=utf8 COMMENT='平台信息';

-- ----------------------------
-- Table structure for pre_news_info_cate 信息分类管理
-- ----------------------------
DROP TABLE IF EXISTS `pre_news_info_cate`;
CREATE TABLE `pre_news_info_cate` (
  `nic_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `nic_pid` int(10) DEFAULT '0' COMMENT '分类上级编号',
  `nic_name` varchar(128) DEFAULT '' COMMENT '分类名称',
  `nic_description` varchar(255) DEFAULT '' COMMENT '分类描述',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '学校编号',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0',
  `nic_path` varchar(255) DEFAULT '' COMMENT '目录路径',
  `nic_sort` int(10) DEFAULT '999' COMMENT '排序',
  `nic_position` varchar(32) DEFAULT 'other' COMMENT '推荐位置',
  `nic_type` varchar(32) DEFAULT 'system' COMMENT '分类类型',
  `nic_url` varchar(128) DEFAULT '' COMMENT '外部网址',
  `nic_icon` varchar(128) NOT NULL DEFAULT '' COMMENT '分类图标',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  PRIMARY KEY (`nic_id`),
  KEY `nic_path` (`nic_path`),
  KEY `nic_sort` (`nic_sort`),
  KEY `nic_name` (`nic_name`) USING BTREE,
  KEY `nic_pid` (`nic_pid`) USING BTREE,
  KEY `nic_position` (`nic_position`),
  KEY `nic_type` (`nic_type`),
  KEY `nic_icon` (`nic_icon`)
) ENGINE=MyISAM AUTO_INCREMENT=203 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for pre_oa_gzjh 办公系统-工作计划
-- ----------------------------
DROP TABLE IF EXISTS `pre_oa_gzjh`;
CREATE TABLE `pre_oa_gzjh` (
  `og_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `og_title` varchar(128) DEFAULT '' COMMENT '标题',
  `og_starttime` date NOT NULL DEFAULT '0000-00-00' COMMENT '开始时间',
  `og_kfry` varchar(128) DEFAULT '' COMMENT '开发人员',
  `og_content` text COMMENT '工作内容',
  `og_wcd` smallint(3) DEFAULT '0' COMMENT '完成度',
  `og_wcd_other` text COMMENT '备注说明',
  `og_sxzt` tinyint(3) DEFAULT '1' COMMENT '完成测试',
  `og_endtime` date NOT NULL DEFAULT '0000-00-00' COMMENT '完成时间',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`og_id`),
  KEY `og_kfry` (`og_kfry`),
  KEY `og_wcd` (`og_wcd`),
  KEY `og_sxzt` (`og_sxzt`),
  KEY `og_starttime` (`og_starttime`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='工作计划';

-- ----------------------------
-- Table structure for pre_oa_hyjy 办公系统-会议纪要
-- ----------------------------
DROP TABLE IF EXISTS `pre_oa_hyjy`;
CREATE TABLE `pre_oa_hyjy` (
  `oh_id` int(10) NOT NULL AUTO_INCREMENT COMMENT '#',
  `oh_title` varchar(255) DEFAULT '' COMMENT '会议标题',
  `oh_hysj_start` varchar(32) DEFAULT '' COMMENT '会议开始时间',
  `oh_hysj_end` varchar(32) DEFAULT '' COMMENT '会议结束时间',
  `oh_hynr` text COMMENT '会议内容',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`oh_id`),
  KEY `oh_title` (`oh_title`),
  KEY `oh_hysj_start` (`oh_hysj_start`),
  KEY `oh_hysj_end` (`oh_hysj_end`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='会议纪要';

-- ----------------------------
-- Table structure for pre_wx_menu
-- ----------------------------
DROP TABLE IF EXISTS `pre_wx_menu`;
CREATE TABLE `pre_wx_menu` (
  `wm_id` int(10) NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(32) DEFAULT '' COMMENT '名称',
  `wm_type` varchar(32) DEFAULT '' COMMENT '类型',
  `wm_url` varchar(128) DEFAULT '' COMMENT '链接网址',
  `wm_sort` int(10) DEFAULT '0' COMMENT '排序',
  `wm_pid` int(10) DEFAULT '0' COMMENT '上级菜单编号',
  `wm_path` varchar(128) DEFAULT '' COMMENT '绝对路径',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`wm_id`),
  KEY `wm_name` (`wm_name`),
  KEY `wm_type` (`wm_type`),
  KEY `wm_sort` (`wm_sort`),
  KEY `wm_pid` (`wm_pid`),
  KEY `wm_path` (`wm_path`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='微信菜单';

-- ----------------------------
-- Table structure for pre_small_main 使用小工具
-- ----------------------------
DROP TABLE IF EXISTS `pre_small_main`;
CREATE TABLE `pre_small_main` (
  `stm_id` int(10) NOT NULL AUTO_INCREMENT,
  `stm_name` varchar(64) DEFAULT '' COMMENT '名称',
  `stm_icon` varchar(128) DEFAULT '' COMMENT '图标',
  `stm_url` varchar(128) DEFAULT '' COMMENT '网址',
  `stm_other` varchar(255) DEFAULT '' COMMENT '其他备注',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`stm_id`),
  KEY `stm_name` (`stm_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='实用小工具';

-- ----------------------------
-- Table structure for pre_small_pdf2img PDF转JPG小工具
-- ----------------------------
DROP TABLE IF EXISTS `pre_small_pdf2img`;
CREATE TABLE `pre_small_pdf2img` (
  `spi_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `spi_name` varchar(128) DEFAULT '' COMMENT '名称',
  `spi_url` varchar(255) DEFAULT '' COMMENT 'PDF原始网址',
  `spi_num` int(10) DEFAULT '0' COMMENT '页数',
  `spi_type` varchar(64) DEFAULT 'jpg' COMMENT '图片类型',
  `spi_pdf_path` varchar(128) DEFAULT '' COMMENT 'PDF存储文件',
  `spi_img_content` text COMMENT 'PDF转换图片',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`spi_id`),
  KEY `spi_name` (`spi_name`),
  KEY `spi_url` (`spi_url`),
  KEY `spi_type` (`spi_type`),
  KEY `spi_pdf_path` (`spi_pdf_path`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='PDF转图片';

-- ----------------------------
-- Table structure for pre_stat_visit 访问量统计
-- ----------------------------
DROP TABLE IF EXISTS `pre_stat_visit`;
CREATE TABLE `pre_stat_visit` (
  `sv_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sv_year` varchar(64) DEFAULT '' COMMENT '访问年',
  `sv_pageview_num` int(10) DEFAULT '0' COMMENT '页面访问量',
  `sv_uvview_num` int(10) DEFAULT '0' COMMENT '唯一用户访问量',
  `sv_login_num` int(10) DEFAULT '0' COMMENT '用户登录数',
  `sv_month` int(10) DEFAULT '0' COMMENT '访问月',
  `area_id1` bigint(20) DEFAULT '0' COMMENT '省编号',
  `area_id2` bigint(20) DEFAULT '0' COMMENT '市编号',
  `area_id3` bigint(20) DEFAULT '0' COMMENT '县编号',
  `area_id4` bigint(20) DEFAULT '0' COMMENT '镇编号',
  `sm_id` int(10) DEFAULT '0' COMMENT '所属学校',
  `dept_id` int(10) DEFAULT '0' COMMENT '所属单位或部门',
  `user_id` int(10) DEFAULT '0' COMMENT '所属用户',
  `create_user_id` int(10) DEFAULT '0' COMMENT '创建用户',
  `modify_user_id` int(10) DEFAULT '0' COMMENT '修改用户',
  `create_dateline` int(10) DEFAULT '0' COMMENT '创建时间',
  `modify_dateline` int(10) DEFAULT '0' COMMENT '修改时间',
  `isdelete` tinyint(1) unsigned zerofill DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`sv_id`),
  KEY `sv_year` (`sv_year`),
  KEY `sv_pageview_num` (`sv_pageview_num`),
  KEY `sv_uvview_num` (`sv_uvview_num`),
  KEY `sv_login_num` (`sv_login_num`),
  KEY `sv_month` (`sv_month`),
  KEY `sv_year_2` (`sv_year`,`sv_month`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='访问量统计';