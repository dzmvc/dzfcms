<?php

$_config = array();

// ----------------------------  CONFIG DB  ----------------------------- //
// ----------------------------  数据库相关设置---------------------------- //

/**
 * 数据库主服务器设置, 支持多组服务器设置, 当设置多组服务器时, 则会根据分布式策略使用某个服务器
 * 多数据库配置与使用可以方便的使用多数据库，可以单独的配置不同的数据库连接，作用有以下两个好处
 * 1、如果负载足够搞，是可以把很一些表分布在多台数据库服务器中。这样来减小负载
 * 2、可以方便的使用别的数据库中的数据
 * @example
 * $_config['db']['1']['dbhost'] = 'localhost'; // 服务器地址
 * $_config['db']['1']['dbuser'] = 'root'; // 用户
 * $_config['db']['1']['dbpw'] = 'root';// 密码
 * $_config['db']['1']['dbcharset'] = 'gbk';// 字符集
 * $_config['db']['1']['pconnect'] = '0';// 是否持续连接
 * $_config['db']['1']['dbname'] = 'x1';// 数据库
 * $_config['db']['1']['tablepre'] = 'pre_';// 表名前缀
 *
 * //2的意思对应随后配置的数据库连接参数,使用静态类DB::table方法，会根据表名调用对应的数据库配置来链接
 * $_config['db']['map']=array('表名'=>'2');
 * //这里面2对应映射配置
 * $_config['db']['2']['dbhost'] = 'localhost';
 * $_config['db']['2']['dbuser'] = 'root';
 * $_config['db']['2']['dbpw'] = '';
 * $_config['db']['2']['dbcharset'] = 'gbk';
 * $_config['db']['2']['pconnect'] = '0';
 * $_config['db']['2']['dbname'] = '数据库名';
 * //这个参数其实没有用，要照随后的修改就可以用了
 * $_config['db']['2']['tablepre'] = 'pre2_';
 * ...
 * 
 * 注意事项
 * 一定要使用DB::table方法，因为通过这个方法来转换数据库连接。
 * 一些联表查询不能使用
 * 外联数据库的表前缀一定得和现在的表前缀一样。如 pre_表名
 * 当打开 debug 参数时，会提示表不存在，所以此时应关闭DEBUG
 *
 * //如果要自定义表前缀需要修改一些额外的修改
 * 如果需要使用其它数据库的非默认系统表，需要修改 class_core(1.5),db_driver_mysql(2.5) 的 table_name 函数，这样就不用限制 pre_ 表前缀了。而且配置中的数据库前缀也可以用了
 * 
    function table_name($tablename) {
        if(!empty($this->map) && !empty($this->map[$tablename])) {
            $id = $this->map[$tablename];
            if(!$this->link[$id]) {
                $this->connect($id);
            }
            $this->curlink = $this->link[$id];
            //增加了下面这一行
            return $this->config[$id]['tablepre'].$tablename;
        } else {
            $this->curlink = $this->link[1];
        }

        return $this->tablepre.$tablename;
    }
 */
$_config['db'][1]['dbhost']  		= 'localhost';
$_config['db'][1]['dbuser']  		= 'root';
$_config['db'][1]['dbpw'] 	 	= '123456';
$_config['db'][1]['dbcharset'] 		= 'utf8';
$_config['db'][1]['pconnect'] 		= 0;
$_config['db'][1]['dbname']  		= 'your_dbname';
$_config['db'][1]['tablepre'] 		= 'pre_';

/**
 * 数据库从服务器设置( slave, 只读 ), 支持多组服务器设置, 当设置多组服务器时, 系统每次随机使用
 * @example
 * $_config['db']['slave']['1']['dbhost'] = 'localhost';
 * $_config['db']['slave']['1']['dbuser'] = 'root';
 * $_config['db']['slave']['1']['dbpw'] = 'root';
 * $_config['db']['slave']['1']['dbcharset'] = 'gbk';
 * $_config['db']['slave']['1']['pconnect'] = '0';
 * $_config['db']['slave']['1']['dbname'] = 'x1';
 * $_config['db']['slave']['1']['tablepre'] = 'pre_';
 *
 * $_config['db']['slave']['2']['dbhost'] = 'localhost';
 * ...
 *
 */
$_config['db']['slave'] = array();

/**
 * 数据库 分布部署策略设置
 *
 * @example 将 common_member 部署到第二服务器, common_session 部署在第三服务器, 则设置为
 * $_config['db']['map']['common_member'] = 2;
 * $_config['db']['map']['common_session'] = 3;
 *
 * 对于没有明确声明服务器的表, 则一律默认部署在第一服务器上
 *
 */
$_config['db']['map'] = array();

/**
 * 数据库 公共设置, 此类设置通常对针对每个部署的服务器
 */
$_config['db']['common'] = array();

/**
 *  禁用从数据库的数据表, 表名字之间使用逗号分割
 *
 * @example common_session, common_member 这两个表仅从主服务器读写, 不使用从服务器
 * $_config['db']['common']['slave_except_table'] = 'common_session, common_member';
 *
 */
$_config['db']['common']['slave_except_table'] = '';

/**
 * 内存服务器优化设置
 * 以下设置需要PHP扩展组件支持，其中 memcache 优先于其他设置，
 * 当 memcache 无法启用时，会自动开启另外的两种优化模式
 */

//内存变量前缀, 可更改,避免同服务器中的程序引用错乱
$_config['memory']['prefix'] = 'core_';

/* reids设置, 需要PHP扩展组件支持, timeout参数的作用没有查证 */
$_config['memory']['redis']['server'] = '';
$_config['memory']['redis']['port'] = 6379;
$_config['memory']['redis']['pconnect'] = 1;
$_config['memory']['redis']['timeout'] = 0;
/**
 * 是否使用 Redis::SERIALIZER_IGBINARY选项,需要igbinary支持,windows下测试时请关闭，否则会出>现错误Reading from client: Connection reset by peer
 * 支持以下选项，默认使用PHP的serializer
 * [重要] 该选项已经取代原来的 $_config['memory']['redis']['igbinary'] 选项
 * Redis::SERIALIZER_IGBINARY =2
 * Redis::SERIALIZER_PHP =1
 * Redis::SERIALIZER_NONE =0 //则不使用serialize,即无法保存array
 */
$_config['memory']['redis']['serializer'] = 1;

$_config['memory']['memcache']['server'] = '';			// memcache 服务器地址
$_config['memory']['memcache']['port'] = 11211;			// memcache 服务器端口
$_config['memory']['memcache']['pconnect'] = 1;			// memcache 是否长久连接
$_config['memory']['memcache']['timeout'] = 1;			// memcache 服务器连接超时

$_config['memory']['apc'] = 1;							// 启动对 apc 的支持
$_config['memory']['xcache'] = 1;						// 启动对 xcache 的支持
$_config['memory']['eaccelerator'] = 1;					// 启动对 eaccelerator 的支持
$_config['memory']['wincache'] = 1;						// 启动对 wincache 的支持
// 服务器相关设置
$_config['server']['id']		= 1;			// 服务器编号，多webserver的时候，用于标识当前服务器的ID

// 附件下载相关
//
// 本地文件读取模式; 模式2为最节省内存方式，但不支持多线程下载
// 1=fread 2=readfile 3=fpassthru 4=fpassthru+multiple
$_config['download']['readmod'] = 2;

// 是否启用 X-Sendfile 功能（需要服务器支持）0=close 1=nginx 2=lighttpd 3=apache
$_config['download']['xsendfile']['type'] = 0;

// 启用 nginx X-sendfile 时，论坛附件目录的虚拟映射路径，请使用 / 结尾
$_config['download']['xsendfile']['dir'] = '/down/';

//  CONFIG CACHE
$_config['cache']['type'] 			= 'sql';	// 缓存类型 file=文件缓存, sql=数据库缓存

// 页面输出设置
$_config['output']['charset'] 			= 'utf-8';	// 页面字符集
$_config['output']['forceheader']		= 1;		// 强制输出页面字符集，用于避免某些环境乱码
$_config['output']['gzip'] 			= 0;		// 是否采用 Gzip 压缩输出
$_config['output']['tplrefresh'] 		= 1;		// 模板自动刷新开关 0=关闭, 1=打开
$_config['output']['language'] 			= 'sc';	// 页面语言 sc/en
$_config['output']['staticurl'] 		= 'static/';	// 站点静态文件路径，“/”结尾
$_config['output']['ajaxvalidate']		= 0;		// 是否严格验证 Ajax 页面的真实性 0=关闭，1=打开
$_config['output']['iecompatible']		= 0;		// 页面 IE 兼容模式

// COOKIE 设置
$_config['cookie']['cookiepre'] 		= 'pre_'; 	// COOKIE前缀
$_config['cookie']['cookiedomain'] 		= ''; 		// COOKIE作用域
$_config['cookie']['cookiepath'] 		= '/'; 		// COOKIE作用路径

// 站点安全设置
$_config['security']['authkey']			= 'asdfasfas';	// 站点加密密钥
$_config['security']['urlxssdefend']		= true;		// 自身 URL XSS 防御
$_config['security']['attackevasive']		= 0;		// CC 攻击防御 1|2|4|8

$_config['security']['querysafe']['status']	= 1;		// 是否开启SQL安全检测，可自动预防SQL注入攻击
$_config['security']['querysafe']['dfunction']	= array('load_file','hex','substring','if','ord','char');
$_config['security']['querysafe']['daction']	= array('intooutfile','intodumpfile','unionselect','(select', 'unionall', 'uniondistinct');
$_config['security']['querysafe']['dnote']	= array('/*','*/','#','--','"');
$_config['security']['querysafe']['dlikehex']	= 1;
$_config['security']['querysafe']['afullnote']	= 0;

$_config['admincp']['founder']			= '1';		// 站点创始人：拥有站点管理后台的最高权限，每个站点可以设置 1名或多名创始人
								// 可以使用uid，也可以使用用户名；多个创始人之间请使用逗号“,”分开;
$_config['admincp']['forcesecques']		= 0;		// 管理人员必须设置安全提问才能进入系统设置 0=否, 1=是[安全]
$_config['admincp']['checkip']			= 1;		// 后台管理操作是否验证管理员的 IP, 1=是[安全], 0=否。仅在管理员无法登陆后台时设置 0。
$_config['admincp']['runquery']			= 0;		// 是否允许后台运行 SQL 语句 1=是 0=否[安全]
$_config['admincp']['dbimport']			= 1;		// 是否允许后台恢复论坛数据  1=是 0=否[安全]

/**
 * 系统远程调用功能模块
 */

// 远程调用: 总开关 0=关  1=开
$_config['remote']['on'] = 0;

// 远程调用: 程序目录名. 出于安全考虑,您可以更改这个目录名, 修改完毕, 请手工修改程序的实际目录
$_config['remote']['dir'] = 'remote';

// 远程调用: 通信密钥. 用于客户端和本服务端的通信加密. 长度不少于 32 位
//          默认值是 $_config['security']['authkey']	的 md5, 您也可以手工指定
$_config['remote']['appkey'] = md5($_config['security']['authkey']);

// 远程调用: 开启外部 cron 任务. 系统内部不再执行cron, cron任务由外部程序激活
$_config['remote']['cron'] = 0;

// $_GET|$_POST的兼容处理，0为关闭，1为开启；开启后即可使用$_G['gp_xx'](xx为变量名，$_GET和$_POST集合的所有变量名)，值为已经addslashes()处理过
$_config['input']['compatible'] = 1;

//DEBUG 消息发送接口
$_config['msg'][1]['host'] = 'http://127.0.0.1';

//DEBUG CONFIG FILES
$_config['file']['1']['host'] = 'http://127.0.0.1';
$_config['vod']['1']['host'] = 'http://127.0.0.1';

//DEBUG Multi-LANG
$_config['detect_language'] = true;	// Auto-detect user language: true|false
$_config['enable_multilingual'] = true;	// Enable/Disable multi-lingual feature
//DEBUG 自定义管理地址
$_config['admin_folder'] = 'admin';
//DEBUG 开发辅助
$_config['plugindeveloper'] = 0;//2
$_config['production'] = 0;//1 网站发布 屏蔽一些错误与异常页面输出 0 开发显示
$_config['debug_log'] = '1';//调试记录到日志文件
$_config['debug'] = 0;//0不显示 1显示错误 2异常全部显示 
$_config['debug_access'] = 1;//0 开启URL 权限校验 1 表示取消访问者的URL权限验证
// -------------------  THE END  -------------------- //
?>