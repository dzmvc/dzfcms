<?php
header('Content-type:text/html; Charset=utf-8');
header('Access-Control-Allow-Origin:*');
require_once '../dz_framework/init.php';
ini_set("display_errors","On");
error_reporting(E_ALL);
$text = isset($_REQUEST['url']) && !empty($_REQUEST['url']) ? $_REQUEST['url']:'';
$text = urldecode($text);
require_once SITE_ROOT.'./plugins/phpqrcode/phpqrcode.php';
$errorCorrectionLevel = 3;  //容错级别
$matrixPointSize = 4;
$margin = 4;
//生成二维码图片
//$QR = QRcode::png($text,false,$errorCorrectionLevel, $matrixPointSize, $size);
QRcode::png($text,false, $errorCorrectionLevel, $matrixPointSize, 2);