<?php
require_once '../../../dz_framework/init.php';
//include SITE_ROOT.'./source/lib/weixin_sdk/native.php';
//ini_set("display_errors","On");
//error_reporting(E_ALL);
//echo json_encode($_REQUEST);
@file_put_contents('debug.log.txt', date('Y-m-d H:i:s',TIMESTAMP).' '.json_encode($_REQUEST).PHP_EOL, FILE_APPEND);
$postStr = file_get_contents('php://input');
@file_put_contents('debug2.log.txt', date('Y-m-d H:i:s',TIMESTAMP).' '.$postStr.PHP_EOL, FILE_APPEND);
$mchid = '1528581791';          //微信支付商户号 PartnerID 通过微信支付商户资料审核后邮件发送
$appid = 'wxf7d7d4355cfc49a3';  //公众号APPID 通过微信支付商户资料审核后邮件发送
$apiKey = '3908c33da5ff3954b7ceed64acbeb59c';   //https://pay.weixin.qq.com 帐户设置-安全设置-API安全-API密钥-设置API密钥
$wxPay = new WxpayService($mchid,$appid,$apiKey);
$result = $wxPay->notify();
if($result){
    /*
    {
        "appid": "wxf7d7d4355cfc49a3",
        "attach": "pay",
        "bank_type": "CFT",
        "cash_fee": "1",
        "fee_type": "CNY",
        "is_subscribe": "N",
        "mch_id": "1528581791",
        "nonce_str": "LqGr80vtNdNz88n2",
        "openid": "oo8ZB1SoD0F_UdGDuZomw234PHHA",
        "out_trade_no": "2019031822324661774",
        "result_code": "SUCCESS",
        "return_code": "SUCCESS",
        "time_end": "20190318223251",
        "total_fee": "1",
        "trade_type": "APP",
        "transaction_id": "4200000264201903184301959738"
    }
    */
    @file_put_contents('debug.log.txt', date('Y-m-d H:i:s',TIMESTAMP).' '.json_encode($result).PHP_EOL, FILE_APPEND);
    //完成你的逻辑
    //例如连接数据库，获取付款金额$result['cash_fee']，获取订单号$result['out_trade_no']，修改数据库中的订单状态等;
    if($result['result_code']=='SUCCESS'){
        $sql = "UPDATE ".DB::table('mall_shop_trade')." SET mst_trade_status ='TRADE_SUCCESS',mst_total_fee_pay='".$result['total_fee']."' WHERE mst_out_trade_no='".$result['out_trade_no']."' LIMIT 1";
        $effect = DB::query($sql);
        if($effect){
            //更新服务期限
            //1、取出订单
            $sql = "SELECT mst_id,user_id FROM ".DB::table('mall_shop_trade')." WHERE mst_out_trade_no='".$result['out_trade_no']."' LIMIT 1";
            $mall_shop_trade = DB::fetch_first($sql);
            //2、取出订单对应产品
            $sql = "SELECT * FROM ".DB::table('mall_shop_trade_products')." WHERE mst_id='".$mall_shop_trade['mst_id']."' LIMIT 100";
            $mall_shop_trade_products = DB::fetch_all($sql);
            //3、更新产品服务
            //取出用户已购买的服务
            $sql = "SELECT * FROM ".DB::table('mall_shop_service')." WHERE user_id='".$mall_shop_trade['user_id']."' AND mss_endtime > ".date('Y-m-d',TIMESTAMP)." LIMIT 100";
            $mall_shop_service = DB::fetch_all($sql);
            $mall_shop_service_array = array();
            foreach ($mall_shop_service as $key => $value) {
                $mall_shop_service_array[$value['msp_id']]=$value;
            }
            //插入产品服务
           foreach($mall_shop_trade_products AS $key => $value){
               $mall_shop_service_detail = array();
               if($mall_shop_service_array[$value]){
                   $mall_shop_service_detail = array(
                        'mss_endtime'=>date('Y-m-d',strtotime($mall_shop_service_array[$value]['mss_endtime'].' 00:00:01')+365*24*3600*$value['mstp_num']),
                   );
                   $condition = array(
                       'mss_id'=>$mall_shop_service_array[$value]['mss_id'],
                   );
                   @DB::update('mall_shop_service', $mall_shop_service_detail,$condition);
               }else{
                    $mall_shop_service_detail = array(
                         'msp_id'=>$value['msp_id'],
                         'msp_name'=>$value['msp_name'],
                         'user_id'=>$mall_shop_trade['user_id'],
                         'mss_endtime'=>date('Y-m-d',TIMESTAMP+365*24*3600*$value['mstp_num']),
                         'msp_code'=>'',
                         'create_dateline'=>TIMESTAMP
                    );
                    @DB::insert('mall_shop_service', $mall_shop_service_detail,TRUE,TRUE);
                    //DEBUG 插入购买课程的课时数据 开始
                    //取出产品对应课时 txwx_live_course
                    $sql = "SELECT * FROM ".DB::table('txwx_live_course')." WHERE msp_id='".$value['msp_id']."' LIMIT 100";
                    $txwx_live_course = @DB::fetch_all($sql);
                    //把课时插入选课表 txwx_live_choose
                    foreach($txwx_live_course AS $kkk => $vvv){
                        $txwx_live_choose_insert_data = array(
                            'user_id'=>$mall_shop_trade['user_id'],
                            'lc_lc_id'=>$vvv['lc_id'],
                            'create_dateline'=>TIMESTAMP,
                            'lc_start_time'=>$vvv['lc_start_time'],
                            'lc_end_time'=>$vvv['lc_start_time']
                        );
                        @DB::insert('txwx_live_choose', $txwx_live_choose_insert_data,false,true);
                    }
                    //DEBUG 插入购买课程的课时数据 结束
               }
           }
        }
    }
}else{
    echo 'pay error';
}
class WxpayService
{
    protected $mchid;
    protected $appid;
    protected $apiKey;
    public function __construct($mchid, $appid, $key)
    {
        $this->mchid = $mchid;
        $this->appid = $appid;
        $this->apiKey = $key;
    }

    public function notify()
    {
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apiKey,
        );
        $postStr = file_get_contents('php://input');
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);        
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($postObj === false) {
            die('parse xml error');
        }
        if ($postObj->return_code != 'SUCCESS') {
            die($postObj->return_msg);
        }
        if ($postObj->result_code != 'SUCCESS') {
            die($postObj->err_code);
        }
        $arr = (array)$postObj;
        unset($arr['sign']);
        if (self::getSign($arr, $config['key']) == $postObj->sign) {
            echo '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
            return $arr;
        }
    }

    /**
     * 获取签名
     */
    public static function getSign($params, $key)
    {
        ksort($params, SORT_STRING);
        $unSignParaString = self::formatQueryParaMap($params, false);
        $signStr = strtoupper(md5($unSignParaString . "&key=" . $key));
        return $signStr;
    }
    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }
}
