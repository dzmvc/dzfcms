<?php
require_once '../../../dz_framework/init.php';
require_once 'AlipayService.php';
//ini_set("display_errors","On");
//error_reporting(E_ALL);
file_put_contents('debug.log.txt', date('Y-m-d H:i:s',TIMESTAMP).' '.json_encode($_REQUEST).PHP_EOL, FILE_APPEND);
//支付宝公钥，账户中心->密钥管理->开放平台密钥，找到添加了支付功能的应用，根据你的加密类型，查看支付宝公钥
$alipayPublicKey='MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAshj581ry96HznksmsA7O+q9Q7/CZDElDFNCip+dbvJq04Jto5NjBsgG/zyC4ID6y+AYdklZ8iF6imdlnGlV+5Q+N5teRI4z9IxYUI1iAkeC42t/iB5ICsX8ckpxEMwgMkn8hHd9KFkXC13oUhHdwmH7h6IEMh4liBF+idYsykGgWTdPVd001INAPohwsmQt7QQ0WkdWvqGX/MxIS4bdM57O806+G2NoyiRpBOahmejF64/skgQE2+p+DQXd83sVQHkP4sFH2U0T8ZIMDb14iE9AQnoGM5ASaNw/wZEZ/8FPFJAUa1yTlDcvEXuv4izxUNoRYJuFj40ts1zbPxtNAGwIDAQAB';

$aliPay = new AlipayService($alipayPublicKey);
//验证签名
$result = $aliPay->rsaCheck($_POST,$_POST['sign_type']);
if($result===true || 1==1){
    //获取订单号$_POST['out_trade_no']，订单金额$_POST['total_amount']等
    //程序执行完后必须打印输出“success”（不包含引号）。如果商户反馈给支付宝的字符不是success这7个字符，支付宝服务器会不断重发通知，直到超过24小时22分钟。一般情况下，25小时以内完成8次通知（通知的间隔频率一般是：4m,10m,10m,1h,2h,6h,15h；
    //更新支付状态
    if($_REQUEST['trade_status']=='TRADE_SUCCESS'){
        $sql = "UPDATE ".DB::table('mall_shop_trade')." SET mst_trade_status ='TRADE_SUCCESS',mst_total_fee_pay='".$_REQUEST['total_amount']."' WHERE mst_out_trade_no='".$_REQUEST['out_trade_no']."' LIMIT 1";
        $effect = DB::query($sql);
        if($effect){
            //更新服务期限
            //1、取出订单
            $sql = "SELECT mst_id,user_id FROM ".DB::table('mall_shop_trade')." WHERE mst_out_trade_no='".$_REQUEST['out_trade_no']."' LIMIT 1";
            $mall_shop_trade = DB::fetch_first($sql);
            //2、取出订单对应产品
            $sql = "SELECT * FROM ".DB::table('mall_shop_trade_products')." WHERE mst_id='".$mall_shop_trade['mst_id']."' LIMIT 100";
            $mall_shop_trade_products = DB::fetch_all($sql);
            //3、更新产品服务
            //取出用户已购买的服务
            $sql = "SELECT * FROM ".DB::table('mall_shop_service')." WHERE user_id='".$mall_shop_trade['user_id']."' AND mss_endtime > ".date('Y-m-d',TIMESTAMP)." LIMIT 100";
            $mall_shop_service = DB::fetch_all($sql);
            $mall_shop_service_array = array();
            foreach ($mall_shop_service as $key => $value) {
                $mall_shop_service_array[$value['msp_id']]=$value;
            }
            //插入产品服务
           foreach($mall_shop_trade_products AS $key => $value){
               $mall_shop_service_detail = array();
               if($mall_shop_service_array[$value]){
                   $mall_shop_service_detail = array(
                        'mss_endtime'=>date('Y-m-d',strtotime($mall_shop_service_array[$value]['mss_endtime'].' 00:00:01')+365*24*3600*$value['mstp_num']),
                   );
                   $condition = array(
                       'mss_id'=>$mall_shop_service_array[$value]['mss_id'],
                   );
                   @DB::update('mall_shop_service', $mall_shop_service_detail,$condition);
               }else{
                    $mall_shop_service_detail = array(
                         'msp_id'=>$value['msp_id'],
                         'msp_name'=>$value['msp_name'],
                         'user_id'=>$mall_shop_trade['user_id'],
                         'mss_endtime'=>date('Y-m-d',TIMESTAMP+365*24*3600*$value['mstp_num']),
                         'msp_code'=>'',
                         'create_dateline'=>TIMESTAMP
                    );
                    @DB::insert('mall_shop_service', $mall_shop_service_detail,TRUE,TRUE);
                    //DEBUG 插入购买课程的课时数据 开始
                    //取出产品对应课时 txwx_live_course
                    $sql = "SELECT * FROM ".DB::table('txwx_live_course')." WHERE msp_id='".$value['msp_id']."' LIMIT 100";
                    $txwx_live_course = @DB::fetch_all($sql);
                    //把课时插入选课表 txwx_live_choose
                    foreach($txwx_live_course AS $kkk => $vvv){
                        $txwx_live_choose_insert_data = array(
                            'user_id'=>$mall_shop_trade['user_id'],
                            'lc_lc_id'=>$vvv['lc_id'],
                            'create_dateline'=>TIMESTAMP,
                            'lc_start_time'=>$vvv['lc_start_time'],
                            'lc_end_time'=>$vvv['lc_start_time']
                        );
                        @DB::insert('txwx_live_choose', $txwx_live_choose_insert_data,false,true);
                    }
                    //DEBUG 插入购买课程的课时数据 结束
               }
           }
           echo 'success';exit();
        }
    }
}
echo 'error';exit();
