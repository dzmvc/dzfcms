<?php
/**
 * 模块信息类
* @filename source/class/common_sql.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class common_sql{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    public static $field_value=array();
    public static $field_value_mids = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
            $object = new self();
        }
        return $object;
    }

    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * @param string $table_name 数据所在表名称 必填
     * @param string $key_name 数据所在表主键名称 必填
     * @param int $key_id 获取数据主键编号 必填
     * @return array $return $info 数据表整个一行数据信息
     */
    public function one_info($table_name,$key_name,$key_id='') {
        $info = array();
        if($table_name && $key_name && $key_id){
            $sql = "SELECT * FROM ".DB::table($table_name)." WHERE ".$key_name."='".$key_id."' LIMIT 1";
            $info = DB::fetch_first($sql);	
        }
        if(empty($info)){
            $info = array();
        }
        return $info;
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * @param string $table_name 数据所在表名称 必填
     * @param array $insert_data 需要添加的数据 数组key 为表字段名  数组key对应value为字段对应数值 必填
     * @param bool $return_insert_id bool true 返回添加的数据主键 false 返回执行操作状态 选填
     * @param bool $replace bool true 遇到唯一索引替换插入 false 遇到唯一索引不插入 选填
     * @param bool $silent bool true 静默运行 false 遇到异常输出 选填
     * @return int $return $insert_id 插入后数据的主键编号 或 true/false
     */
    public function add($table_name,$insert_data=array(), $return_insert_id = true, $replace = false, $silent = false) {
        $insert_id = '';
        if($table_name && $insert_data){
            $insert_id = DB::insert($table_name,$insert_data,$return_insert_id,$replace,$silent);
        }
        return $insert_id;
    }

    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * @param string $table_name 数据所在表名称 必填
     * @param array $update_data 需要修改的数据 数组key 为表字段名  数组key对应value为字段对应数值 必填
     * @param array $where 需要修改数据的条件 数组key 为表字段名  数组key对应value为字段对应数值 必填
     * @return int $return $effect_row 更新影响数据数量
     */
    public function edit($table_name,$update_data,$where) {
        //DEBUG 定义返回 消息格式与消息代码编号
        $effect_row='';
        if($update_data && $where){
            $effect_row = DB::update($table_name,$update_data,$where);
        }
        return $effect_row;
    }

    /**
     * 删除一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * @param string $table_name 数据所在表名称 必填
     * @param array $where 需要删除数据的条件 数组key 为表字段名  数组key对应value为字段对应数值 必填
     * @return int $return 操作结果状态 成功/失败 由 true/false 表示
     */
    public function delete($table_name,$where,$limit=1) {
        $return='';
        if($table_name && $where){
            $return = DB::delete($table_name, $where, $limit);
            //TODO 逻辑删需要创建isdelete字段 0=未删除 1=已删除
            //DB::update($table_name,array('isdelete'=>1), $table_name, $limit=1);
        }
        return $return;
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * @param string $table_name 数据所在表名称 必填
     * @param array $page_condition 分页条件
     * @param int $page_condition['page'] 当前页 获取哪一页数据
     * @param int $page_condition['limit'] 获取数据量限制
     * @param int $page_condition['perpage'] 每页显示数据量 计算总页数
     * @param string $page_condition['wheresql'] 其他SQL WHERE 条件
     * @param string $page_condition['orderby'] 数据排序条件
     * @return int $return 返回获取数据
     * @return int $return['page_now'] 当前页
     * @return int $return['total_page'] 总页数
     * @return int $return['total_rows'] 满足查询条件的数据总量
     * @return array $return['page_data'] 一页数据内容
     */
    public function index($table_name,$page_condition){
        $page_result = array();
        $page = $page_condition['page'];
        $limit = $page_condition['limit'];
        $perpage = $page_condition['perpage'];
        $start=(($page-1) * $perpage);
        $wheresql = $page_condition['wheresql'];
        $orderby = $page_condition['orderby'];
        $sql_info = "SELECT * FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql." ".$page_condition['orderby']." ".DB::limit($start, $limit);
        $sql_info_result = DB::fetch_all($sql_info);
        $sql_total_rows = "SELECT count(*) FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql."";
        $sql_total_rows_result = DB::result_first($sql_total_rows);
        //DEBUG 列表数据返回结构
        $page_result = array(
            //当前页面
            'page_now' => $page,
            //总页数
            'total_page' => max(1, ceil($sql_total_rows_result / $perpage)),
            //int 返回结果总数
            'total_rows' => $sql_total_rows_result,
            //array 一页数据数组
            'page_data' => $sql_info_result
        );
        return $page_result;
    }

    /*
    *  获取一个表字段的多个数据值
    * @author yangw <2441069162@qq.com>
    * @copyright (c) 2017-11-06 api.bozedu.net $
    * @version 1.0
    * @param string $table_name 数据所在表名称 必填
    * @param string $key_name 数据所在表条件字段名称 必填
    * @param string/array $key_id 数据条件字段值 如果是字符形式需要 英文半角逗号间隔主键数字 例如: 1,2,3 获取$key_id 为 1,2,3 的 $key_name 值
    * @param string $field_name 需要获取的字段名称
    * @return array $return 获取到的字段值 形式如 array(1=>'aaa',2=>'bbb',3=>'ccc')
    */    
    protected static function get_field_value_by_id($table_name,$key_name,$key_id,$field_name) {
        if(is_array($key_id) && !empty($key_id)){
            $key_ids = implode('_',$key_id);
            if(!isset(self::$field_value[$key_ids])) {
                $limit = count($key_id);
                $return = DB::fetch_all('SELECT '.$key_name.','.$field_name.' FROM '.DB::table($table_name)." WHERE ".$key_name." IN (".dimplode($key_id).") LIMIT ".$limit);
                foreach($return AS $key => $value){
                    $return_tmp[$value[$key_name]] = $value;
                    unset($return[$key]);
                }
                self::$field_value[$key_ids]=$return_tmp;
                $key_id = $key_ids;
            }else{
                $key_id = $key_ids;
            }
        }else{
            if(!isset(self::$field_value[$key_id])) {
                self::$field_value[$key_id]=DB::result_first('SELECT '.$field_name.' FROM '.DB::table($table_name)." WHERE ".$key_name."='".$key_id."' LIMIT 1");
            }
        }
        return self::$field_value[$key_id];
    }

    /*
    * 获取多个表字段的多个数据值
    * @author yangw <2441069162@qq.com>
    * @copyright (c) 2017-11-06 api.bozedu.net $
    * @version 1.0
    * @param string $table_name 数据所在表名称 必填
    * @param string/array $condition 获取数据条件 必填 如 $condition="a=1"; $condition=array("a=1","b=2");
    * @param string $field_name 需要获取的字段名称 必填 如果是多个 英文半角逗号间隔字段名称 例如: $field_name = 'field1,field2,field3';
    * @param int $limit 限制获取条数
    * @return array $return 获取到的字段值 形式如 array(0=>array('field1'=>'aaa'),2=>array('field2'=>'bbb'),3=>array('field3'=>'ccc'))
    */ 
    protected static function get_field_value_by_mids($table_name, $condition, $field_name, $limit = '') {
        //DEBUG 处理 WHERE 条件
        if (empty($condition)) {
            $where = '';
        } elseif (is_array($condition)) {
            $where = DB::implode($condition, ' AND ');
        } else {
            $where = $condition;
        }
        if ($where) {
            $where = " WHERE " . trim($where);
        }
        if ($limit) {
            $limit = " LIMIT " . $limit;
        }
        $return_key = $table_name . '_' . $where;
        if ($table_name && $field_name) {
            if (!isset(self::$field_value_mids[$return_key])) {
                $sql = 'SELECT ' . $field_name . ' FROM ' . DB::table($table_name) . " " . $where . " " . $limit;
                $return = DB::fetch_all($sql);
                self::$field_value_mids[$return_key] = $return;
            }
        }
        return self::$field_value_mids[$return_key];
    }
}
?>