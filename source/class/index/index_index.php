<?php
/**
* @filename source_class.php 2013-11-11 02:10:56x
* @author Huming Xu <huming17@126.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
	exit('Access Denied');
}
class index_index{
	public $info_array = array();
	public $page_array = array();
	public $tree_array = array();

	static function &instance() {
		static $object;
		if(empty($object)) {
			$object = new self();
		}
		return $object;
	}
	/*
	*	获取一条信息
	*/
	public function one_info($table_name,$key_name,$key_id='') {
		$info = array();
		if($table_name && $key_name && $key_id){
			$sql = "SELECT * FROM ".DB::table($table_name)." WHERE ".$key_name."='".$key_id."' LIMIT 1";
			$info = DB::fetch_first($sql);	
		}
		return $info;
	}
    
	/*
	*  添加信息
	*/
	public function add($table_name,$insert_data=array()) {
        $insert_id = '';
        if($table_name && $insert_data){
            $insert_id = DB::insert($table_name,$insert_data,true);
        }
        return $insert_id;
	}
    
	/*
	*  编辑信息
	*/
	public function edit($table_name,$update_data,$where) {
        //DEBUG 定义返回 消息格式与消息代码编号
        $effect_row='';
        if($update_data && $where){
            $effect_row = DB::update($table_name,$update_data,$where);
        }
        return $effect_row;
	}
    
	/*
	*  删除信息
	*/
	public function delete($table_name,$where) {
        $return='';
        if($table_name && $where){
            $return = DB::delete($table_name, $where, $limit=1);
            //TODO 逻辑删需要创建isdelete字段 0=未删除 1=已删除
            //DB::update($table_name,array('isdelete'=>1), $table_name, $limit=1);
        }
        return $return;
    }
	/*
	*  列表信息
    *
    * @param $table_name string.
    * @param $page_condition array.
    *
    * @return array
	*/
	public function index($table_name,$page_condition){
        $page_result = array();
        $page = $page_condition['page'];
        $limit = $page_condition['limit'];
        $perpage = $page_condition['perpage'];
        $start=(($page-1) * $perpage);
        $wheresql = $page_condition['wheresql'];
        $orderby = $page_condition['orderby'];
        $sql_info = "SELECT * FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql." ".$page_condition['orderby']." ".DB::limit($start, $limit);
        $sql_info_result = DB::fetch_all($sql_info);
        $sql_total_rows = "SELECT count(*) FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql."";
        $sql_total_rows_result = DB::result_first($sql_total_rows);
        //DEBUG 列表数据返回结构
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $sql_total_rows_result,
            //array 一页数据数组
            'page_data' => $sql_info_result 
        );
        return $page_result;
	}

   /**
     * 接口 AJAX或外部请求检索数据
     *
     * @param $table_name string.
     * @param $page_condition array.
     *
     * @return array
     */
	public function api($table_name,$page_condition){
        $page_result = array();
        $page = $page_condition['page'];
        $limit = $page_condition['limit'];
        $perpage = $page_condition['perpage'];
        $start=(($page-1) * $perpage);
        $wheresql = $page_condition['wheresql'];
        $orderby = $page_condition['orderby'];
        $sql_info = "SELECT * FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql." ".$page_condition['orderby']." ".DB::limit($start, $limit);
        $sql_info_result = DB::fetch_all($sql_info);
        $sql_total_rows = "SELECT count(*) FROM ".DB::table($table_name)." WHERE 1=1 ".$wheresql."";
        $sql_total_rows_result = DB::result_first($sql_total_rows);
        //DEBUG 列表数据返回结构
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $sql_total_rows_result,
            //array 一页数据数组
            'page_data' => $sql_info_result 
        );
        return $page_result;
	}
    
	/*
	* ajax 返回信息
	*/
	public function return_json($error,$message,$output=1) {
		$return_array = array(
			"error" => $error,
			"message" => $message
		);
		if($output){
			echo json_ext($return_array);
			die;
		}else{
			return json_ext($return_array);
		}
	}
}
?>