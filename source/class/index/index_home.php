<?php

/**
 * @filename source_module.php 2013-11-11 02:10:56x
 * @author Huming Xu <huming17@126.com>
 * @version 1.0.0
 * @copyright DZF (c) 2013, Huming Xu
 */
class index_home {

    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();

    static function &instance() {
        static $object;
        if (empty($object)) {
            $object = new self();
        }
        return $object;
    }

    public function workDays($year, $month) {
        $day = 1;
        $days = 0;
        $t = mktime(0, 0, 0, $month, $day, $year);
        $days = date('t', $t);
        $fristDayWeek = date('w', $t); //每月一号的星期数
        $lastDayWeek = date('w', mktime(0, 0, 0, $month, $days, $year)); //每月最后一天的星期数
        if ($days > 28) {//非平年二月算法，平年二月无论怎么都只有20天。
            if ($fristDayWeek == 6)//起始日是星期六的减去2天，星期天的减去一天。
                $days-=2;
            if ($fristDayWeek == 0)
                $days-=1;
            if ($lastDayWeek == 6)//结束日是星期六的减去一天，星期天的减去2天。
                $days-=1;
            if ($lastDayWeek == 0)
                $days-=2;
        }

        if ($days < 28)//每个月最少会工作20天，此处修正开始日期是星期六，结束日期是星期天的31天的月份
            $days = 28;
        return $days - 8; // 减去每个月都有的4个星期天共8天
    }
}

?>