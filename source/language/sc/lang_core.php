<?php

$lang = array(
    'username_right' => '用户名正确',
    'username_wrong' => '用户名错误',
    'username_effective' => '用户名有效',
    'username_invalid' => '用户名无效',
    'password_invalid' => '密码无效',
    'username_exist' => '用户名已存在',
    'operation_successful' => '操作成功',
    'operation_failed' => '操作失败,请检查输入是否正确',
    'lock' => '锁定',
    'unlock' => '解锁',
    'info_activation' => 'CN激活码',
    //DEBUG 系统模块
    'no_linux_permission' => '您的服务器不是Linux系统或无权限操作',
    'file_not_exist' => '文件不存在',
    'nextpage' => '下一页',
    'prevpage' => '上一页',
    'pageunit' => '页',
    'total' => '共',
    'date' => array(
            'before' => '前',
            'day' => '天',
            'yday' => '昨天',
            'byday' => '前天',
            'hour' => '小时',
            'half' => '半',
            'min' => '分钟',
            'sec' => '秒',
            'now' => '刚刚',
    ),
    'yes' => '是',
    'no' => '否',
    'weeks' => array(
            1 => '周一',
            2 => '周二',
            3 => '周三',
            4 => '周四',
            5 => '周五',
            6 => '周六',
            7 => '周日',
    )
);
?>