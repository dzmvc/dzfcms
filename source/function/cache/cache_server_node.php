<?php

/**
 * 基本服务器节点配置缓存
 * @author HumingXu E-mail:huming17@126.com
 */
function build_cache_server_node() {
    global $_G;

    $skipkeys = array('backupdir', 'custombackup');
    $serialized = array('');//序列化cache value
    $data = array();
    list($data['server_node']) = get_cachedata_server_node();
    //DEBUG 获取描述以及创建修改时间
    $server_node_info = DB::fetch_first("SELECT * FROM ".DB::table('common_syscache')." WHERE cname='server_node' LIMIT 1");
    savecache('server_node', $data['server_node'], $server_node_info);
}

/*
* 获取服务器表数据 缓存至 server_node
* @return array server_node
*/
function get_cachedata_server_node(){
    $sys_all_server_node_results = array();
    $sys_all_server_node_sql = "SELECT * FROM " . DB::table('server_node') . " AS sn WHERE sn.isdelete = 0";
    $sys_all_server_node_results = DB::fetch_all($sys_all_server_node_sql);
    foreach($sys_all_server_node_results AS $key => $value){
        $data['server_node'][$value['sn_type']][$value['sn_id']]=$value;
    }
    return array($data['server_node']);
}
?>