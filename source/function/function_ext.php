<?php

function gpc($name, $w = 'GPC', $default = '') {
    $i = 0;
    $w = strtoupper($w);
    for($i = 0; $i < strlen($w); $i++) {
        if($w[$i] == 'G' && isset($_GET[$name])) return $_GET[$name];
        if($w[$i] == 'P' && isset($_POST[$name])) return $_POST[$name];
        if($w[$i] == 'C' && isset($_COOKIE[$name])) return $_COOKIE[$name];
    }
    return $default;
}

/**
 * 密码加密函数
 *
 * @param string $password
 * @return string $encode_password
 * @author HumingXu E-mail:huming17@126.com
 */
function encode_password($password) {
    $encode_password = '';
    $encode_password = sha1(md5($password));
    return $encode_password;
}

/**
 * 获取分类名称函数
 *
 * @param int $info_cateid
 * @return string $cate_title
 * @author HumingXu E-mail:huming17@126.com
 */
function get_title_by_info_cateid($info_cateid) {
    $cate_title = '';
    if (!empty($info_cateid)) {
        $cate_title_sql = "SELECT title FROM " . DB::table('content_cate') . " WHERE info_cateid='" . $info_cateid . "' LIMIT 1";
        $cate_title = DB::result_first($cate_title_sql);
    }
    return $cate_title;
}

/**
 * 页面跳转函数
 *
 * @param int $info_cateid
 * @return string $cate_title
 * @author HumingXu E-mail:huming17@126.com
 */
function zshowmessage($message, $url_forward = '', $values = array(), $extraparam = array(), $custom = 0) {
    @header("location: " . $url_forward);
}

//DEBUG 获取模版列表数组
function get_templates() {
    $tpldir_array = array();
    $tpldir_path = SITE_ROOT . 'template';
    $tpldir_array = directoryToArray($tpldir_path, false, true, false);
    foreach ($tpldir_array AS $key => $value) {
        $tpldir_array[$key] = str_ireplace($tpldir_path, '', $value);
        $tpldir_array[$key] = str_ireplace(array('\\', '/'), '', $tpldir_array[$key]);
    }
    return $tpldir_array;
}

/*
 * HTML 中返回第一个图片
 * get_first_imgpath_from_html
 * @pram string $html
 * @return string img src
 * @author xuhm
 */

function get_first_imgpath_from_html($html) {
    $matches = array();
    preg_match_all("/<img(.*)(src=\"[^\"]+\")[^>]+>/isU", $html, $matches);
    return $matches[2][0];
}

/*
 * 后台信息分类数据数组树格式化函数
 * @pram 一维 array $array
 * @return 一维 $array tree
 */
function dbarr2tree($tree, $rootId = 0) {
    $return = array();
    foreach ($tree as $leaf) {
        if ($leaf['info_catepid'] == $rootId) {
            foreach ($tree as $subleaf) {
                if ($subleaf['info_catepid'] == $leaf['info_cateid']) {
                    $leaf['children'] = dbarr2tree($tree, $leaf['info_cateid']);
                    break;
                }
            }
            $return[] = $leaf;
        }
    }
    return $return;
}

/*
 * 后台分类列表选择树HTML格式化UL格式函数
 * @pram array $array tree
 * @return html ul
 */
function tree2htmlul($tree, $relate) {
    if ($relate == 1) {
        $bringback_id = 'relate_info_cateid';
        $bringback_title = 'title2';
    } else {
        $bringback_id = 'info_cateid';
        $bringback_title = 'title';
    }
    $tree2htmlul .= '<ul>';
    foreach ($tree as $leaf) {
        $tree2htmlul .= "<li><a href='javascript:' onclick=\"$.bringBack({" . $bringback_id . ":'" . $leaf['info_cateid'] . "', " . $bringback_title . ":'" . $leaf['title'] . "'})\">" . $leaf['title'] . "</a>";
        if (!empty($leaf['children'])) {
            $tree2htmlul .= tree2htmlul($leaf['children'], $relate);
        }
        $tree2htmlul .= '</li>';
    }
    $tree2htmlul .= '</ul>';
    return $tree2htmlul;
}

/*
 * 后台分类列表选择树HTML格式化UL格式函数 用户信息分类检索
 * @pram array $array tree
 * @return html ul
 */
function tree2htmlul_infosearch($tree, $tree_class) {
    $tree2htmlul .= '<ul class="' . $tree_class . '">';
    foreach ($tree as $leaf) {
        $tree2htmlul .= "<li><a href='admin.php?mod=info&action=index&do=info_ajax&info_cateid=" . $leaf['info_cateid'] . "' target='ajax' rel='jbsxBox'>" . $leaf['title'] . "</a>";
        if (!empty($leaf['children'])) {
            $tree2htmlul .= tree2htmlul_infosearch($leaf['children']);
        }
        $tree2htmlul .= '</li>';
    }
    $tree2htmlul .= '</ul>';
    return $tree2htmlul;
}

/*
 * 后台分类列表选择树HTML格式化UL格式函数 用户信息分类检索
 * @pram array $array tree
 * @return html ul
 */
function tree2htmlul_catesearch($tree, $tree_class) {
    $tree2htmlul .= '<ul class="' . $tree_class . '">';
    foreach ($tree as $leaf) {
        $tree2htmlul .= "<li><a href='admin.php?mod=info_cate&action=index&do=cate_ajax&info_cateid=" . $leaf['info_cateid'] . "' target='ajax' rel='jbsxBox'>" . $leaf['title'] . "</a>";
        if (!empty($leaf['children'])) {
            $tree2htmlul .= tree2htmlul_infosearch($leaf['children']);
        }
        $tree2htmlul .= '</li>';
    }
    $tree2htmlul .= '</ul>';
    return $tree2htmlul;
}

//判断奇数，是返回TRUE，否返回FALSE
function is_odd($num) {
    return (is_numeric($num) & ($num & 1));
}
//判断偶数，是返回TRUE，否返回FALSE
function is_even($num) {
    return (is_numeric($num) & (!($num & 1)));
}

/**
 * 判断当前系统是否类linux
 * @author xuhm
 * @return int
 *  ex: 1 = 是类linux
 *      2 = 非类linux
 *      3 = 未知系统
 */
function is_unix() {
    $return_int = 3;
    $os_str = php_uname();
    if (strpos($os_str, 'Ubuntu')) {
        $return_int = 1;
    } elseif (strpos($os_str, 'inux')) {
        $return_int = 1;
    } else {
        $return_int = 2;
    }
    return $return_int;
}

/**
 * 检查升级包是否有效
 * @author xuhm
 * @return array
 *  ex: array(
 *          'status' => '1 成功 2 失败或无效',
 *          'data_cache_pku' => '' //更新包解压目录,
 *          'patch' => array() // 更新包XML信息
 *      )
 */
function check_pku($update_package_path) {
    $return_array = array(
        'status' => '2',
        'data_cache_pku' => '',
        'patch' => array()
    );
    //DEBUG 安装包缓存目录
    $data_cache_pku = SITE_ROOT . './data/cache/pku_' . random(4);
    if (!empty($update_package_path) && file_exists($update_package_path)) {
        if (is_dir($data_cache_pku)) {
            $data_cache_pku = SITE_ROOT . './data/cache/pku_' . random(8);
        }
        dmkdir($data_cache_pku);
        //DEBUG 执行解压
        if ($data_cache_pku) {
            //执行PHP ZipArchive 解压
            $zip = new ZipArchive;
            if ($zip->open($update_package_path) === true) {
                $zip->extractTo($data_cache_pku);
                $zip->close();
            }
            //解析更新包XML
            //DEBUG 1.获取XML版本信息 以及升级环境要求
            $upgrade_xml = realpath($data_cache_pku . '/upgrade.xml');
            $upgrade_upgradelist = realpath($data_cache_pku . '/patch_upgradelist.txt');
            //DEBUG 2.检查版本是否匹配
            if (file_exists($upgrade_xml) && file_exists($upgrade_upgradelist)) {
                $upgrade_xml_content = file_get_contents($upgrade_xml);
                $zip_upgradelist_content = file_get_contents($upgrade_upgradelist);
                //DEBUG 3.获取文件列表 检测文件
                if (!empty($upgrade_xml_content) && !empty($zip_upgradelist_content)) {
                    $upgrade_xml_array = xml2array($upgrade_xml_content);
                    //DEBUG 4.检测版本是否符合和升级包对应 检测 $project_id 和 $latestversion 和 $latestrelease 是当前系统是否比配 暂未加限制 以免混淆
                    //DEBUG 5.检查更新文件是否完整(检测文件列表文件MD5是否有效 由于二进制 和 ASCII网络传输后,同样文件产生的MD5不一致,因此上传方式MD5检测机制暂未添加)
                    //DEBUG 6.返回更新包信息
                    if (!empty($upgrade_xml_array['patch']['latestversion']) && !empty($upgrade_xml_array['patch']['latestrelease'])) {
                        $return_array['status'] = 1;
                        $return_array['data_cache_pku'] = $data_cache_pku;
                        $return_array['patch'] = $upgrade_xml_array['patch'];
                    }
                }
            }
        }
    }
    return $return_array;
}

/**
 * 获取URL上的ID参数
 * @author xuhm
 * @return string ajax_url_href
 */

function set_ajax_url_href($url = ''){
    //DEBUG 处理 index.php?mod=index&action=info&do=detail&id=7
    //DEBUG 处理 index.php?mod=index&action=cate&do=list&id=125
    $return_ajax_url_href = '';
    $url_array = explode('=', $url);
    $url_array_num = count($url_array);
    if ($url_array[($url_array_num - 2)] == 'detail&id') {
        $return_ajax_url_href = "javascript:info_detail('" . $url_array[($url_array_num - 1)] . "')";
    } elseif ($url_array[($url_array_num - 2)] == 'list&id') {
        $return_ajax_url_href = "javascript:waterfall('ajax','" . $url_array[($url_array_num - 1)] . "','',1);";
    } else {
        $return_ajax_url_href = $url;
    }
    return $return_ajax_url_href;
}

/*
 * 缓存用户权限菜单数据数组树格式化函数
 * @pram 一维 array $array
 * @return 一维 $array tree
 */
function menuarr2tree($tree, $rootId = 0) {
    $return = array();
    foreach ($tree as $leaf) {
        if ($leaf['menu_pid'] == $rootId) {
            foreach ($tree as $subleaf) {
                if ($subleaf['menu_pid'] == $leaf['menu_id']) {
                    $leaf['submenu'] = menuarr2tree($tree, $leaf['menu_id']);
                    break;
                }
            }
            $return[$leaf['menu_id']] = $leaf;
        }
    }
    return $return;
}

function writetojscache() {
    $dir = DZF_ROOT . 'static/js/';
    $dh = opendir($dir);
    $remove = array(
        array(
            '/(^|\r|\n)\/\*.+?\*\/(\r|\n)/is',
            "/([^\\\:]{1})\/\/.+?(\r|\n)/",
            '/\/\/note.+?(\r|\n)/i',
            '/\/\/debug.+?(\r|\n)/i',
            '/(^|\r|\n)(\s|\t)+/',
            '/(\r|\n)/',
        ), array(
            '',
            '\1',
            '',
            '',
            '',
            '',
        ));
    while (($entry = readdir($dh)) !== false) {
        if (fileext($entry) == 'js') {
            $jsfile = $dir . $entry;
            $fp = fopen($jsfile, 'r');
            $jsdata = @fread($fp, filesize($jsfile));
            fclose($fp);
            $jsdata = preg_replace($remove[0], $remove[1], $jsdata);
            if (@$fp = fopen(DZF_ROOT . './data/cache/' . $entry, 'w')) {
                fwrite($fp, $jsdata);
                fclose($fp);
            } else {
                exit('Can not write to cache files, please check directory ./data/ and ./data/cache/ .');
            }
        }
    }
}

/*
 * 函数说明 URL登录功能函数
 * @author xuhm
 * @pram username 用户名
 * @password 用户名密码 md5(32位)后的密文
 * @return 无
 */
function url_login() {
    //DEBUG 模拟登录
    $user_name = isset($_GET['user_name']) ? $_GET['user_name'] : '';
    $user_password = isset($_GET['user_password']) ? $_GET['user_password'] : '';
    $url = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
    $replace_pram = 'user_name=' . $user_name . '&user_password=' . $user_password;
    $location_url = str_replace($replace_pram, '', $url);
    if (!empty($user_name) && !empty($user_password)) {
        /*
         * url pram add user_name=teacher1&user_password=e10adc3949ba59abbe56e057f20f883e
         * eg: home.php?user_name=teacher1&user_password=e10adc3949ba59abbe56e057f20f883e
         */
        $member = DB::fetch_first("SELECT user_id,user_password from " . DB::table('user') . " WHERE user_name='" . $user_name . "' LIMIT 1");
        if (empty($member)) {
            $member = DB::fetch_first("SELECT user_id,user_password from " . DB::table('user') . " WHERE user_id='" . $user_name . "' LIMIT 1");
        }
        if ($member['user_password'] && $member['user_id']) {
            $user_id = $member['user_id'];
            //校验密码 是否正确
            $uc_password = sha1($user_password);
            if ($uc_password == $member['user_password']) {
                //loaducenter();
                //$ucsynlogin = uc_user_synlogin($uid);
                $user['user_id'] = $user_id;
                ext::synlogin($user, $user);
                header('location:' . $location_url . '');
                die;
            }
        }
    }
}

/*
 * 函数说明 根据树形节点的 menu_id 获取该节点路径
 * @author xuhm
 * @pram $data array
 * @return Array(
  [0] => Array(
  [menu_id] => 1021
  [menu_pid] => 1002
  ......
  [submenu] => Array(
  [1025] => Array(
  [menu_id] => 1025
  [menu_pid] => 1021
  ......
  )
  [1026] => Array(
  [menu_id] => 1026
  [menu_pid] => 1021
  ......
  )
  [1027] => Array(
  [menu_id] => 1027
  [menu_pid] => 1021
  ......
  )
  )
  )
  )
 */

function get_treepath($tree, $level = 0, $current_menu_id, $treepath = array('stop' => 0)) {
    foreach ($tree as $leaf) {
        if ($treepath['stop'] == 1) {
            return $treepath;
            break;
        }
        if (empty($treepath['stop'])) {
            if ($current_menu_id == $leaf['menu_id']) {
                if (0 == $leaf['menu_pid']) {
                    break;
                } else {
                    $treepath[$level] = $leaf;
                    $treepath = get_treepath($tree, $level + 1, $leaf['menu_pid'], $treepath);
                }
            }
        }
    }
    return $treepath;
}

/*
 * 缓存用户权限菜单数据数组树格式化函数
 * @pram 一维 array $array
 * @return 一维 $array tree
 */
function menuarr2tree2($tree, $rootId = 0) {
    $return = array();
    foreach ($tree as $leaf) {
        if ($leaf['menu_pid'] == $rootId) {
            foreach ($tree as $subleaf) {
                if ($subleaf['menu_pid'] == $leaf['menu_id']) {
                    $leaf['submenu'] = menuarr2tree($tree, $leaf['menu_id']);
                    break;
                }
            }
            $return[$leaf['menu_id']] = $leaf;
        }
    }
    return $return;
}

/*
 * 加载任意业务模块方法
 */
function auto_mad($mod, $action, $do, $data=array()){
    //加载模块主对象
    $action_class_path = libfile($mod . '/' . $action, 'class', '..');
    if (file_exists($action_class_path)) {
        require_once $action_class_path;
        //$action_class_name = $mod.'_'.$action;
        //$action_class = new $action_class_name();
    }
    //加载模块业务对象
    require_once libfile($mod . '/' . $action, 'module', '..');
    $mod_action_class = 'ctrl_' . $mod . '_' . $action;
    if ($mod && $action && $do && class_exists($mod_action_class)) {
        $mod_action = new $mod_action_class();
        $ctrl_name = 'do_' . $do;
        $result = $mod_action->$ctrl_name($mod, $action, $do, $data);
    } else {
        $result = '';
    }
    return $result;
}

/*
 * 函数说明 ajax 通用返回函数
 * @author DZF
 * @pram $data array 输出返回数据内容
 * @pram $api string json\layui\xls\xml\html 格式的统一输出于处理 默认json
 * @pram $mod string 模块 默认空
 * @pram $action string 功能 默认空
 * @pram $do string 事件 默认空
 * @pram $client 终端 wap\web\ios\android\h5
 * @pram $lang_name string 指定语言包文件名称  默认 wap
 * @pram $lang_key string 指定语言名称key 默认空$form_name
 * @pram $form_name string 指定数据库表名称 默认空
 * @pram $ep array 扩展参数支持 option2name=1 需要转换data option_id 为 option_name 并追加输出
 * @return string 
 */

function format_data($data, $api = 'json', $mod = '', $action = '', $do = '', $client = 'wap', $lang_name = 'msg', $lang_key = '', $form_name = '', $ep = array()) {
    global $_G;
    //设置接口状态与信息
    $format_return['code'] = $data['code'];
    $format_return['param'] = $data['param'] ? $data['param'] : '';
    $format_return['template'] = $data['template'] ? $data['template'] : '';
    if ($data['msg']) {
        $format_return['msg'] = $data['msg'];
    }else{
        if ($data['code'] != '') {
            if (empty($lang_key)) {
                $format_return['msg'] = lang($lang_name, $mod . '_' . $action . '_' . $do . '_' . $data['code']);
            } else {
                $format_return['msg'] = lang($lang_name, $lang_key . '_' . $data['code']);
            }
        } else {
            $format_return['msg'] = $data['code'];
        }
    }
    if(empty($format_return['msg'])){
        if($data['code'] == '1'){
            $format_return['msg'] = lang($lang_name, 'api_code_1');
        }else{
            $format_return['msg'] = lang($lang_name, 'api_code_0');
        }
    }
    //设置接口结构(表结构)
    if($api !='json'){
        $ep['option2name'] = 1;//layui\xls\html 情况下需要返回数据结构
    }
    if(empty($format_return['template']['hidden_structure']) && $ep['option2name']){
        if(empty($form_name)){
            $data['data']['table_name'] =$mod.'_'.$action;
        }
        if(empty($data['data']['table_title'])){
            $data['data']['table_title'] = get_table_title($data['data']['table_name']);
        }
        if(empty($data['data']['table_structure'])){
            $data['data']['table_structure'] = get_table_structure($mod, $action);
        }
        if(empty($data['data']['table_structure']['field'])){
            $template_data = template_data($data['data']['table_name']);
            $data['data']['table_title'] = $template_data['table_title']?$template_data['table_title']:'';
            $data['data']['table_structure'] = $template_data['table_structure']?$template_data['table_structure']:'';
        }
        //unset($format_return['template']);
    }

    //设置接口数据
    $format_return['data'] = $data['data'];
    if (empty($data['data'])) {
        $format_return['data'] = (object) array();
    } else {
        $format_return['data'] = $data['data'];
    }
    $api_debug = !empty($_REQUEST['api_debug']) ? $_REQUEST['api_debug']:'';
    if($api_debug==1){
        $format_return['request']=$_REQUEST;
    }
    //输出数据格式化与校验 开始
    switch ($_G['do']) {
        case "index":
            //校验
            //列表状态转换 开始
            $page_data = array();
            //提取表结构option数组
            foreach($format_return['data']['page_data'] AS $key => $value){
                foreach ($value AS $k => $v) {
                    if(!empty($data['data']['table_structure']['field'][$k]['option_array'])){
                        $v_array = explode(',', $v);
                        $tmp_option = array();
                        foreach ($v_array AS $kk => $vv) {
                            if (isset($data['data']['table_structure']['field'][$k]['option_array'][$vv])) {
                                $tmp_option[$vv] = $data['data']['table_structure']['field'][$k]['option_array'][$vv];//option id 对应名称
                            }else{
                                $tmp_option[$vv] = $vv;
                            }
                        }
                        $value[$k]= implode(',', $tmp_option);
                        $value[$k.'_option_n']= implode(',', $tmp_option);
                        $value[$k.'_option_k']= implode(',', array_keys($tmp_option));
                    }
                }
                $page_data[]=$value;
            }
            $format_return['data']['page_data']=$page_data;
            //列表状态转换 结束
            break;
        case "add":
            //校验
            break;
        case "edit":
            //校验
            break;
        case "delete":
            //校验
            break;
    }
    //输出数据格式化与校验 结束
    switch ($api) {
        case "json":
            @header("Content-type: application/json; charset=utf-8");//头部输出
            //NULL数据处理
            if(isset($format_return['data']) && !empty($format_return['data'])){
//                foreach($format_return['data'] AS $key => $value){
//                    if(!is_array($value) && empty($value) && $value !='0'){
//                        $format_return['data'][$key]='';
//                    }
//                }
//                if(isset($format_return['data']['one_info']) && $format_return['data']['one_info']){
//                    foreach($format_return['data']['one_info'] AS $key => $value){
//                        if(empty($value) && $value !='0'){
//                            $format_return['data']['one_info'][$key]='';
//                        }
//                    }
//                }
//                if(isset($format_return['data']['table_name'])){
//                    unset($format_return['data']['table_name']);
//                }
//                if(isset($format_return['data']['table_title'])){
//                    unset($format_return['data']['table_title']);
//                }
//                if(isset($format_return['data']['table_structure'])){
//                    unset($format_return['data']['table_structure']);
//                }
            }
            echo json_ext($format_return);
            die;
            break;
        case "layui"://layui特殊格式JSON API 请求
            @header("Content-type: application/json; charset=utf-8");//头部输出
            $table_data = $tmp = array();
            //全列表隐藏HTML
            foreach ($format_return['data']['page_data'] AS $key => $value) {
                $tips_html = "";
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if($format_return['data']["table_structure"]["table_key"]!=$k){
                        $first_k = $k;
                    }
                    $tmp[$k] = empty($value[$k]) ? '' : $value[$k];
                    $tips_html .= $v.":".$value[$k].'<br />';
                }
                //$tips_html = "<div style='display:none;' class='layui_tips_html'>".$tips_html."</div>";
                //$tmp[$first_k]=$tmp[$first_k].$tips_html;
                $first_k = '';
                $table_data[] = $tmp;
            }
            $layui = array(
                'code'=>'0',
                'msg'=>'',
                'count'=>$format_return['data']['total_rows'],
                'data'=>$table_data
            );
            echo json_ext($layui);
            die;
            break;
        case "xml":
            @header("Content-type: application/xml");//头部输出
            $return = array2xml($format_return);
            //DEBUG XML 解析
            //$return_array = xml2array($return);
            //TODO 方法二 反向解析XML为数组 此方法位置DOM报错
            //$return = arrayxml::createXML('root', $data);
            //$result = arrayxml::xml2array($result,0);
            break;

        case "html":
//            $table_name = $_G['mod'].'_'.$_G['action'];
//            $template_data = template_data($table_name);
//            $template_data = array_merge($template_data,$page_result);
            if ($api_debug) {
                echo json_ext($format_return);
                die;
            }
            include template('global/' . $_G['do']);
            die;
            break;
        case "xls":
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            foreach($format_return['data']['page_data'] AS $key => $value){
                $tmp = array();
                //字段和数值做对应排序后导出
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(isset($format_return['data']['table_title'][$k])){
                        $tmp[$k]=$value[$k];
                    }
                }
                $excel_data[]=$tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
            break;
    }
    if ($_G['config']['debug_log']) {
        @file_put_contents(SITE_ROOT . './data/debug/input_log_' . date('Ymd') . '.txt', 'RECEIVE : ' . date('Y-m-d H:i:s') . ' ' . serialize($_REQUEST) . PHP_EOL, FILE_APPEND);
        @file_put_contents(SITE_ROOT . './data/debug/input_log_' . date('Ymd') . '.txt', 'RETURN : ' . date('Y-m-d H:i:s') . ' ' . $return . PHP_EOL, FILE_APPEND);
    }
    return $format_return;
}

/*
 * 函数说明 单个数据通用输出格式化
 * @author DZF
 * @pram $data array 单个数据
 * @return rray 输出返回数据内容 
 */
function format_info($info, $api = 'json', $mod = '', $action = '', $do = '', $client = 'wap', $lang_name = 'msg', $lang_key = '', $form_name = '', $ep = array()){
    global $_G;
    if(empty($form_name)){
        $data['data']['table_name'] =$mod.'_'.$action;
    }
    if(empty($data['data']['table_title'])){
        $data['data']['table_title'] = get_table_title($data['data']['table_name']);
    }
    if(empty($data['data']['table_structure'])){
        $data['data']['table_structure'] = get_table_structure($mod, $action);
    }
    if(empty($data['data']['table_structure']['field'])){
        $template_data = template_data($data['data']['table_name']);
        $data['data']['table_title'] = $template_data['table_title']?$template_data['table_title']:'';
        $data['data']['table_structure'] = $template_data['table_structure']?$template_data['table_structure']:'';
    }
    //列表状态转换 开始
    $page_data = array();
    //提取表结构option数组
    foreach ($info AS $k => $v) {
        if(!empty($data['data']['table_structure']['field'][$k]['option_array'])){
            $v_array = explode(',', $v);
            $tmp_option = array();
            foreach ($v_array AS $kk => $vv) {
                if (isset($data['data']['table_structure']['field'][$k]['option_array'][$vv])) {
                    $tmp_option[$vv] = $data['data']['table_structure']['field'][$k]['option_array'][$vv];//option id 对应名称
                }else{
                    $tmp_option[$vv] = $vv;
                }
            }
            $info[$k]= implode(',', $tmp_option);
            $info[$k.'_option_n']= implode(',', $tmp_option);
            $info[$k.'_option_k']= implode(',', array_keys($tmp_option));
        }
    }
    return $info;
}

function get_rest_time($time_s, $time_n) {
    $strtime = '剩余';
    $time = $time_n - $time_s;
    if ($time >= 86400) {
        return '截止' . date('Y-m-d', $time_n);
    }
    if ($time >= 3600) {
        $strtime .= intval($time / 3600) . '小时';
        $time = $time % 3600;
    } else {
        $strtime .= '';
    }
    if ($time >= 60) {
        $strtime .= intval($time / 60) . '分';
        $time = $time % 60;
    } else {
        $strtime .= '';
    }
    if ($time > 0) {
        $strtime .= intval($time) . '秒';
    } else {
        $strtime = "时间错误";
    }
    return $strtime;
}

function get_role() {
    $role = DB::fetch_all("SELECT role_name,role_id FROM " . DB::table("user_role") . " WHERE isdelete=0");
    foreach ($role as $key => $value) {
        $data[$value['role_id']] = $value['role_name'];
    }

    return $data;
}

# 获取资产分类

function get_asset_cate() {
    $asset_cate = DB::fetch_all("SELECT ac_name,ac_code FROM " . DB::table("asset_cate") . " WHERE isdelete=0");
    foreach ($asset_cate as $key => $value) {
        $data[$value['ac_code']] = $value['ac_name'];
    }

    return $data;
}

# 获取资产资金来源

function get_asset_money_from() {
    $asset_money_from = DB::fetch_all("SELECT amf_name,amf_id FROM " . DB::table("asset_money_from") . " WHERE isdelete=0");
    foreach ($asset_money_from as $key => $value) {
        $data[$value['amf_id']] = $value['amf_name'];
    }

    return $data;
}

# 获取资产增加方式

function get_asset_from() {
    $asset_money_from = DB::fetch_all("SELECT af_name,af_id FROM " . DB::table("asset_from") . " WHERE isdelete=0");
    foreach ($asset_money_from as $key => $value) {
        $data[$value['af_id']] = $value['af_name'];
    }

    return $data;
}

# 获取用户姓名

function realname($uid) {
    return DB::result_first("SELECT user_realname FROM " . DB::table("user") . " WHERE user_id='" . $uid . "' LIMIT 1");
}

# 获取教师/办公室

function get_platform_room($pid = 0) {
    $data = DB::fetch_all("SELECT * FROM " . DB::table("platform_room") . " WHERE pr_pid='" . $pid . "' AND isdelete=0");
    foreach ($data as $key => $value) {
        $depart[$value['pr_id']] = $value['pr_name'];
    }
    return $depart;
}

function get_platform_room_all($ap_id){
    $data = DB::fetch_first("SELECT * FROM " . DB::table("platform_room") . " WHERE pr_id='" . $ap_id . "' AND isdelete=0");
    $room_name[1] = $data['pr_name'];
    $room_name[0] = DB::result_first("SELECT pr_name FROM " . DB::table("platform_room") . " WHERE pr_id='" . $data['pr_pid'] . "' AND isdelete=0");
    if(empty($room_name[0])){
        return $room_name[1];
    }else{
        return $room_name[0].' > '.$room_name[1];
    }
}

# 生成资产二维码

function create_qrcode($code) {
    require_once '../plugins/phpqrcode/phpqrcode.php';
    //二维码内容
    $value = $code;
    //容错级别
    $errorCorrenctionLevel = 'l';
    //二维码图片大小
    $matrixPointSize = 50;
    //生成二维码
    Qrcode::png($value, '../data/upload/qrcode/' . $code . '.png', $errorCorrenctionLevel, $matrixPointSize, 2);
    //图片logo加入二维码
    $qrcode = 'qrcode.png';
    $qrcode = imagecreatefromstring(file_get_contents($qrcode));
}

function show_room_qrcode($file_name,$data){
    require_once '../plugins/phpqrcode/phpqrcode.php';

    $errorCorrenctionLevel = 'l';
    //二维码图片大小
    $matrixPointSize = 10;
    //生成二维码
    $file_name_ori = '../data/upload/qrcode/'.$file_name. '.png';
    //$file_name = '../data/upload/qrcode/' .iconv("UTF-8", "GB2312//IGNORE", $file_name). '.png';
    $file_name = '../data/upload/qrcode/' . $file_name. '.png';

    Qrcode::png($data, $file_name, $errorCorrenctionLevel, $matrixPointSize, 2);

    return $file_name_ori;
}

function characet($data) {
    if (!empty($data)) {
        $fileType = mb_detect_encoding($data, array('UTF-8', 'GBK', 'LATIN1', 'BIG5'));
        if ($fileType != 'UTF-8') {
            $data = mb_convert_encoding($data, 'utf-8', $fileType);
        }
    }
    return $data;
}

function get_weixin_info() {
    $wxuser_json = urldecode(base64_decode(getcookie('wxuser')));
    $wxuser_json = json_decode($wxuser_json, true);
    return $wxuser_json;
}

function gwt_weixin_bind_statue($openid) {
    if ($openid) {
        $bind = DB::fetch_first("SELECT * FROM " . DB::table("user") . " WHERE openid='" . $openid . "' LIMIT 1");
        $user['user_id'] = $bind['user_id'];
        return $user;
    }
}

function get_asset_name_by_code($ag_code){
    return DB::result_first("SELECT al.al_name FROM ".DB::table("asset_gps")." as ag,".DB::table("asset_list")." as al WHERE ag.ag_code='".$ag_code."' AND ag.al_id=al.al_id limit 1");
}

# 消息中心

function jx_messenger($mod = 'default', $content, $link_id, $uids) {
    global $_G;

    # tag
    $data['mc_uid'] = $_G['user_id'];
    $data['mc_mod'] = $mod;
    $data['mc_content'] = $content;
    $data['mc_link'] = $link_id;//需要处理
    $data['mc_read'] = 0;//未读
    $data['mc_tag'] = date("YmdHis") . '#' . rand(1000,9999);
    $data['create_dateline'] = time();
    $data['isdelete'] = 0;

    switch ($mod) {
        case 'teadoc':
            $data['mc_link'] = 'index.php?mod=teadoc&action=index&do=tree&user_id='.$_G['user_id'].'&ti_id='.$link_id;
            break;
        case 'wechat_asset_exchange';
        case 'wechat_asset_exchange_allow_1':
        case 'wechat_asset_exchange_allow_0':
            $data['mc_link'] = 'wechat.php?do=check_asset_code&ag_code='.$link_id;
            break;
        default:
            $data['mc_link'] = '';
            break;
    }
    if(is_array($uids)){
        foreach ($uids as $key => $uid) {
            $data['mc_to_uid'] = $uid;
            DB::insert('messenger_center', $data);
        }
    }else{
        $data['mc_to_uid'] = $uids;
        DB::insert('messenger_center', $data);
    }
}

//获得表的字段名称
function get_table_title($table_name){
    $sql = "SELECT A.* FROM ".DB::table('tools_module_field')." A right join ".DB::table('tools_module_table')." B ON A.tmt_id=B.tmt_id WHERE B.tmt_table_name = '".$table_name."' ORDER BY A.tmf_sort ASC";
    $table_field = DB::fetch_all($sql);
    $table_title = array();
	foreach ($table_field as $key=>$value){
        //增加主键显示
        if($value['tmf_field_list'] == 1 || $value['tmf_filed_key']==1){
            $table_title[$value['tmf_field_name']] = $value['tmf_name'] ;
        }
    }
    return $table_title;
}

//获取表结构JSON
function get_table_structure($mod, $action) {
    global $_G;
    
    # 获取需要隐藏字段的文件
    if(file_exists(SITE_ROOT.'./data/cache/table_field_hidden.json')){
        $table_field_hidden = file_get_contents(SITE_ROOT.'./data/cache/table_field_hidden.json');
        $table_field_hidden = json_decode($table_field_hidden,true);
    }
    $table_structure = array();
    $sql = "SELECT * FROM " . DB::table('tools_module_table') . " WHERE tmt_table_name='" . $mod.'_'.$action . "' LIMIT 1";
    $tools_module_table = DB::fetch_first($sql);
    $sql = "SELECT * FROM " . DB::table('tools_module') . " WHERE tm_id='" . $tools_module_table['tm_id'] . "' LIMIT 1";
    $tools_module = DB::fetch_first($sql);
    $sql = "SELECT * FROM " . DB::table('tools_module_field') . " WHERE tmt_id='" . $tools_module_table['tmt_id'] . "' ORDER BY tmf_sort ASC";
    $tools_module_field = DB::fetch_all($sql);
    if($tools_module && $tools_module_table && $tools_module_field){
        //生成表结构JSON文件 开始
        $module_table = array(
            'table_title'=>$tools_module_table['tmt_name'],
            'description'=>$tools_module_table['tmt_description'],
            'allowdelete'=>$tools_module_table['tmt_allowdelete'],
            'allowadd'=>$tools_module_table['tmt_allowadd'],
            'allowedit'=>$tools_module_table['tmt_allowedit'],
            'allowexport'=>$tools_module_table['tmt_allowexport'],
            'allowimport'=>$tools_module_table['tmt_allowimport']
        );
        $module_table_field = array();
        $tm_field = array();
        $count_air_textarea=0;
        foreach($tools_module_field AS $key => $value){
            if($value['tmf_type']){
                //是否数字 结束
                $tmf_form_hidden = $value['tmf_form_hidden'];
                
                # 根据json判断当前角色数据字段是否需要隐藏
                if($table_field_hidden[$_G['user_role_id']][$mod . '_' . $action][$value['tmf_field_name']]){
                    $tmf_form_hidden = 1;
                }else{
                    $tmf_form_hidden = $value['tmf_form_hidden'];
                }
                $module_table_field[$value['tmf_field_name']] = array(
                    'type'=>$value['tmf_type'],
                    'label'=>$value['tmf_name'],
                    'field_type'=>$value['tmf_field_type'],
                    'tmf_filed_search'=>$value['tmf_filed_search'],
                    'tmf_filed_search_ext'=>$value['tmf_filed_search_ext'],
                    'tmf_filed_search_ext_type'=>$value['tmf_filed_search_ext_type'],
                    'tmf_type'=>$value['tmf_type'],
                    'tmf_type_ext'=>$value['tmf_type_ext'],
                    'tmf_type_group'=>$value['tmf_type_group'],
                    //'option'=>array(),
                    'list_api'=>'',
                    'require'=>(string)$value['tmf_require'],//1是必填 0是非必填
                    'readonly'=>(string)$value['tmf_readonly'],//是否只读 0 否 1是
                    'disabled'=>(string)$value['tmf_disabled'],//是否禁止编辑 0 否 1是
                    'display'=>(string)$value['tmf_display'],//是否隐藏不显示 0 否 1是
                    'description'=>(string)$value['tmf_description'],//字段描述 0 否 1是
                    'tmf_form_hidden'=>(string)$tmf_form_hidden,
                    'foreign_link'=>(string)$value['foreign_link']
                );
                //计算富文本编辑器个数 以便控制只加载一次富文本编辑器样式
                if($value['tmf_type']=='air_textarea'){
                    $count_air_textarea++;
                    $module_table_field[$value['tmf_field_name']]['count_air_textarea']=$count_air_textarea;
                }
                //是否有选项 开始
                if($value['tmf_type_value']){
                    $tmf_type_value = $tmf_type_value_option = array();
                    $tmf_type_value = nl2br(trim($value['tmf_type_value']));
                    $tmf_type_value = explode('<br />', $tmf_type_value);
//                    if(!is_array($tmf_type_value)){
//                        $tmf_type_value = explode('\r', $tmf_type_value);
//                    }
//                    if(!is_array($tmf_type_value)){
//                        $tmf_type_value = explode('\n', $tmf_type_value);
//                    }
//                    if(!is_array($tmf_type_value)){
//                        $tmf_type_value = explode('\r\n', $tmf_type_value);
//                    }
                    $tmf_type_value_option_array = array();
                    foreach($tmf_type_value AS $k => $v){
                        list($option_value,$option_name) = explode('|',trim($v));
                        $tmp = array(
                            'v'=>$option_value,
                            'n'=>trim($option_name)
                        );
                        $tmf_type_value_option[] = $tmp;
                        $tmf_type_value_option_array[$tmp['v']] = $tmp['n'];
                    }
                    $module_table_field[$value['tmf_field_name']]['option']=$tmf_type_value_option;
                    $module_table_field[$value['tmf_field_name']]['option_array']=$tmf_type_value_option_array;
                }
                //是否有选项 结束
                $tm_field[$value['tmf_field_name']] = $value['tmf_field_default'];
            }else{
                //补充主键说明
                if($value['tmf_filed_key']==1){
                    $tmf_filed_key = $value['tmf_field_name'];
                }
            }
        }
        $tm_name = $tools_module_table['tmt_table_name'];
        $table_structure = array(
            'table_brief'=>$module_table,
            'table_key'=>$tmf_filed_key,
            'field'=>$module_table_field,
            'field_default' => $tm_field,
            'count_air_textarea' => $count_air_textarea
        );
    }
    return $table_structure;
}

function menutree($tree,$pid){
    $return = array();
    foreach ($tree as $key => $v){
        if($v['menu_pid'] == $pid){
            $v['son'] = menutree($tree,$v['menu_id']);
            $return[] = $v;
        }
    }
    return $return;
}

function de_apiurl(){
    define('APPDOMAIN','http://localhost/');
}

function cross_domain(){
    header('Access-Control-Allow-Origin:*');
    header('Access-Control-Allow-Methods:GET,POST,PATCH,PUT,OPTIONS');
}

/*
 * 函数说明 获取当前模块下在菜单
 * @author bozedu
 * @pram $data array
 * @return string
 */
function mod_menu($menu_pid){
    global $_G;
    //自动根据参数 获取左侧菜单
    $menu_pid = 21;
    //获取全部菜单
    $sql = "SELECT * FROM ".DB::table('common_menu')." WHERE isdelete=0 AND enable=1";
    $result = DB::fetch_all($sql);
    return $result;
}

//模版数据JSON
function template_data($table_name){
    global $_G;
    //获取表头
    $template_json['table_title'] = get_table_title($table_name);
    //获取数据表结构 与 默认值
    $module_table_json = SITE_ROOT.'source/module/'.$_G['mod'].'/dbtable/'.$table_name.'.json';
    $module_table_array = array();
    if(file_exists($module_table_json)){
        $module_table_array = json_decode(file_get_contents($module_table_json),true);
    }
    $template_json = $module_table_array;
    return $template_json;
}

function get_school_role_menu($schoolid,$user_role_id,$area_id){
    //获取角色 省级 市级 县级 学校 模块菜单 开始
    //2         管理员
    //66	市级管理员
    //67	市级教研员
    //68	市级评审员
    //69	区县管理员
    //70	区县教研员
    //71	区县评审员
    //72	学校管理员
    //73	学校年级组长
    //74	学校学科组长
    //75	学校教师
    //76	学生
    //77	家长
    //echo $schoolid.'_'.$user_role_id.'_'.$area_id;
    switch ($user_role_id) {
        case "2":
            $sql = "SELECT sr_id FROM ".DB::table('school_role_menu')." WHERE isdelete=0 AND area_id1='".$area_id."' LIMIT 100";
            $result = DB::fetch_all($sql);
            break;
        case "66":
        case "67":
        case "68":
            $sql = "SELECT sr_id FROM ".DB::table('school_role_menu')." WHERE isdelete=0 AND area_id2='".$area_id."' LIMIT 100";
            $result = DB::fetch_all($sql);
            break;
        case "69":
        case "70":
        case "71":
            $sql = "SELECT sr_id FROM ".DB::table('school_role_menu')." WHERE isdelete=0 AND area_id3='".$area_id."' LIMIT 100";
            $result = DB::fetch_all($sql);
            break;
        case "72":
        case "73":
        case "74":
        case "75":
            $sql = "SELECT sr_id FROM ".DB::table('school_role_menu')." WHERE isdelete=0 AND schoolid='".$schoolid."' LIMIT 100";
            $result = DB::fetch_all($sql);
            break;
        case "76":
        case "77":
            $sql = "SELECT sr_id FROM ".DB::table('school_role_menu')." WHERE isdelete=0 AND schoolid='".$schoolid."' LIMIT 100";
            $result = DB::fetch_all($sql);
            break;

        default:
            break;
    }
    $school_role_menu = array();
    foreach($result AS $key => $value){
        $school_role_menu[$value['sr_id']] = $value['sr_id'];
    }
    //DEBUG 根据用户角色 获取菜单
    if($user_role_id==76){
        $sql = "SELECT * FROM ".DB::table('school_role')." WHERE sr_id IN (".  dimplode($school_role_menu).") AND user_role_id=76 ORDER BY sr_cate ASC, sr_sort ASC LIMIT 100";
    }else{
        $sql = "SELECT * FROM ".DB::table('school_role')." WHERE sr_id IN (".  dimplode($school_role_menu).") AND user_role_id=75 ORDER BY sr_cate ASC, sr_sort ASC LIMIT 100";
    }
    $result = DB::fetch_all($sql);
    $school_role = array();
    foreach($result AS $key => $value){
        $tmp[$value['sr_cate']][]=$value;
    }
    foreach($tmp AS $key => $value){
        $school_role[] = array(
            'sr_cate'=>$key,
            'sr_cate_name'=>ext::name_var($key),
            'sr_cate_menu'=>$value,
        );
    }
    //获取学校 模块菜单 结束
    return $school_role;
}

/*
 * 输出失败信息
 * */
function showexit($message)
{
    exit(json_encode(
        array(
            'code'=>0,
            'msg'=>$message
        )
    ));
}

/*
 * 输出成功信息
 * */
function showsuccess($message,$data=array(),$others=array())
{
    exit(json_encode(
        array_merge(array(
            'code'=>1,
            'msg'=>$message,
            'data'=>$data
        ),$others
    )));
}

/*
 * 快速输出信息, 相对于 format_data
 * */
function fast_format_data($return,$api='json')
{
    if(empty($return['msg'])){
        if($return['code']=='1'){
            $return['msg']='操作成功';
        }else{
            $return['msg']='操作失败';
        }
    }
    //去除全局字段
    unset($return['data']['one_info']['create_dateline']);
    unset($return['data']['one_info']['modify_dateline']);
    unset($return['data']['one_info']['isdelete']);
    $return = array(
        'code'=>(string)$return['code'],
        'msg'=>(string)$return['msg'],
        'data'=>(object)$return['data']
    );
    if($api=='return'){
        return $return;
    }else{
        @header("Content-type: application/json; charset=utf-8");
        $return_string = json_encode($return);
        echo $return_string;die();
    }
}
/*
 * 请求接口
 * */
function get_url($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);  //设置访问的url地址
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//不输出内容
    $result =  curl_exec($ch);
    curl_close ($ch);
    return $result;
}

/*
 * 2017/12/28 wangdi 基于url的用户权限判断
 * */
function rbac_by_url(){
    global $_G;
    $cm_mod = $_G['mod'];
    $cm_action = $_G['action'];
    $cm_do = $_G['do'];
    $user_role_id = $_G['user_role_id'];
    $menu_id = DB::result_first("SELECT menu_id FROM ".DB::table('common_menu')." WHERE cm_mod='".$cm_mod."' AND cm_action='".$cm_action."' AND cm_do='index' ORDER BY menu_id ASC LIMIT 1");
    if($menu_id){
        $user_role_menu = DB::fetch_first("SELECT * FROM ".DB::table('user_role_menu')." WHERE menu_id=".$menu_id." AND role_id=".$user_role_id);
        if(!$user_role_menu){
            $result['code'] = 0;
            $result['msg'] = '您没有权限';
            $_G['gp_api'] == 'json' ? exit(format_data($result,'json')) : showmessage('您没有权限','/');
        }
    }
}

function subtext($text, $length) {
    if (mb_strlen($text, 'utf8') > $length) {
        return mb_substr($text, 0, $length, 'utf8') . '...';
    } else {
        return $text;
    }
}

function get_role_where(){
    global $_G;
    $where = '';
    if($_G['user_role_id']==72){
        //学校管理员
        $where = " AND user_dept='".$_G['member']['user_dept']."' ";
    }elseif ($_G['user_role_id']==75) {
        //学校教师
        //$where = " AND user_id='".$_G['user_id']."' ";
        $where = " AND user_dept='".$_G['member']['user_dept']."' ";
    }
    return $where;
}

function get_menu_status($user_role_id,$mod,$action,$do='',$id=''){
    $where = '';
//    //查询当前模块 增改删 菜单编号
    $add_position = $mod.'_'.$action.'_add';
    $edit_position = $mod.'_'.$action.'_edit';
    $del_position = $mod.'_'.$action.'_delete';
//    $sql="SELECT * FROM ".DB::table('common_menu')." WHERE position IN ('".$add_position."','".$edit_position."','".$del_position."')";
//    $result = DB::fetch_all($sql);
//    $menu_id = array();
//    $common_menu = $menu_status = array();
//    foreach($result AS $key => $value){
//        $menu_id[] = $value['menu_id'];
//        if($value['position']){
//            $common_menu[$value['position']][$value['menu_id']]=$value['menu_id'];
//        }
//    }
//    if($menu_id){
//        $sql="SELECT * FROM ".DB::table('user_role_menu')." WHERE menu_id IN (".dimplode($menu_id).") AND role_id='".$_G['user_role_id']."'";
//        $result = DB::fetch_all($sql);
//        foreach($result AS $key => $value){
//            foreach($common_menu AS $k => $v){
//                if($v[$value['menu_id']]){
//                    $menu_status[$k]=1;
//                }
//            }
//        }
//    }
    //先直接固定设置 后续完善到数据库动态设置 开始
//3	超级管理员
//69	区县管理员
//72	学校管理员
//75	学校教师
    //市级培训 只有超级管理员 或 市级管理员 有增删改权限 定义权限数组
    
    //先直接固定设置 后续完善到数据库动态设置 开始
    return $menu_status;
}

//校验用户操作权限
function check_access($user_role_id, $table, $mod, $action, $do, $user_id, $user_dept, $key='', $id='',$return_type='html',$url_forward=''){
    //3	超级管理员
    //69	区县管理员
    //72	学校管理员
    //75	学校教师
    //学校管理员限制操作本校数据
    if($user_role_id=='72'){
        $sql = "SELECT * FROM ".DB::table($table)." WHERE user_dept='".$user_dept."' AND ".$key."=".$id.' LIMIT 1';
    }
    //普通用户限制操作自己创建的数据
    if($user_role_id=='75'){
        $sql = "SELECT * FROM ".DB::table($table)." WHERE user_id='".$user_id."' AND ".$key."=".$id.' LIMIT 1';
    }
    if($sql){
        $result = DB::fetch_first($sql);
        if(empty($result)){
            switch ($return_type) {
                case 'html':
                    if(empty($url_forward)){
                        $url_forward = 'index.php?mod='.$mod.'&action='.$action.'&do=index&api=html';
                    }
                    showmessage('您无权限操作', $url_forward);
                    break;
                case 'json':
                    $return['code'] = '0';
                    $return['msg'] = '您无权限操作';
                    $return['data'] = array();
                    echo format_data($return, $return_type, $mod, $action, $do);
                    die;
                    break;
                default:
                    $url_forward = 'index.php?mod='.$mod.'&action='.$action.'&do=index&api=html';
                    showmessage('您无权限操作', $url_forward);
                    break;
            } 
        }   
    }
    return '';
}

function is_mobile() {
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
        return true;
    }
    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset($_SERVER['HTTP_VIA'])) {
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 判断手机发送的客户端标志,兼容性有待提高
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array('nokia',
            'sony',
            'ericsson',
            'mot',
            'samsung',
            'htc',
            'sgh',
            'lg',
            'sharp',
            'sie-',
            'philips',
            'panasonic',
            'alcatel',
            'lenovo',
            'iphone',
            'ipod',
            'blackberry',
            'meizu',
            'android',
            'netfront',
            'symbian',
            'ucweb',
            'windowsce',
            'palm',
            'operamini',
            'operamobi',
            'openwave',
            'nexusone',
            'cldc',
            'midp',
            'wap',
            'mobile'
        );
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    // 协议法，因为有可能不准确，放到最后判断
    if(isset($_SERVER['HTTP_ACCEPT']))
    { 
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html'))))
        {
            return true;
        } 
    } 
    return false;
}

function curl_post($curl_post, $url, $headers = '') {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); //只需要设置一个秒的数量就可以
    if ($headers) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    } else {
        curl_setopt($curl, CURLOPT_HEADER, false);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_NOBODY, true);
    curl_setopt($curl, CURLOPT_POST, true);
    if (strpos('--' . $url, 'https')) {
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); // https请求 不验证证书和hosts
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); //这个是重点
    }
    if ($raw == 1) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($curl_post));
    } else {
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($curl_post));
    }
    $return_str = curl_exec($curl);
    curl_close($curl);
    return $return_str;
}

/**
 * 获取天喻 access_token
 * @param array $api_host 接口服务器地址:port 必填
 * @param string $api['1']['host'] 接口服务地址
 * @param string $api['1']['APPID'] 接口服务地址
 * @param string $api['1']['APPKEY'] 接口服务地址
 * @param string $api['1']['platformCode'] 接口服务地址
 * @author 
 * @copyright (c) 2017-03-15 13:45:53Z 信息技术有限公司 $
 * @version 1.0
 * @return string access_token
 */
function get_access_token($api) {
    $starttime = microtime(true);
    $APPID = $api['1']['APPID'];
    $APPKEY = $api['1']['APPKEY'];
    $timestamp = TIMESTAMP;
    $data = $APPID . $APPKEY . $timestamp;
    $key = $APPKEY;
    $keyInfo = hash_hmac('sha1', $data, $key);
    $keyInfo = strtoupper($keyInfo);
    $platformCode = $api['1']['platformCode'];
    $url = $api['1']['host'] . '/apigateway/getAccessToken';
    $curl_post = array(
        'appId' => $APPID,
        'timeStamp' => $timestamp,
        'keyInfo' => $keyInfo,
            //'platformCode'=>$platformCode
    );
    $curl_post = json_encode($curl_post);
    $result = curl_post($curl_post, $url);
    $result = json_decode($result, TRUE);
    $endtime = microtime(true);
    if (1) {
        $access_token_log = 'IP API start time :' . date('Y-m-d H:i:s', $starttime) . PHP_EOL . 'IP API end time :' . date('Y-m-d H:i:s', $endtime) . PHP_EOL . 'IP API COST TIME =' . ($endtime - $starttime) . 's' . PHP_EOL . 'API GET AccessToken =' . $result['tokenInfo']['accessToken'] . '' . PHP_EOL;
        @file_put_contents(SITE_ROOT . './data/debug/access_token_' . date('Ymd') . '.txt', $access_token_log . PHP_EOL, FILE_APPEND);
    }
    return $result = $result['tokenInfo']['accessToken'];
}

/*
 * 
 * option 转换方法
 * value|name 多行文本转数字的方法
 *  */
function option2array($optiontext) {
    $optionarray = $tmparray = array();
    $optiontext = nl2br(trim($optiontext));
    $tmparray = explode('<br />', $optiontext);
    foreach($tmparray AS $k => $v){
        list($option_value,$option_name) = explode('|',trim($v));
        $optionarray[$option_value] = $option_name;
    }
    return $optionarray;
}

/*
 * 
 * 可 url 传输的base64 转换方法
*/
function dzf_base64_encode($string){
    $result = $tmp = '';
    $tmp = base64_encode($string);
    $dist = array("/", "+", "=");
    $src = array("_a", "_b", "_c");
    $result = str_replace($dist, $src, $tmp);
    return $result;
}


/*
 * 
 * 可 url 传输的base64 转换方法
*/
function dzf_base64_decode($string){
    $result = $tmp = '';
    $dist = array("/", "+", "=");
    $src = array("_a", "_b", "_c");
    $result = str_replace($src, $dist, $string);
    $result = base64_decode($result);
    return $result;
}

/*
* 微信小程序
* 获取openid
 */
function dzf_wxapp($js_code){
    global $_G;
    $APPID = $_G['setting']['site_wxapp_appid'];
    $SECRET = $_G['setting']['site_wxapp_secret'];
    $auth_url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' . $APPID . '&secret=' . $SECRET . '&js_code=' . $js_code . '&grant_type=authorization_code';
    $return = file_get_contents($auth_url);
    return $return;
}

/*
* 权限控制处理
* 获取openid
 */
function dzf_access_denied($api='json'){
    switch ($api) {
        case 'json':
            header('Location: /404.json');
            break;

        default:
            header('HTTP/1.1 404 Forbidden');die;
            break;
    }
}

/*
* id混淆转换方法
* id
 * key 加密key 保持10位
 * 方法  encode decode
 */
function dzf_idkey($id,$type='encode',$key='5652156525'){
    $id2 = '';
    switch ($type) {
        case 'encode':
            //拆分key平均放在id两侧
            $key_pre = substr($key,0,5);
            $key_end = substr($key,-5);
            $id2 = $key_pre.$id.$key_end;
            break;
        case 'decode':
            $idl = strlen($id)-10;
            $id2 = substr($id,5,$idl);
            break;
    }
    return $id2;
}

//3. 生成原始的二维码(不生成图片文件)
function scerweima2($url=''){
    require_once SITE_ROOT.'./plugins/phpqrcode/phpqrcode.php';
    $value = $url;         //二维码内容
    $errorCorrectionLevel = 'L';  //容错级别
    $matrixPointSize = 5;      //生成图片大小
    //生成二维码图片
    $QR = QRcode::png($value,false,$errorCorrectionLevel, $matrixPointSize, 2);
}

/*
 * DB数据数组树格式化函数
 * @pram 一维 array $array
 * @return 一维 $array tree
 */
function db2tree($tree, $rootId = 0, $idname='id', $pidname='upid', $subname='sub') {
    $return = array();
    foreach ($tree as $leaf) {
        if ($leaf[$pidname] == $rootId) {
            foreach ($tree as $subleaf) {
                if ($subleaf[$pidname] == $leaf[$idname]) {
                    $leaf[$subname] = db2tree($tree, $leaf[$idname],$idname,$pidname,$subname);
                    break;
                }
            }
            $return[] = $leaf;
        }
    }
    return $return;
}

/*
 * 
 * 可 url 传输的base64 转换方法
*/
function dzf_base64($string,$type){
    $result = $tmp = '';
    switch ($type) {
        case 'encode':
            $tmp = base64_encode($string);
            $dist = array("/", "+", "=");
            $src = array("_a", "_b", "_c");
            $result = str_replace($dist, $src, $tmp);
            break;

        case 'decode':
            $dist = array("/", "+", "=");
            $src = array("_a", "_b", "_c");
            $result = str_replace($src, $dist, $string);
            $result = base64_decode($result);
            break;
    }
    return $result;
}

/**
 * @name mymd5 获取目录下文件名列表(包含子目录路径)
 * @abstract 大文件上传 MD5校验 用于 upload_main
 * @param $file string 文件路径
 * @return string md5校验值
 */
function mymd5( $file ) {
    $fragment = 65536;

    $rh = fopen($file, 'rb');
    $size = filesize($file);

    $part1 = fread( $rh, $fragment );
    fseek($rh, $size-$fragment);
    $part2 = fread( $rh, $fragment);
    fclose($rh);

    return md5( $part1.$part2 );
}

//文件全路径格式化方法
/*
 * @param $url string 地址
 * @param $type int 图片类型用来控制默认图片的输出 1 课程默认封面图片  2 课程视频 3 学科资源
 *  */
function file_url($url, $type = '',$file_ext='') {
    global $_G;
    if ($url) {
        switch ($type) {
            //是否预览 只支持 OFFICE 文件
            case "6":
                if(strpos('#'.$_G['setting']['office_preview_ext'], $file_ext)){
                    $url = $_G['setting']['office_preview'] . '/' . $url;
                }else{
                    $url = "";
                }
                break;
            
            default:
                if (!strpos('#' . $url, '//')) {
                    $url = $_G['setting']['site_oss_api'] . '/' . $url;
                }
                break;
        }
    } else {
        switch ($type) {
            case "1":
                $url = $_G['setting']['site_oss_api'] . '/' . $_G['setting']['default_lesson_img_live'];
                break;

            case "2":
                $url = $_G['setting']['site_oss_api'] . '/' . $_G['setting']['default_lesson_img_live'];
                break;

            case "3":
                $url = $_G['setting']['site_oss_api'] . '/' . $_G['setting']['default_lesson_img_live'];
                break;
            //默认头像
            case "5":
                if (!strpos('#' . $_G['setting']['default_user_avatar'], '//')) {
                    $url = $_G['setting']['site_oss_api'] . '/' . $_G['setting']['default_user_avatar'];
                }else{
                    $url = $_G['setting']['default_user_avatar'];
                }
                break;
        }
    }
    return $url;
}

/*
 * @name 移除字段前缀
 * @abstract 移除字段前缀方法
 * @param $data array 数据数组 key 为原始字段名
 * @param $pre string 前缀 
 *
 */
function remove_field_pre($data, $pre = '') {
    $return = array();
    foreach ($data AS $key => $value) {
        $key = str_replace($pre, '', $key);
        $return[$key] = $value;
    }
    return $return;
}

/*
 * @name JSON合法检查
 * @abstract 检查字符串是否JSON
 * @param $string string 校验字符串 
 *
 */
function is_json($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
}

/*
 * @name 访问量统计
 * @abstract 主要统计PV UV 以及 登录
 * @param 无 
 *
 */
function stat_visit() {
    global $_G;
    if($_G['setting']['site_stat_visit']=='2'){
        //初始化访问量统计 如果本月未有统计记录 初始化本月0记录
        $sv_year = date('Y',TIMESTAMP);
        $sv_month = date('m',TIMESTAMP);
        $sql = "SELECT sv_id FROM ".DB::table('stat_visit')." WHERE sv_year='".$sv_year."' AND sv_month='".$sv_month."' LIMIT 1";
        $result = DB::result_first($sql);
        if(empty($result)){
            $insert_data = array(
                'sv_year'=>$sv_year,
                'sv_month'=>$sv_month,
                'sv_pageview_num'=>1,
                'sv_uvview_num'=>1,
                'sv_login_num'=>1
            );
            @DB::insert('stat_visit', $insert_data);
        }else{
            $update_data=array();
            $update_where = " WHERE sv_year ='".date('Y',TIMESTAMP)."' AND sv_month='".date('m',TIMESTAMP)."' ";
            //PV +1
            $update_data[] =  "sv_pageview_num = sv_pageview_num+1";
            //UV +1
            if(empty($_G['cookie'][$_G['cookie']['saltkey']])){
                $update_data[] =  "sv_uvview_num = sv_uvview_num+1";
                dsetcookie($_G['cookie']['saltkey'], 1, 86400 * 1);
            }
            //LOGIN + 1
            if($_G['gp_mod']=='user' && $_G['gp_action']=='main' && $_G['gp_do']=='login'){
                $update_data[] =  "sv_login_num = sv_login_num+1";
            }
            if($update_data && $update_where){
                $sql = "UPDATE ".DB::table('stat_visit')." SET ".implode(',',$update_data)." ".$update_where." LIMIT 1";
                @DB::query($sql);
            }   
        }
    }
}
?>