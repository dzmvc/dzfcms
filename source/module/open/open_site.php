<?php
/**
 * 模块信息事件类
* @filename source/module/open_site.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_open_site extends open_site{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['open_site'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        die;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $open_site = isset($_REQUEST['open_site']) && !empty($_REQUEST['open_site']) ? $_REQUEST['open_site']:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($open_site AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $open_site[$key] = implode(",", $value);
                }
            }
            $open_site['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $open_site['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $open_site['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $open_site['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['sm_id']){
                $open_site['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id']){
                $open_site['create_user_id'] = $_G['member']['user_id'];
            }
            $insert_id = $this->add('open_site',$open_site);
            $info = $this->one_info('open_site', 'os_id', $insert_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=open&action=site&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=open&action=site&do=index');
                }
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('open/site/add');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['open_site'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        die;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $open_site = isset($_REQUEST['open_site']) && !empty($_REQUEST['open_site']) ? $_REQUEST['open_site']:'';
        $os_id = isset($_REQUEST['os_id']) ? $_REQUEST['os_id'] : '';
        if($os_id){
            if('1'==$issubmit){
                $open_site['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($open_site AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $open_site[$key] = implode(",", $value);
                    }
                }
                $where = array('os_id'=>$os_id);
                if($_G['member']['area_id1']){
                    $where['area_id1'] = $_G['member']['area_id1'];
                }
                if($_G['member']['area_id2']){
                    $where['area_id2'] = $_G['member']['area_id2'];
                }
                if($_G['member']['area_id3']){
                    $where['area_id3'] = $_G['member']['area_id3'];
                }
                if($_G['member']['sm_id']){
                    $where['sm_id'] = $_G['member']['sm_id'];
                }
                if($_G['member']['user_id']){
                    $open_site['modify_user_id'] = $_G['member']['user_id'];
                }
                $effect_row = $this->edit('open_site',$open_site,$where);
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('open_site','os_id',$os_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $info;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=open&action=site&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=open&action=site&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('open/site/edit');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['os_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        die;
        $os_id = isset($_REQUEST['os_id']) ? $_REQUEST['os_id'] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if(is_array($os_id)){
            foreach($os_id AS $key => $value){
                $where["os_id"]=$value;
                $effect_row = $this->delete("open_site", $where, $limit=1);
            }
        }else{
            if($os_id){
                $where["os_id"]=$os_id;
                $effect_row = $this->delete("open_site", $where, $limit=1);
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=open&action=site&do=index');
            }else{
                showmessage('操作失败','index.php?mod=open&action=site&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        die;
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND os_title LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        
        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }
        $orderby = " ORDER BY os_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('open_site',$page_condition);
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html' && $api !='xls'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach($format_return['data']['page_data'] AS $key => $value){
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($data['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($data['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $v);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $tmp[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[]=$tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=open&action=site&do=index&keyword=".$keyword);
            //include template('open/site/index');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['company_qygsbxgl'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_import() {
        global $_G;
        die;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($_FILES) {
//            $csv = new csv($_FILES['file']['tmp_name']);
//            $csv_data = $csv->csv_to_array_to_utf8();
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $excel_file_path = $_FILES['file']['tmp_name'];
            $excel = new excel();
            $excel_data = $excel->read_excel($excel_file_path);
            $sheet_data = $excel_data[0];//返回第一页
            //填写校验
//            foreach ($sheet_data as $key => $val) {
//                if (!$flag) {
//                    $msg = "";
//                    if ($nodate) {
//                        $nodatestr = implode($nodate, ',');
//                        $msg .= "第{$nodatestr}行没有填写;";
//                    }
//                    $result['code'] = 0;
//                    $result['msg'] = $msg;
//                    exit(json_encode($result));
//                }
//            }
            $insert_num = $effect_num = 0;
            $import_data = $insert_data = $effect_data = array();
            $table_structure = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($sheet_data as $sk => $sv) {
                if(($sv[0] !='#' && $sv[0] != $table_structure['table_key']) && $sk > 4){
                    $tmp = array();
                    foreach($sv AS $skk => $svv){
                        if(in_array($sheet_data[3][$skk], $trim_field)){
                            $svv = trim($svv);
                        }
                        $tmp[$sheet_data[3][$skk]]=$svv;
                    }
                    
                    $import_data[$sk] = $tmp;
                    //去除主键 开始
                    unset($tmp['#']);
                    unset($tmp[$table_structure['table_key']]);
                    //去除主键 结束
                    $table_name = $_G['gp_mod'].'_'.$_G['gp_action'];
                    if(empty($sv[0])){
                        $tmp['create_dateline'] = TIMESTAMP;
                        $insert_id = DB::insert($table_name, $tmp,true);
                        if($insert_id){
                            $insert_num++;
                            $insert_data[] = $import_data[$sk];
                        }
                    }else{
                        //编辑
                        $where = array(
                            $table_structure['table_key']=>$sv[0],
                        );
                        $effect_id = DB::update($table_name, $tmp, $where);
                        if($effect_id){
                            $tmp['modify_dateline'] = TIMESTAMP;
                            @DB::update($table_name, $tmp, $where);
                            $effect_num++;
                            $effect_data[] = $import_data[$sk];
                        }
                    }
                }
            }
            if ($insert_num || $effect_num) {
                $result['code'] = 1;
                if($insert_num){
                    $result['msg'] = "成功导入{$insert_num}条数据<br />";
                }
                if($effect_num){
                    $result['msg'] .= "成功更新{$effect_num}条数据<br />";
                }
                $result['msg'] .= "请核对导入是否正确<br />";
                //返回HTML
                $colgroup_col = '';
                $thead_tr_td = '';
                foreach ($sheet_data[4] as $hk => $hv) {
                    $colgroup_col .= '<col width="">';
                    $thead_tr_td .= '<th>'.$hv.'</th>';
                }
                $result['data']['dhtml']='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 28px;"><legend>已导入(更新)数据明细</legend></fieldset><table class="layui-table" lay-skin="line">
                    <colgroup>
                    '.$colgroup_col.'
                    <col>
                    </colgroup>
                  <thead>				
                    <tr>'.$thead_tr_td.'</tr> 
                  </thead>
                <tbody>';
                foreach($insert_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                foreach($effect_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                $result['data']['dhtml'] .='</tbody></table>';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            } else {
                $result['code'] = 0;
                $result['msg'] = '导入数据未有新增或修改<br />';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            }
            exit(json_encode($result));
        }else{
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 首页信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_portal() {
        global $_G;
//        ini_set("display_errors","On");
//        error_reporting(E_ALL);
        $portal = array();
        $FONTNUM = 1;
        define('FONTNUM', $FONTNUM);
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //获取首页显示的新闻分类
        $sql = "SELECT nic_id,nic_pid,nic_name,nic_path,nic_sort,nic_position,nic_type,nic_icon,nic_url,nic_description FROM ".DB::table('news_info_cate')." WHERE nic_position='home' ORDER BY nic_sort ASC LIMIT 4";
        $result = DB::fetch_all($sql);
        $data = array();
        $cate_info = $cate_list = array();
        foreach ($result AS $key => $value) {
            if($key==0){
                $cate_info[] = $value;//首页信息列表分类
            }else{
                $cate_list[] = $value;//首页顶部分类列表
            }
        }
        $data['cate_list'] = $cate_list;
        //获取首页显示的新闻信息
        foreach ($cate_info AS $key => $value) {
            $wheresql = "";
            //如果是顶级分类 获取下级分类来获取西西
            if($value['nic_pid']=='0'){
                $sql = "SELECT nic_id FROM ".DB::table('news_info_cate')." WHERE nic_pid='".$value['nic_id']."'";
                $result = DB::fetch_all($sql);
                $nic_ids = array();
                foreach($result AS $k => $v){
                    $nic_ids[] = $v['nic_id'];
                }
                if($nic_ids){
                    $wheresql = " nic_id IN (".dimplode($nic_ids).") ";
                }
            }else{
                $wheresql = " nic_id = '".$value['nic_id']."' ";
            }
            if($wheresql){
                $sql = "SELECT ni_id,ni_title,ni_tag,ni_img,ni_fbr FROM ".DB::table('news_info')." WHERE ".$wheresql;
                $result = DB::fetch_all($sql);
                $news_info = array();
                foreach($result AS $kk => $vv){
                    $vv['ni_tags'] = explode(',', $vv['ni_tag']);
                    $news_info[] = $vv;
                }
                $data['cate_info'][] = array(
                    'nic_id'=>$value['nic_id'],
                    'nic_name'=>$value['nic_name'],
                    'news_info'=>$news_info,
                );
            }
        }
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data'] = $data;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('open/site/portal');
        }
    }
    
    /**
     * 内页列表
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_list() {
        global $_G;
//        ini_set("display_errors","On");
//        error_reporting(E_ALL);
        $nic_id = isset($_REQUEST['nic_id']) ? $_REQUEST['nic_id'] : '';
        //获取分类信息
        if($nic_id){
            $data['news_info_cate'] = $this->one_info('news_info_cate','nic_id',$nic_id);   
        }
        //分类下信息
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '6':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql .= " AND ni_title LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        if($nic_id && $data['news_info_cate']){
            if($data['news_info_cate']['nic_pid']=='0'){
                $sql = "SELECT nic_id FROM ".DB::table('news_info_cate')." WHERE nic_pid='".$nic_id."'";
                $result = DB::fetch_all($sql);
                $nic_ids = array();
                foreach($result AS $k => $v){
                    $nic_ids[] = $v['nic_id'];
                }
                if($nic_ids){
                    $wheresql .= " AND nic_id IN (".dimplode($nic_ids).") ";
                }else{
                    $wheresql .= " AND nic_id = '".$nic_id."' ";
                }
            }else{
                $wheresql .= " AND nic_id = '".$nic_id."' ";
            }
        }
        $orderby = " ORDER BY ni_id DESC ";
        //右侧分类获取

        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        $page_result = $this->index('news_info',$page_condition);
        $page_data = $ni_tags_all =  array();
        foreach($page_result['page_data'] AS $key => $value){
            $value['create_dateline_format'] = date('Y/m/d',$value['create_dateline']);
            $value['ni_tags'] = explode(',', $value['ni_tag']);
            if($value['ni_tags']){
               $ni_tags_all = array_merge($value['ni_tags'],$ni_tags_all);
            }
            $value['ni_brief'] = cutstr(strip_tags($value['ni_content']),128,'...');
            $value['ni_img'] = file_url($value['ni_img']);
            $page_data[] = $value;
        }
        $page_result['page_data'] = $page_data;
        $data['news_info'] = $page_result;
        $data['ni_tags_all'] = $ni_tags_all;
        //获取首页显示的新闻分类
        $sql = "SELECT nic_id,nic_pid,nic_name,nic_path,nic_sort,nic_position,nic_type,nic_icon,nic_url,nic_description FROM ".DB::table('news_info_cate')." WHERE nic_position='home' ORDER BY nic_sort ASC LIMIT 4";
        $data['news_info_cate_nav'] = DB::fetch_all($sql);
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data'] = $data;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $multipage = multi($page_result['total_rows'], $perpage, $page, "/open/site/list?nic_id=".$nic_id."&keyword=".$keyword);
            include template('open/site/list');
        }
    }
    
    
    /**
     * 内页详情页
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        //获取信息详情
        $ni_id = isset($_REQUEST['ni_id']) ? $_REQUEST['ni_id'] : '';
        if($ni_id){
            $data['news_info'] = $this->one_info('news_info','ni_id',$ni_id);
            $data['news_info']['create_dateline_format'] = date('Y/m/d',$data['news_info']['create_dateline']);
            $data['news_info']['ni_tags_all'] = explode(',', $data['news_info']['ni_tag']);
            $data['news_info']['ni_brief'] = cutstr(strip_tags($data['news_info']['ni_content']),128,'...');
            $data['news_info']['ni_img'] = file_url($data['news_info']['ni_img']);
            $data['news_info']['ni_content'] = stripslashes($data['news_info']['ni_content']);
            if($data['news_info']['nic_id']){
                $nic_id = isset($_REQUEST['nic_id']) ? $_REQUEST['nic_id'] : $data['news_info']['nic_id'];
                //获取分类信息
                if($nic_id){
                    $data['news_info_cate'] = $this->one_info('news_info_cate','nic_id',$nic_id);   
                }
            }
        }
        //获取首页显示的新闻分类 右侧分类获取
        $sql = "SELECT nic_id,nic_pid,nic_name,nic_path,nic_sort,nic_position,nic_type,nic_icon,nic_url,nic_description FROM ".DB::table('news_info_cate')." WHERE nic_position='home' ORDER BY nic_sort ASC LIMIT 4";
        $data['news_info_cate_nav'] = DB::fetch_all($sql);
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data'] = $data;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $multipage = multi($page_result['total_rows'], $perpage, $page, "/open/site/list?nic_id=".$nic_id."&keyword=".$keyword);
            include template('open/site/detail');
        }
    }
}
?>