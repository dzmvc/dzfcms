<?php
/**
 * 模块信息事件类
* @filename source/module/user_role.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_user_role extends user_role{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : '';
        if($role_id){
            $detail = $this->one_info('user_role','role_id',$role_id);   
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('user/role/detail');
        }
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['user_role'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $user_role = isset($_REQUEST['user_role']) && !empty($_REQUEST['user_role']) ? $_REQUEST['user_role']:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            $user_role_menu_new = $user_role['authids'];
            unset($user_role['authids']);
            foreach($user_role AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $user_role[$key] = implode(",", $value);
                }
            }
            $user_role['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $user_role['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $user_role['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $user_role['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['sm_id']){
                $user_role['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id']){
                $user_role['create_user_id'] = $_G['member']['user_id'];
            }
            $insert_id = $this->add('user_role',$user_role);
            if($insert_id){
                //authids权限分配修改 开始
                $role_id = $insert_id;
                //删除原有角色菜单 并添加新菜单权限 排除冗余干扰
                $condition = array('role_id'=>$role_id);
                @DB::delete('user_role_menu', $condition);
                //添加设置的新菜单
                $default_menu_pid = array(1,5,50);
                $user_role_menu = array_merge($default_menu_pid,$user_role_menu_new);
                if($user_role_menu){
                    foreach ($user_role_menu as $key => $value) {
                        $insert_data = array('menu_id'=>$value,'role_id'=>$role_id);
                        @$this->add('user_role_menu',$insert_data);
                    }
                }
                //更新系统缓存数据
                $cname = array('setting');
                updatecache($cname);
                //authids权限分配修改 结束
                $info = $this->one_info('user_role', 'role_id', $insert_id);   
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=user&action=role&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=user&action=role&do=index');
                }
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('user/role/add');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['user_role'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $user_role = isset($_REQUEST['user_role']) && !empty($_REQUEST['user_role']) ? $_REQUEST['user_role']:'';
        $role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : '';
        if($role_id){
            if('1'==$issubmit){
                //authids权限分配修改 开始
                $user_role_menu_new = $user_role['authids'];
                unset($user_role['authids']);
                //删除原有角色菜单 并添加新菜单权限
                $condition = array('role_id'=>$role_id);
                $effect_row_del = DB::delete('user_role_menu', $condition);
                if($effect_row_del){
                    //添加设置的新菜单
                    $default_menu_pid = array(1,5);
                    $user_role_menu = array_merge($default_menu_pid,$user_role_menu_new);
                    if(isset($user_role_menu)){
                        foreach ($user_role_menu as $key => $value) {
                            $insert_data = array('menu_id'=>$value,'role_id'=>$role_id);
                            @$this->add('user_role_menu',$insert_data);
                        }
                    }
                    //authids权限分配修改 结束
                    $user_role['modify_dateline'] = TIMESTAMP;
                    //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                    foreach($user_role AS $key => $value){
                        if(is_array($value) && !empty($value)){
                            $user_role[$key] = implode(",", $value);
                        }
                    }
                    $where = array('role_id'=>$role_id);
                    if($_G['member']['area_id1']){
                        $where['area_id1'] = $_G['member']['area_id1'];
                    }
                    if($_G['member']['area_id2']){
                        $where['area_id2'] = $_G['member']['area_id2'];
                    }
                    if($_G['member']['area_id3']){
                        $where['area_id3'] = $_G['member']['area_id3'];
                    }
                    if($_G['member']['sm_id']){
                        $where['sm_id'] = $_G['member']['sm_id'];
                    }
                    if($_G['member']['user_id']){
                        $user_role['modify_user_id'] = $_G['member']['user_id'];
                    }
                    $effect_row = $this->edit('user_role',$user_role,$where);
                    //更新系统缓存数据
                    $cname = array('setting');
                    updatecache($cname);   
                }
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('user_role','role_id',$role_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $info;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row && $effect_row_del){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=user&action=role&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=user&action=role&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('user/role/edit');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['role_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if(is_array($role_id)){
            foreach($role_id AS $key => $value){
                $where["role_id"]=$value;
                if($value && $value != 3){
                   $effect_row = $this->delete("user_role", $where, $limit=1); 
                }elseif($value == 3){
                    $return['msg'] = '系统管理员不能删除';
                }
            }
        }else{
            if($role_id && $role_id != 3){
                $where["role_id"]=$role_id;
                $effect_row = $this->delete("user_role", $where, $limit=1);
            }elseif($role_id == 3){
                $return['msg'] = '系统管理员不能删除';
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=user&action=role&do=index');
            }else{
                showmessage('操作失败','index.php?mod=user&action=role&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $cookie_page = getcookie($_G['gp_mod'].'_'.$_G['gp_action'].'_'.'page');
        $page = empty($_REQUEST['page']) ? intval($cookie_page):intval($_REQUEST['page']);
        if($page){
            dsetcookie($_G['gp_mod'].'_'.$_G['gp_action'].'_'.'page', $page);
        }
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND role_name LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
               $role_desc = isset($_REQUEST["role_desc"]) ? $_REQUEST["role_desc"] :"";
       if($role_desc){
           $wheresql .= " AND role_desc LIKE '%".$role_desc."%' ";
          $wherearray["role_desc"] = "'".$role_desc."' ";
        }

        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }
        $orderby = " ORDER BY role_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('user_role',$page_condition);
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $return['code'] = '1';
            $return['data'] = $page_result;
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=user&action=role&do=index&keyword=".$keyword);
            include template('user/role/index');
        }
    }
    
    /**
     * 获取用户角色树
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_tree() {
        global $_G;
        $user_role_menu = array();
        $role_id = isset($_REQUEST['role_id']) ? $_REQUEST['role_id'] : '';
        $user_role_menu = $this->user_role_menu($role_id);
        if($role_id){
            $tree_selected = $user_role_menu['role_menu'][$role_id]['menu_url_list'];
            $trees = $this->list2tree($user_role_menu['sys_all_menu'],5,$tree_selected);
        }else{
            $tree_selected = array();
            $trees = $this->list2tree($user_role_menu['sys_all_menu'],5,$tree_selected);
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['trees'] = $trees;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('user/role/detail');
        }
    }
    
    /*
     * 后台信息分类数据数组树格式化函数
     * @pram 一维 array $array
     * @return 一维 $array tree
     */
    public function list2tree($tree, $rootId = 0, $tree_selected) {
        $return = array();
        foreach ($tree as $leaf) {
            if ($leaf['menu_pid'] == $rootId) {
                $leaf['name'] = $leaf['name_var'];
                $leaf['value'] = $leaf['menu_id'];
                if($tree_selected[$leaf['menu_id']]){
                    $leaf['checked'] = true;
                }else{
                    $leaf['checked'] = false;
                }
                foreach ($tree as $subleaf) {
                    if ($subleaf['menu_pid'] == $leaf['menu_id']) {
                        $leaf['list'] = $this->list2tree($tree, $leaf['menu_id'],$tree_selected);
                        break;
                    }
                }
                //TODO 暂不对do分配权限 对do进行权限分配
                if($leaf['cm_do']=='' || $leaf['cm_do']=='index' || 1==1){
                    $return[] = $leaf;
                }
            }
        }
        return $return;
    }
    
    /*
     * 获取角色菜单表数据 缓存至 common_syscache
     * @return array user_role_menu
     */

    public function user_role_menu($role_id) {
        //DEBUG 取出所有角色菜单
        $data['user_role_menu'] = array(
            'role_menu' => array(
            //'role_id' => array(	
            //'menu_url_md5'=>array(),
            //'menu_url_tree'=>array()
            //)
            ),
            'user_menu' => array(
            //'user_id' => array(
            //'menu_url_md5'=>array(),
            //'menu_url_tree'=>array()
            //)
            ),
            'sys_all_menu' => array()
        );
        if(empty($role_id)){
            $role_id=3;
        }
        $sys_all_menu_sql_results = array();
        if($role_id==3){
            $sys_all_menu_sql = "SELECT cm.menu_id,cm.menu_pid,cm.position,cm.sub_position,cm.cm_mod,cm.cm_action,cm.cm_do,cm.menu_icon,cm.menu_icon_current,cm.name_var,cm.url,cm.menu_isopen,cm.menu_display,cm.self_style,cm.sort  
            FROM " . DB::table('common_menu') . " AS cm 
            WHERE cm.enable = 1 AND cm.menu_isopen = 0 AND cm.isdelete = 0 
            ORDER BY cm.menu_pid ASC, cm.sort ASC";
            $sys_all_menu_sql_results = DB::fetch_all($sys_all_menu_sql); 
        }else{
            $sys_all_menu_sql = "SELECT cm.menu_id,cm.menu_pid,cm.position,cm.sub_position,cm.cm_mod,cm.cm_action,cm.cm_do,cm.menu_icon,cm.menu_icon_current,cm.name_var,cm.url,cm.menu_isopen,cm.menu_display,cm.self_style,cm.sort  
            FROM " . DB::table('common_menu') . " AS cm 
            WHERE cm.enable = 1 AND cm.menu_isopen = 0 AND cm.isdelete = 0  
            ORDER BY cm.menu_pid ASC, cm.sort ASC";
            $sys_all_menu_sql_results = DB::fetch_all($sys_all_menu_sql);   
        }
        $data['user_role_menu']['sys_all_menu'] = $sys_all_menu_sql_results;
        $user_role_menu_sql_results = array();
        $user_role_menu_sql = "SELECT cm.menu_id,cm.menu_pid,cm.position,cm.sub_position,cm.cm_mod,cm.cm_action,cm.cm_do,cm.menu_icon,cm.menu_icon_current,cm.name_var,cm.url,cm.self_style,cm.sort,urm.role_id,urm.user_id 
        FROM " . DB::table('user_role_menu') . " AS urm 
        LEFT JOIN " . DB::table('common_menu') . " AS cm ON urm.menu_id = cm.menu_id 
        WHERE cm.enable = 1 AND cm.isdelete = 0 AND urm.role_id = '".$role_id."' 
        ORDER BY cm.menu_pid ASC, cm.sort ASC";
        $user_role_menu_sql_results = DB::fetch_all($user_role_menu_sql);
        //按角色 和 用户 格式化菜单数据 KEY 为 $key !empty(sub_position) = md5(url.'&'.sub_position) else md5(url);
        foreach ($user_role_menu_sql_results as $tkey => $tvalue) {
            if ($tvalue['url'] || $tvalue['sub_position']) {
                $tmp_key = empty($tvalue['sub_position']) ? md5($tvalue['url']) : md5($tvalue['url'] . '&' . $tvalue['sub_position']);
            } else {
                $tmp_key = md5($tvalue['menu_id']);
            }
            if ($tvalue['role_id']) {
                $data['user_role_menu']['role_menu'][$tvalue['role_id']]['menu_url_md5'][$tmp_key] = 1;
                $data['user_role_menu']['role_menu'][$tvalue['role_id']]['menu_url_tree'][$tvalue['menu_id']] = $tvalue;
            }
            if ($tvalue['user_id']) {
                $data['user_role_menu']['user_menu'][$tvalue['user_id']]['menu_url_md5'][$tmp_key] = 1;
                $data['user_role_menu']['user_menu'][$tvalue['user_id']]['menu_url_tree'][$tvalue['menu_id']] = $tvalue;
            }
        }

        //DEBUG 格式化权限菜单数
        foreach ($data['user_role_menu']['role_menu'] AS $key => $value) {
            $data['user_role_menu']['role_menu'][$key]['menu_url_tree'] = $this->menuarr2tree($value['menu_url_tree']);
            $data['user_role_menu']['role_menu'][$key]['menu_url_list'] = $value['menu_url_tree'];
        }
        foreach ($data['user_role_menu']['user_menu'] AS $key => $value) {
            $data['user_role_menu']['user_menu'][$key]['menu_url_tree'] = $this->menuarr2tree($value['menu_url_tree']);
            $data['user_role_menu']['user_menu'][$key]['menu_url_list'] = $value['menu_url_tree'];
        }
        return $data['user_role_menu'];
    }
    
    /*
     * 缓存用户权限菜单数据数组树格式化函数
     * @pram 一维 array $array
     * @return 一维 $array tree
     */
    public function menuarr2tree($tree, $rootId = 0) {
        $return = array();
        foreach ($tree as $leaf) {
            if ($leaf['menu_pid'] == $rootId) {
                foreach ($tree as $subleaf) {
                    if ($subleaf['menu_pid'] == $leaf['menu_id']) {
                        $leaf['submenu'] = $this->menuarr2tree($tree, $leaf['menu_id']);
                        break;
                    }
                }
                $return[$leaf['menu_id']] = $leaf;
            }
        }
        return $return;
    }
}
?>