<?php
/**
* 应用接口控制事件模块文件
* 此应用接口控制事件文件或模块事件程序作用描述
* 
* @filename source/module/user/user_org.php 2013-11-11 02:10:56x
* @author DZF <info@dzmvc.com>
* @version 1.0.0
* @since 1.0.0
* @copyright DZF (c) 2013
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
* 应用接口控制事件类
* 应用接口控制事件类名称类的含义说明
* 
* @author 开发者名称 <info@mail.com>
* @version 1.0
* @since 1.0 （可选）
*/
class ctrl_user_org extends user_org{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    public $_table = 'user_org';
    public $_pk    = 'uo_id';
    public $_field_pre    = '';
    public $_keyword    = 'uo_name';
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    
    /**
    * 获取详情
    * 可以 通过数据表主键对应数据的详情信息
    *
    * @api          {get} /user_org/detail/?uo_id=id
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {get} {Number} uo_id 获取数据唯一编号 必传
    * @apiSuccess   {json}
    */
    public function do_detail() {
        global $_G;
        $detail = array();
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_pk){
            if($_G['member']['area_id1']){
                $where['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $where['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $where['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['area_id4']){
                $where['area_id4'] = $_G['member']['area_id4'];
            }
            if($_G['member']['sm_id']){
                $where['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
                $where['create_user_id'] = $_G['member']['user_id'];
            }
            $where[$this->_pk] = $_pk;
            $detail = $this->get_field_value_by_mids($this->_table, $where, '*' ,'', 1);
            $detail = $detail[0];
            $detail = format_info($detail[0], $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $detail['create_dateline'] = date('Y-m-d H:i:s',$detail['create_dateline']);
            //$detail = $this->one_info($this->_table,$_pk,$_pk);   
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            //include template('user/org/detail');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
    * 添加数据
    * 可以添加一条数据信息
    *
    * @api          {post} /user_org/add
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {get} issubmit 表单安全校验字符串 必传
    * @apiParam     {post} {Object} 添加数据内容 参考表结构 必传
    * @apiSuccess   {json} 返回JSON内容
    */   
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $table_data = isset($_REQUEST[$this->_table]) && !empty($_REQUEST[$this->_table]) ? $_REQUEST[$this->_table]:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($table_data AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $table_data[$key] = implode(",", $value);
                }
            }
            $table_data['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $table_data['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $table_data['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $table_data['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['area_id4']){
                $table_data['area_id4'] = $_G['member']['area_id4'];
            }
            if($_G['member']['sm_id']){
                $table_data['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id']){
                $table_data['create_user_id'] = $_G['member']['user_id'];
            }
            //设置uo_path
            if($table_data['uo_pid']){
                $where[$this->_pk] = $table_data['uo_pid'];
                $detail = $this->get_field_value_by_mids($this->_table, $where, '*' ,'', 1);
                $detail = $detail[0];
                if($detail['uo_path']){
                    $table_data['uo_path'] = $detail['uo_path'].','.$table_data['uo_pid'];
                }else{
                    $table_data['uo_path'] = $table_data['uo_pid'];
                }
            }else{
                //查找当前用户的上级单位编号
                $table_data['uo_path'] = '0';
            }
            
            $insert_id = $this->add($this->_table,$table_data);
            $detail = $this->one_info($this->_table, $this->_pk, $insert_id);
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            $node_info = array(
                'uo_id'=>$detail['uo_id'],
                'uo_pid'=>$detail['uo_pid'],
                'uo_name'=>$detail['uo_name'],
                'uo_sort'=>$detail['uo_sort']
            );
            $detail['node_info'][] = $node_info;
            if($detail){
               $return['data']['one_info'] = $detail;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=user&action=org&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=user&action=org&do=index');
                }
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('user/org/add');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
    * 编辑数据
    * 编辑一条数据信息
    *
    * @api          {get} /user_org/edit?uo_id=id
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {get} issubmit 表单安全校验字符串 必传
    * @apiParam     {post} {Object} 添加数据内容 参考表结构 必传
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_edit() {
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $return['code'] = '1';
        $return['msg'] = '';
        $table_data = isset($_REQUEST[$this->_table]) && !empty($_REQUEST[$this->_table]) ? $_REQUEST[$this->_table]:'';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_pk){
            $where = array($this->_pk=>$_pk);
//            if($_G['member']['area_id1']){
//                $where['area_id1'] = $_G['member']['area_id1'];
//            }
//            if($_G['member']['area_id2']){
//                $where['area_id2'] = $_G['member']['area_id2'];
//            }
//            if($_G['member']['area_id3']){
//                $where['area_id3'] = $_G['member']['area_id3'];
//            }
//            if($_G['member']['area_id4']){
//                $where['area_id4'] = $_G['member']['area_id4'];
//            }
//            if($_G['member']['sm_id']){
//                $where['sm_id'] = $_G['member']['sm_id'];
//            }
//            if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
//                $where['create_user_id'] = $_G['member']['user_id'];
//            }
            if('1'==$issubmit){
                $table_data['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($table_data AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $table_data[$key] = implode(",", $value);
                    }
                }
                $table_data['modify_user_id'] = $_G['member']['user_id'];
                $effect_row = $this->edit($this->_table,$table_data,$where);
            }
            //DEBUG 获取操作对象信息
            $detail = $this->get_field_value_by_mids($this->_table, $where, '*' ,'', 1);
            $detail = $detail[0];
            $detail = $this->one_info($this->_table,$this->_pk,$_pk);
            if($detail){
               $return['data']['one_info'] = $detail;
            }else{
               $return['data']['one_info'] = array();  
            }
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $detail;
        if ($api) {
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=user&action=org&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=user&action=org&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('user/org/edit');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
    * 删除数据
    * 删除一条数据信息
    *
    * @api          {post} /user_org/delete
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object} 多个删除 或 {String} 单个删除 删除数据编号 必传
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_delete() {
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['area_id4']){
            $where['area_id4'] = $_G['member']['area_id4'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $where['create_user_id'] = $_G['member']['user_id'];
        }
        if(is_array($_pk)){
            foreach($_pk AS $key => $value){
                $where[$this->_pk]=$value;
                $effect_row = $this->delete($this->_table, $where, $limit=1);
            }
        }else{
            if($_pk){
                $where[$this->_pk]=$_pk;
                $effect_row = $this->delete($this->_table, $where, $limit=1);
            }
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=user&action=org&do=index');
            }else{
                showmessage('操作失败','index.php?mod=user&action=org&do=index');
            }
        }
    }
    
    /**
     * 
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */

    /**
    * 列表数据
    * 获取一页数据列表信息
    *
    * @api          {post} /user_org/index
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object}  必传 user_org
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_index(){
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND ".$this->_keyword." LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        
        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wherearray['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wherearray['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wherearray['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['area_id4']){
            $wheresql .= " AND area_id4 = '".$_G['member']['area_id4']."' ";
            $wherearray['area_id4'] = $_G['member']['area_id4'];
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
            $wherearray['sm_id'] = $_G['member']['sm_id'];
        }
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $wheresql .= " AND create_user_id = '".$_G['member']['user_id']."' ";
            $wherearray['create_user_id'] = $_G['member']['user_id'];
        }
        $orderby = " ORDER BY ".$this->_pk." DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index($this->_table,$page_condition);
        //DEBUG 返回信息
        if($site){
            $page_data = array();
            foreach($page_result['page_data'] AS $key => $value){
                $tmp = array();//格式化选择性输出
                $tmp = remove_field_pre($value, $this->_field_pre);
                $page_data[] = $tmp;
            }
            $page_result['page_data'] = $page_data;   
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html' && $api !='xls'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do'], $_G['gp_client'], 'msg', '', '', array('option2name'=>1));
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($format_return['data']['page_data'] AS $key => $value) {
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($format_return['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($format_return['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $value[$k]);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $value[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[] = $tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=user&action=org&do=index&keyword=".$keyword);
            //include template('user/org/index');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
    * 导入数据
    * 批量导入数据信息
    *
    * @api          {post} /user_org/import
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object}  必传 user_org
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_import() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($_FILES) {
//            $csv = new csv($_FILES['file']['tmp_name']);
//            $csv_data = $csv->csv_to_array_to_utf8();
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $excel_file_path = $_FILES['file']['tmp_name'];
            $excel = new excel();
            $excel_data = $excel->read_excel($excel_file_path);
            $sheet_data = $excel_data[0];//返回第一页
            //填写校验
//            foreach ($sheet_data as $key => $val) {
//                if (!$flag) {
//                    $msg = "";
//                    if ($nodate) {
//                        $nodatestr = implode($nodate, ',');
//                        $msg .= "第{$nodatestr}行没有填写;";
//                    }
//                    $result['code'] = 0;
//                    $result['msg'] = $msg;
//                    exit(json_encode($result));
//                }
//            }
            $insert_num = $effect_num = 0;
            $import_data = $insert_data = $effect_data = array();
            $table_structure = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($sheet_data as $sk => $sv) {
                if(($sv[0] !='#' && $sv[0] != $table_structure['table_key']) && $sk > 4){
                    $tmp = array();
                    foreach($sv AS $skk => $svv){
                        if(in_array($sheet_data[3][$skk], $trim_field)){
                            $svv = trim($svv);
                        }
                        $tmp[$sheet_data[3][$skk]]=$svv;
                    }
                    
                    $import_data[$sk] = $tmp;
                    //去除主键 开始
                    unset($tmp['#']);
                    unset($tmp[$table_structure['table_key']]);
                    //去除主键 结束
                    $table_name = $_G['gp_mod'].'_'.$_G['gp_action'];
                    if(empty($sv[0])){
                        $tmp['create_dateline'] = TIMESTAMP;
                        $insert_id = DB::insert($table_name, $tmp,true);
                        if($insert_id){
                            $insert_num++;
                            $insert_data[] = $import_data[$sk];
                        }
                    }else{
                        //编辑
                        $where = array(
                            $table_structure['table_key']=>$sv[0],
                        );
                        $effect_id = DB::update($table_name, $tmp, $where);
                        if($effect_id){
                            $tmp['modify_dateline'] = TIMESTAMP;
                            @DB::update($table_name, $tmp, $where);
                            $effect_num++;
                            $effect_data[] = $import_data[$sk];
                        }
                    }
                }
            }
            if ($insert_num || $effect_num) {
                $result['code'] = 1;
                if($insert_num){
                    $result['msg'] = "成功导入{$insert_num}条数据<br />";
                }
                if($effect_num){
                    $result['msg'] .= "成功更新{$effect_num}条数据<br />";
                }
                $result['msg'] .= "请核对导入是否正确<br />";
                //返回HTML
                $colgroup_col = '';
                $thead_tr_td = '';
                foreach ($sheet_data[4] as $hk => $hv) {
                    $colgroup_col .= '<col width="">';
                    $thead_tr_td .= '<th>'.$hv.'</th>';
                }
                $result['data']['dhtml']='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 28px;"><legend>已导入(更新)数据明细</legend></fieldset><table class="layui-table" lay-skin="line">
                    <colgroup>
                    '.$colgroup_col.'
                    <col>
                    </colgroup>
                  <thead>				
                    <tr>'.$thead_tr_td.'</tr> 
                  </thead>
                <tbody>';
                foreach($insert_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                foreach($effect_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                $result['data']['dhtml'] .='</tbody></table>';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            } else {
                $result['code'] = 0;
                $result['msg'] = '导入数据未有新增或修改<br />';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            }
            exit(json_encode($result));
        }else{
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
    * 列表数据
    * 获取一页数据列表信息
    *
    * @api          {post} /user_org/index
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object}  必传 user_org
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_index_tree(){
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        //如果当前学校或机构单位有没有根节点 就自动创建一个根节点
        $wheresql = '';
        if($_G['user_role_id'] >= 72 && $_G['member']['sm_id']){
            //校级用户
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
            $uo_name = ext::school_name($_G['member']['sm_id']);
        }elseif($_G['user_role_id'] >= 69 && $_G['user_role_id'] <= 71){
            //县级用户
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wheresql .= " AND sm_id = '0' ";
            $uo_name = ext::d_name($_G['member']['area_id3']);
        }elseif($_G['user_role_id'] >= 66 && $_G['user_role_id'] <= 68){
            //市级用户
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wheresql .= " AND sm_id = '0' ";
            $uo_name = ext::d_name($_G['member']['area_id2']);
        }elseif($_G['user_role_id'] == 63 && $_G['member']['area_id1']){
            //省级用户
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wheresql .= " AND sm_id = '0' ";
            $uo_name = ext::d_name($_G['member']['area_id1']);
        }elseif($_G['user_role_id'] <= 10){
            //系统管理员
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wheresql .= " AND sm_id = '0' ";
            $uo_name = $_G['setting']['sitename'];
        }else{
            //异常用户
            $wheresql .= " AND 1=2 ";
            $uo_name = '';
        }
        $sql = "SELECT uo_id FROM ".DB::table('user_org')." WHERE 1=1 ".$wheresql;
        $result = DB::result_first($sql);
        if(empty($result) && !empty($uo_name)){
            $data = array(
                'uo_pid'=>'0',
                'uo_name'=>$uo_name,
                'uo_sort'=>'0',
                'uo_path'=>'0',
                'area_id1'=>$_G['member']['area_id1'],
                'area_id2'=>$_G['member']['area_id2'],
                'area_id3'=>$_G['member']['area_id3'],
                'sm_id'=> empty($_G['member']['sm_id'])?0:$_G['member']['sm_id'],
                'create_user_id'=>$_G['user_id'],
                'create_dateline'=>TIMESTAMP
            );
            @DB::insert('user_org', $data);
        }
        include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/'.$_G['gp_do'].'');
    }
    
    /**
    * 列表数据
    * 获取一页数据列表信息
    *
    * @api          {post} /user_org/index
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object}  必传 user_org
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_api_tree(){
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $type = empty($_REQUEST['type']) ? 'table_tree':$_REQUEST['type'];//根据参数控制返回不同前端插件需要的格式 table_tree xm_select
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND ".$this->_keyword." LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        $uo_pid = isset($_REQUEST['uo_pid']) ? $_REQUEST['uo_pid'] : '0';
        if($uo_pid !=''){
            $wheresql = " AND uo_pid = '".$uo_pid."' ";
            $wherearray['uo_pid'] = $uo_pid;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        //根据用户角色取出组织机构，暂设置县级取出下级单位，其他级别仅取出自身单位同级机构
        /*
         * user_role_id 大于等于 72 查看所属学校 组织结构
         * user_role_id 介于 69 到 71 查看 area_id3 下所有学校 以及学校部门组织结构
         * user_role_id 小于 68 查看本机单位的 组织结构
         */
        $wheresql = ' AND uo_pid=0 ';
        if($_G['user_role_id'] >= 72 && $_G['member']['sm_id'] && $_G['member']['sm_id']){
            //校级用户
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }elseif($_G['user_role_id'] >= 69 && $_G['user_role_id'] <= 71 && $_G['member']['area_id3']){
            //县级用户
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }elseif($_G['user_role_id'] >= 66 && $_G['user_role_id'] <= 68 && $_G['member']['area_id2']){
            //市级用户
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }elseif($_G['user_role_id'] == 63 && $_G['member']['area_id1'] && $_G['member']['area_id1']){
            //省级用户
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }elseif($_G['user_role_id'] <= 10){
            //系统管理员
        }else{
            //异常用户
            $wheresql .= " AND 1=2 ";
        }
        $orderby = " ORDER BY uo_sort ASC,".$this->_pk." ASC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index($this->_table,$page_condition);
        //DEBUG 返回信息
        $page_data = array();
        foreach($page_result['page_data'] AS $key => $value){
            switch ($type) {
                case 'table_tree':
                    //获取所有子集数数据
                    $sql = "SELECT * FROM ".DB::table($this->_table)." WHERE FIND_IN_SET(".$value['uo_id'].",uo_path) ORDER BY uo_sort desc,uo_id desc";
                    $subtree = DB::fetch_all($sql);
                    $treeList = $this->dborg2tree($subtree, $value['uo_id']);
                    $value['treeList'] = $treeList; 
                    break;

                case 'xm_select':
                    //获取所有子集数数据
                    $tmp = array(
                        'name'=>$value['uo_name'],
                        'value'=>$value['uo_id'],
                    );
                    $sql = "SELECT uo_id AS value,uo_name AS name,uo_pid AS pid FROM ".DB::table($this->_table)." WHERE FIND_IN_SET(".$value['uo_id'].",uo_path) ORDER BY uo_sort desc,uo_id desc";
                    $subtree = DB::fetch_all($sql);
                    $treeList = $this->dborg2tree($subtree, $tmp['value'],'value', 'pid', 'children');
                    if(!empty($treeList)){
                        $tmp['children'] = $treeList; 
                    }
                    $value = $tmp;
                    break;
            }
            $page_data[] = $value;
        }
        $page_result['page_data'] = $page_data;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if($api && $api !='html' && $api !='xls'){
            $return['code'] = 0;
            $return['msg'] = '操作成功';
            $return['count'] = (int)$page_result['total_rows'];
            $return['data'] = $page_result['page_data'];
            @header("Content-type: application/json; charset=utf-8");
            $return_string = json_encode($return);
            echo $return_string;die();
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($format_return['data']['page_data'] AS $key => $value) {
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($format_return['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($format_return['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $value[$k]);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $value[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[] = $tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }
    }
    
    public function dborg2tree($tree, $rootId = 0, $idname = 'uo_id', $pidname = 'uo_pid', $subname = 'treeList') {
        $return = array();
        foreach ($tree as $leaf) {
            if ($leaf[$pidname] == $rootId) {
                foreach ($tree as $subleaf) {
                    if ($subleaf[$pidname] == $leaf[$idname]) {
                        $leaf[$subname] = $this->dborg2tree($tree, $leaf[$idname],$idname,$pidname,$subname);
                        break;
                    }
                }
                $return[] = $leaf;
            }
        }
        return $return;
    }
}
?>