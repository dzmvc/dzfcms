<?php
/**
 * 模块信息事件类
* @filename source/module/user_main.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_user_main extends user_main{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        //$user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $user_id = $_G['user_id'];
        if($user_id){
            $detail = $this->one_info('user','user_id',$user_id); 
            foreach($detail AS $key => $value){
                if($value==NULL){
                    $detail[$key]='';
                }
            }
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['hidden_structure'] = 1;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('user/main/detail');
        }
    }

    public function do_info() {
        global $_G;
        //ini_set("display_errors","On");
        //error_reporting(E_ALL);
        $return['code'] = '2';
        $return['data']['one_info'] = array();
        $user_main = isset($_REQUEST['user_main']) ? $_REQUEST['user_main'] : '';
        if (!empty($_G['user_id']) && !empty($user_main)) {
            $update_data = array();
            if ($user_main['user_realname']) {
                $update_data['user_realname'] = $user_main['user_realname'];
            }
            if ($user_main['user_avatar']) {
                $update_data['user_avatar'] = $user_main['user_avatar'];
            }
            if ($user_main['user_cardno']) {
                $update_data['user_cardno'] = $user_main['user_cardno'];
            }
            if ($user_main['gender']) {
                $update_data['gender'] = $user_main['gender'];
            }
            if ($user_main['user_phone']) {
                $update_data['user_phone'] = $user_main['user_phone'];
            }
            if ($user_main['user_email']) {
                $update_data['user_email'] = $user_main['user_email'];
            }
            if ($user_main['about']) {
                $update_data['about'] = $user_main['about'];
            }
//            $user_main['user_password_old'] = encode_password($user_main['user_password_old']);
//            if($user_main['user_password'] && $user_main['user_password_old']==$_G['member']['user_password']){
//                $user_main['user_password'] = encode_password($user_main['user_password']);
//            }else{
//                unset($user_main['user_password']);
//            }
//            unset($user_main['user_password_old']);
            $where = array(
                'user_id' => $_G['user_id']
            );
            $effect_row = $this->edit('user', $update_data, $where);
            if ($_G['gp_api'] && $effect_row) {
                $return['code'] = '1';
                $return['data'] = $update_data;
            }
        }
        if($_G['user_id']){
            $info = $this->one_info('user', 'user_id', $_G['user_id']);
            $info['user_realname']=ext::user_realname($_G['user_id']);
        }
        $return['data']['one_info'] = $info;
        //DEBUG 调试输出
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        //DEBUG 调试输出
        if($api=='json'){
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('user/main/info');
        }
    }
    
    /*
     *  校验用户名是否已存在
     * $out_put 是否输出 1 总是输出并结束 2 仅失败输出
     */

    public function do_check_user_name($out_put = 1) {
        global $_G;
        $return = array('code' => '1');
        $user_main = isset($_REQUEST['user_main']) && !empty($_REQUEST['user_main']) ? $_REQUEST['user_main'] : '';
        if (!empty($user_main['user_name'])) {
            $check_user = $this->check_user_exist($user_main['user_name']);
            if (empty($check_user)) {
                $return['code'] = '0';
            } else {
                $return['code'] = '1';
            }
        } else {
            $return['code'] = '2';
        }
        if ($out_put == 1 || ($out_put == 2 && $return['code'] != 0)) {
            if ($_G['gp_api']) {
                echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            } else {
                echo format_data($return, 'json', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
                if ($out_put == 2 && $return['code'] != 0) {
                    die();
                }
            }
        }
    }

    public static function check_user_password($user_id, $user_password = 0) {
        global $_G;
        $check_user_password = $wheresql = '';
        if ($user_password && $user_id) {
            $wheresql = " AND user_password = '" . encode_password($user_password) . "'";
        }
        if (!empty($wheresql)) {
            $check_user_password = DB::result_first('SELECT user_id FROM ' . DB::table('user') . " WHERE user_id='" . $user_id . "' " . $wheresql . " LIMIT 1");
        }
        return $check_user_password;
    }
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['user_main'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $user_main = isset($_REQUEST['user_main']) && !empty($_REQUEST['user_main']) ? $_REQUEST['user_main']:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //DEBUG 校验短信是否通过
            if($user_main['user_phone']){
//                $sms_main_check = auto_mad('sms', 'main', 'check');
//                $sms_main_check_array = json_decode($sms_main_check, true);
//                if ($sms_main_check_array['code'] != '0') {
//                    echo $sms_main_check;
//                    die;
//                }
            }
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($user_main AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $user_main[$key] = implode(",", $value);
                }
            }
            $user_main['create_dateline'] = TIMESTAMP;
//            if($_G['member']['area_id1']){
//                $user_main['area_id1'] = $_G['member']['area_id1'];
//            }
//            if($_G['member']['area_id2']){
//                $user_main['area_id2'] = $_G['member']['area_id2'];
//            }
//            if($_G['member']['area_id3']){
//                $user_main['area_id3'] = $_G['member']['area_id3'];
//            }
//            if($_G['member']['sm_id']){
//                $user_main['sm_id'] = $_G['member']['sm_id'];
//            }
//            if($_G['member']['user_id']){
//                $user_main['create_user_id'] = $_G['member']['user_id'];
//            }
            if (!empty($user_main['user_name']) && !empty($user_main['user_password'])) {
                //DEBUG 校验用户是否已存在
                $check_user = $this->check_user_exist($user_main['user_name']);
                //DEBUG 校验手机号码是否已存在
                //$check_user_id = $this->check_user_exist($sms_main_check_array['data']['sms_main']['sms_phone']);
                if (empty($check_user)) {
                    $user_password_encode = encode_password($user_main['user_password']);
                    //获取接收短信的手机号码
                    $user_main_data = array(
                        "user_name" => $user_main['user_name'],
                        "user_realname" => $user_main['user_realname'],
                        "user_password" => $user_password_encode,
                        "user_cardno" => $user_main['user_cardno'],
                        "user_avatar" => $user_main['user_avatar'],
                        "user_phone" => $user_main['user_phone'],
                        "user_email" => $user_main['user_email'],
                        "gender" => $user_main['gender'],
                        "qq" => $user_main['qq'],
                        "ischeck" => 1,
                        "create_dateline" => TIMESTAMP
                    );
    //                $user_detail_data = $user_detail;
    //                $user_detail_data['user_phone'] = $sms_main_check_array['data']['sms_main']['sms_phone'];
    //                $user_detail_data['create_dateline'] = TIMESTAMP;
    //                $user_detail_data['modify_dateline'] = TIMESTAMP;
                    $insert_id = $this->add('user',$user_main_data);
                    if ($insert_id) {
    //                    $user_detail_data['user_id'] = $user_main_insert_id;
    //                    $user_detail_insert_id = DB::insert('user_detail', $user_detail_data, true);
                        $return['code'] = '1';
                    } else {
                        $return['code'] = '3';
                    }
                } else {
                    $return['code'] = '2';
                    $return['msg'] = '同名用户已存在';
                }
                $info = $this->one_info('user', 'user_id', $insert_id);   
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api && $api != 'html') {
            $return['code'] = '1';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=user&action=main&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=user&action=main&do=index');
                }
            }
            $condition = ' role_id > 1 ';
            $user_role = self::get_field_value_by_mids('user_role', $condition, '*', '20');
            //初始化关联下拉选项 开始
            $user_role_option = array();
            foreach($user_role AS $key => $value){
                $user_role_option[]=array(
                    'v'=>$value['role_id'],
                    'n'=>$value['role_name']
                );
            }
            //初始化关联下拉选项 结束
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $format_return['data']['table_structure']["field"]["user_role_id"]["option"] = $user_role_option;
            include template('user/main/add');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['user_main'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $user_main = isset($_REQUEST['user_main']) && !empty($_REQUEST['user_main']) ? $_REQUEST['user_main']:'';
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : $_G['user_id'];
        if(!in_array($_G['user_role_id'],array(3,5))){
            if($_G['user_id']!=$user_id || empty($_G['user_id'])){
                header('HTTP/1.1 404 Forbidden');die;
            }
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if($user_id){
            //DEBUG 编辑数据
            if('1'==$issubmit || $user_main){
                //DEBUG 校验短信是否通过
                if($user_main['user_phone']){
    //                $sms_main_check = auto_mad('sms', 'main', 'check');
    //                $sms_main_check_array = json_decode($sms_main_check, true);
    //                if ($sms_main_check_array['code'] != '0') {
    //                    echo $sms_main_check;
    //                    die;
    //                }
                }
                //TODO 后端字段校验 根据具体业务逻辑添加
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($user_main AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $user_main[$key] = implode(",", $value);
                    }
                }
                $where = array('user_id'=>$user_id);
                if($_G['member']['area_id1']){
                    $where['area_id1'] = $_G['member']['area_id1'];
                }
                if($_G['member']['area_id2']){
                    $where['area_id2'] = $_G['member']['area_id2'];
                }
                if($_G['member']['area_id3']){
                    $where['area_id3'] = $_G['member']['area_id3'];
                }
                if($_G['member']['sm_id']){
                    $where['sm_id'] = $_G['member']['sm_id'];
                }
                if($_G['member']['user_id']){
                    $user_main['modify_user_id'] = $_G['member']['user_id'];
                }
                //DEBUG 校验用户是否已存在
                $check_user = $this->check_user_exist($user_main['user_name'],$user_id);
                //DEBUG 校验手机号码是否已存在 //获取接收短信的手机号码
                //$check_user_id = $this->check_user_exist($sms_main_check_array['data']['sms_main']['sms_phone']);
                if (empty($check_user)) {
                    $user_main_data = array(
                        "modify_dateline" => TIMESTAMP
                    );
                    if($user_main['user_avatar']){
                         $user_main_data['user_avatar']=$user_main['user_avatar'];
                        if($user_main['user_realname']){
                            $user_main_data['user_realname']=$user_main['user_realname'];
                        }
                        if($user_main['gender']){
                            $user_main_data['gender']=$user_main['gender'];
                        }
                    }elseif($user_main['user_password']){
                         $user_password_encode = encode_password($user_main['user_password']);
                         $user_main_data['user_password']=$user_password_encode;
                    }else{
                        $user_main_data = array(
                            "user_realname" => empty($user_main['user_realname'])? $_G['member']['user_realname']:$user_main['user_realname'],
                            "user_phone" => empty($user_main['user_phone'])? $_G['member']['user_phone']:$user_main['user_phone'],
                            "user_cardno" => empty($user_main['user_cardno'])? $_G['member']['user_cardno']:$user_main['user_cardno'],
                            "user_email" => empty($user_main['user_email'])? $_G['member']['user_email']:$user_main['user_email'],
                            "gender" => empty($user_main['gender'])? $_G['member']['gender']:$user_main['gender'],
                            "user_alipay_account" => empty($user_main['user_alipay_account'])? $_G['member']['user_alipay_account']:$user_main['user_alipay_account'],
                            "qq" => empty($user_main['qq'])? $_G['member']['qq']:$user_main['qq'],
                            "address" => empty($user_main['address'])? $_G['member']['address']:$user_main['address'],
                            "ischeck" => 1,
                            "modify_dateline" => TIMESTAMP
                        );
                        if($user_main['user_avatar']){
                            $user_main_data['user_avatar']=$user_main['user_avatar'];
                        }
                        if($user_main['user_password']){
                            $user_password_encode = encode_password($user_main['user_password']);
                            $user_main_data['user_password']=$user_password_encode;
                        }
                    }
                    if(in_array($_G['user_role_id'],array(3,5))){
                         $user_main_data['user_level']=$user_main['user_level'];
                         $user_main_data['user_abalance']=$user_main['user_abalance'];
                         $user_main_data['user_dcash']=$user_main['user_dcash'];
                         $user_main_data['user_role_id']=$user_main['user_role_id'];
                    }
    //                $user_detail_data = $user_detail;
    //                $user_detail_data['user_phone'] = $sms_main_check_array['data']['sms_main']['sms_phone'];
    //                $user_detail_data['create_dateline'] = TIMESTAMP;
    //                $user_detail_data['modify_dateline'] = TIMESTAMP;
                    $where = array(
                        'user_id'=>$user_id
                    );
                    $effect_row = $this->edit('user',$user_main_data,$where);
                    if ($user_main_insert_id) {
    //                    $user_detail_data['user_id'] = $user_main_insert_id;
    //                    $user_detail_insert_id = DB::insert('user_detail', $user_detail_data, true);
                        $return['code'] = '5';
                    } else {
                        $return['code'] = '3';
                    }
                } else {
                    $return['code'] = '2';
                    $return['msg'] = '用户不存在';
                }
            }
            $info = $this->one_info('user', 'user_id', $user_id);   
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $info;
        if ($api && $api != 'html') {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=user&action=main&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=user&action=main&do=index');
                } 
            }
            $condition = ' role_id > 1 ';
            $user_role = self::get_field_value_by_mids('user_role', $condition, '*', '20');
            //初始化关联下拉选项 开始
            $user_role_option = array();
            foreach($user_role AS $key => $value){
                $user_role_option[]=array(
                    'v'=>$value['role_id'],
                    'n'=>$value['role_name']
                );
            }
            //初始化关联下拉选项 结束
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $format_return['data']['table_structure']["field"]["user_role_id"]["option"] = $user_role_option;
            include template('user/main/edit');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['user_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if(is_array($user_id)){
            foreach($user_id AS $key => $value){
                $where["user_id"]=$value;
                $effect_row = $this->delete("user", $where, $limit=1);
            }
        }else{
            if($user_id){
                $where["user_id"]=$user_id;
                $effect_row = $this->delete("user", $where, $limit=1);
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=user&action=main&do=index');
            }else{
                showmessage('操作失败','index.php?mod=user&action=main&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $cookie_page = getcookie($_G['gp_mod'].'_'.$_G['gp_action'].'_'.'page');
        $page = empty($_REQUEST['page']) ? intval($cookie_page):intval($_REQUEST['page']);
        if($page){
            dsetcookie($_G['gp_mod'].'_'.$_G['gp_action'].'_'.'page', $page);
        }
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND user_realname LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
               $user_name = isset($_REQUEST["user_name"]) ? $_REQUEST["user_name"] :"";
       if($user_name){
           $wheresql .= " AND user_name = '".$user_name."' ";
           $wherearray["user_name"] = "'".$user_name."' ";
        }

        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }
        $orderby = " ORDER BY user_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('user',$page_condition);
        foreach($page_result['page_data'] AS $key => $value){
            $page_result['page_data'][$key]['user_role_id'] = ext::role_name($value['user_role_id']);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $return['code'] = '1';
            $return['data'] = $page_result;
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=user&action=main&do=index&keyword=".$keyword);
            include template('user/main/index');
        }
    }
    
    /*
     *  用户密码重置
     */

    public function do_reset_password() {
        global $_G;
        /*
          $user_name = isset($_REQUEST['user_name']) && !empty($_REQUEST['user_name']) ? $_REQUEST['user_name']:'';
          $user_password = isset($_REQUEST['user_password']) && !empty($_REQUEST['user_password']) ? $_REQUEST['user_password']:'';
          $user_phone = isset($_REQUEST['user_phone']) && !empty($_REQUEST['user_phone']) ? $_REQUEST['user_phone']:'';
          $user_nickname = isset($_REQUEST['user_nickname']) && !empty($_REQUEST['user_nickname']) ? $_REQUEST['user_nickname']:'';
          $in_school = isset($_REQUEST['in_school']) && !empty($_REQUEST['in_school']) ? $_REQUEST['in_school']:'';//1 表示不在校 2表示在校
          $am_id_1 = isset($_REQUEST['am_id_1']) && !empty($_REQUEST['am_id_1']) ? $_REQUEST['am_id_1']:'';
          $am_id_2 = isset($_REQUEST['am_id_2']) && !empty($_REQUEST['am_id_2']) ? $_REQUEST['am_id_2']:'';
          $am_id_3 = isset($_REQUEST['am_id_3']) && !empty($_REQUEST['am_id_3']) ? $_REQUEST['am_id_3']:'';
          $sm_id = isset($_REQUEST['sm_id']) && !empty($_REQUEST['sm_id']) ? $_REQUEST['sm_id']:'';
          $grade_num = isset($_REQUEST['grade_num']) && !empty($_REQUEST['grade_num']) ? $_REQUEST['grade_num']:'';
          $class_num = isset($_REQUEST['class_num']) && !empty($_REQUEST['class_num']) ? $_REQUEST['class_num']:'';
         */
        //DEBUG 校验短信是否通过
        $sms_main_check = auto_mad('sms', 'main', 'check');
        $sms_main_check_array = json_decode($sms_main_check, true);
        if ($sms_main_check_array['code'] != '0') {
            echo $sms_main_check;
            die;
        }
        //DEBUG 
        $user_main = isset($_REQUEST['user_main']) && !empty($_REQUEST['user_main']) ? $_REQUEST['user_main'] : '';
        if (!empty($user_main['user_phone']) && !empty($user_main['user_password'])) {
            //DEBUG 校验用户是否已存在
            $check_user_id = $this->check_user_exist($sms_main_check_array['data']['sms_main']['sms_phone']);
            if (!empty($check_user_id)) {
                $user_password_encode = encode_password($user_main['user_password']);
                $user_main_data = array(
                    "user_password" => $user_password_encode
                );
                $reset_user_password_result = DB::update('user', $user_main_data, array('user_id' => $check_user_id));
                $return['code'] = '1';
            } else {
                $return['code'] = '2'; //用户名不存在
            }
        } else {
            //DEBUG TODO 跳转登出
            $return['code'] = '2';
        }
        if ($_G['gp_api']) {
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }
    }

    /*
     *  修改我的帐号密码
     */

    public function do_reset_my_password() {
        global $_G;
        //DEBUG 
        $user_id = isset($_REQUEST['user_id']) && !empty($_REQUEST['user_id']) ? $_REQUEST['user_id'] : $_G['user_id'];
        $user_main = isset($_REQUEST['user_main']) && !empty($_REQUEST['user_main']) ? $_REQUEST['user_main'] : '';
        if (!empty($user_main['user_password_old']) && !empty($user_main['user_password_new'])) {
            //DEBUG 校验用户旧密码是否正确
            $check_user_id = $this->check_user_password($user_id, $user_main['user_password_old']);
            if (!empty($check_user_id)) {
                $data = array('user_password' => encode_password($user_main['user_password_new']));
                $reset_user_password_result = DB::update('user', $data, array('user_id' => $check_user_id));
                $return['msg'] = '操作成功';
                $return['code'] = '1';
                ext::synlogout();
            } else {
                $return['msg'] = '操作失败 原密码错误';
                $return['code'] = '0'; //旧密码错误
            }
        } else {
            //DEBUG TODO 跳转登出
            $return['code'] = '1';
        }
        if ($_G['gp_api']) {
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('user/main/password');
        }
    }

    /*
     *  登录信息
     */

    public function do_login() {
        global $_G;
        /*
         * TODO 登录队列处理 ZeroMQ Redis ActiveMQ 处理 获取部署第三方软件处理
         */
        $check_user_name = $check_user_password = 0;
        $return = array('code' => '5', 'msg' => '用户名或密码错误', 'data' => array());
        /* RECEIVE VALUE */
        $user_name = isset($_REQUEST['user_name']) ? $_REQUEST['user_name'] : '';
        $user_password = isset($_REQUEST['user_password']) ? $_REQUEST['user_password'] : '';
        $login_type = isset($_REQUEST['login_type']) ? $_REQUEST['login_type'] : '';
        if ($user_name && $user_password) {
            //校验用户或者编号是否存在
            $user_id = DB::result_first('SELECT user_id FROM ' . DB::table('user') . " WHERE user_name='" . $user_name . "' LIMIT 1");
            if ($user_id) {
                $check_user = DB::fetch_first("SELECT * FROM " . DB::table('user') . " WHERE user_id ='" . $user_id . "' LIMIT 1");
            }
            //校验密码
            if ($check_user['user_id']) {
                $check_login = auto_mad('user', 'failedlogin', 'check', $check_user);
                if ($check_login['code'] != '1') {
                    echo fast_format_data($check_login);
                    die;
                }
                $user_encode_password = encode_password($user_password);
                if ($user_encode_password == $check_user['user_password']) {
                    ext::synlogin($check_user, $check_user, $check_user);
                    $return['code'] = '1';
                    $return['msg'] = '登录成功';
                    $return['data'] = array();
                    $return['data']['user'] = $check_user;
                    if(empty($return['data']['user']['user_avatar']) || !file_exists(SITE_ROOT.$return['data']['user']['user_avatar'])){
                        $return['data']['user']['user_avatar'] = TPLDIR.'/static/img/user_avatar.png';
                    }
                    $auth = ext::authcode("$check_user[user_password]\t$check_user[user_id]", 'ENCODE'); 
                    $dist = array("/", "+", "=");
                    $src = array("_a", "_b", "_c");
                    $auth = str_replace($dist, $src, $auth);
                    $return['data']['user']['token'] = $auth;
                }else{
                    //更新密码错误记录表 可转换 redis 存储
                    if($check_login['data']['ufl_count']){
                        $sql = "UPDATE ".DB::table('user_failedlogin')." SET ufl_count=ufl_count+1,ufl_lastupdate='".TIMESTAMP."',ufl_ip='".$_G['clientip']."' WHERE ufl_user_name = '".$check_user['user_name']."' LIMIT 1";
                        @DB::query($sql);
                    }else{
                        $insert_data = array(
                            'ufl_ip'=>$_G['clientip'],
                            'ufl_user_name'=>$check_user['user_name'],
                            'ufl_count'=>'1',
                            'ufl_lastupdate'=>TIMESTAMP,
                            'user_id'=>$check_user['user_id'],
                            'area_id1'=>$check_user['area_id1'],
                            'area_id2'=>$check_user['area_id2'],
                            'area_id3'=>$check_user['area_id3'],
                            'sm_id'=>$check_user['sm_id'],
                            'create_dateline'=>TIMESTAMP
                        );
                        @DB::insert('user_failedlogin',$insert_data);
                    }
                }
            }
            if ($_G['gp_api']) {
                $return['data'] = $return['data']['user'];
                echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            } else {
                if ($return['code'] == 1) {
                    showmessage('登录成功', 'index.php?mod=index&action=index&do=index');
                } else {
                    showmessage('用户名密码错误', 'index.php?mod=user&action=main&do=login');
                }
            }
        }else{
            if ($_G['user_id']) {
                header("location:index.php?mod=index&action=index&do=index");
            }
            include template('user/index/login');
        }
    }

    /*
     * 	用户注销退出
    */
    public function do_logout() {
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $sitedomain_web = empty($_G['setting']['sitedomain_web'])?'/':$_G['setting']['sitedomain_web'];
        $life = TIMESTAMP - 3600;
        foreach($_COOKIE AS $key => $value){
            setcookie($key, '', $life, '/', $_G['config']['cookie']['cookiedomain'],true);
        }
        ext::synlogout();
        if($api=='json'){
            $return['code']="0";
            $return['msg']="退出成功";
            $return['data'] = array('sitedomain_web'=>$sitedomain_web);
            echo fast_format_data($return);
        }else{
            echo '<script type="text/javascript">localStorage.clear();setTimeout(function(){top.window.location=\''.$sitedomain_web.'\'}, 1);</script>';
            //@header('Location: /');
        }
    }
    
    /*
     * 	取出用户头像 
     * TODO 优化直接从物理路径获取各种尺寸的头像
     */

    public function do_user_avatar() {
        global $_G;
        $detail = array();
        $user_id = isset($_REQUEST['user_id']) ? $_REQUEST['user_id'] : '';
        $size = isset($_GET['size']) ? $_GET['size'] : '';
        $random = isset($_GET['random']) ? $_GET['random'] : '';
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $check = isset($_GET['check_file_exists']) ? $_GET['check_file_exists'] : '';
        if($user_id){
            $user_avatar = ext::user_avatar($user_id);
        }
        if(empty($user_avatar) || !file_exists(SITE_ROOT.'/'.$user_avatar)){
            $user_avatar = 'template/default/static/img/user_avatar.png';
        }
        if(empty($random)) {
            header("HTTP/1.1 301 Moved Permanently");
            header("Last-Modified:".date('r'));
            header("Expires: ".date('r', time() + 86400));
        }
        header('Location: '.$_G['config']['static']['1']['host'].'/'.$user_avatar);
        exit;
    }
    /*
     * 通信安全KEY
    */
    public function do_safekey() {
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : 'json';
        $appid = isset($_REQUEST['appid']) ? $_REQUEST['appid'] : '';
        $appkey = isset($_REQUEST['appkey']) ? $_REQUEST['appkey'] : '';
        $return = array('code' => '101', 'msg' => '参数异常', 'data' => array());
        if($appid && $appkey){
            $return['code'] = '1';
            $return['msg'] = '操作成功';
            $string = date('Ymd').'-'.$appid.'-'.$appkey;
            $return['data']['safekey'] = dzf_base64($string, 'encode'); 
        }
        if($api=='json'){
            echo fast_format_data($return);
            die;
        }
    }
}
?>