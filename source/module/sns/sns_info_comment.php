<?php

/**
 * 模块信息事件类
 * @filename source/module/sns_info_comment.php 2013-11-11 02:10:56x
 * @author Huming Xu <info@dzmvc.com>
 * @version 1.0.0
 * @copyright DZF (c) 2013, Huming Xu
 */
if (!defined('IN_SITE')) {
    exit('Access Denied');
}

/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_sns_info_comment extends sns_info_comment {

    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();

    static function &instance() {
        static $object;
        if (empty($object)) {
            $object = new self();
        }
        return $object;
    }

    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $sic_id = isset($_REQUEST['sic_id']) ? $_REQUEST['sic_id'] : '';
        if ($sic_id) {
            $detail = $this->one_info('sns_info_comment', 'sic_id', $sic_id);
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        //DEBUG 调试输出
        if ($api) {
            $return['code'] = '1'; //1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        } else {
            include template('sns/info_comment/detail');
        }
    }

    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['sns_info_comment'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit'] : '';
        $sns_info_comment = isset($_REQUEST['sns_info_comment']) && !empty($_REQUEST['sns_info_comment']) ? $_REQUEST['sns_info_comment'] : '';
        //DEBUG 插入数据
        if ('1' == $issubmit) {
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach ($sns_info_comment AS $key => $value) {
                if (is_array($value) && !empty($value)) {
                    $sns_info_comment[$key] = implode(",", $value);
                }
            }
            $sns_info_comment['user_id'] = $_G['user_id'];
            $sns_info_comment['create_dateline'] = TIMESTAMP;
//            if ($_G['member']['area_id1']) {
//                $sns_info_comment['area_id1'] = $_G['member']['area_id1'];
//            }
//            if ($_G['member']['area_id2']) {
//                $sns_info_comment['area_id2'] = $_G['member']['area_id2'];
//            }
//            if ($_G['member']['area_id3']) {
//                $sns_info_comment['area_id3'] = $_G['member']['area_id3'];
//            }
//            if ($_G['member']['sm_id']) {
//                $sns_info_comment['sm_id'] = $_G['member']['sm_id'];
//            }
            $insert_id = $this->add('sns_info_comment', $sns_info_comment);
            $info = $this->one_info('sns_info_comment', 'sic_id', $insert_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if ($insert_id) {
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id;
                } else {
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";
                }
            }
            $return['data']['one_info'] = $info;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        } else {
            if ($issubmit == 1) {
                if ($insert_id) {
                    showmessage('操作成功', 'index.php?mod=sns&action=info_comment&do=index');
                } else {
                    showmessage('操作失败', 'index.php?mod=sns&action=info_comment&do=index');
                }
            }
            include template('sns/info_comment/add');
        }
    }

    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['sns_info_comment'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit'] : '';
        $sns_info_comment = isset($_REQUEST['sns_info_comment']) && !empty($_REQUEST['sns_info_comment']) ? $_REQUEST['sns_info_comment'] : '';
        $sic_id = isset($_REQUEST['sic_id']) ? $_REQUEST['sic_id'] : '';
        if ($sic_id) {
            if ('1' == $issubmit) {
                $sns_info_comment['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach ($sns_info_comment AS $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        $sns_info_comment[$key] = implode(",", $value);
                    }
                }
                $where = array('sic_id' => $sic_id);
                if ($_G['member']['area_id1']) {
                    $where['area_id1'] = $_G['member']['area_id1'];
                }
                if ($_G['member']['area_id2']) {
                    $where['area_id2'] = $_G['member']['area_id2'];
                }
                if ($_G['member']['area_id3']) {
                    $where['area_id3'] = $_G['member']['area_id3'];
                }
                if ($_G['member']['sm_id']) {
                    $where['sm_id'] = $_G['member']['sm_id'];
                }
                $effect_row = $this->edit('sns_info_comment', $sns_info_comment, $where);
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('sns_info_comment', 'sic_id', $sic_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if ($effect_row) {
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                } else {
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            $return['data']['one_info'] = $info;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        } else {
            if ($issubmit == 1) {
                if ($effect_row) {
                    showmessage('操作成功', 'index.php?mod=sns&action=info_comment&do=index');
                } else {
                    showmessage('操作失败', 'index.php?mod=sns&action=info_comment&do=index');
                }
            }
            include template('sns/info_comment/edit');
        }
    }

    /**
     * 删除一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['sic_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $sic_id = isset($_REQUEST['sic_id']) ? $_REQUEST['sic_id'] : '';
        if ($_G['member']['area_id1']) {
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if ($_G['member']['area_id2']) {
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if ($_G['member']['area_id3']) {
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if ($_G['member']['sm_id']) {
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if (is_array($sic_id)) {
            foreach ($sic_id AS $key => $value) {
                $where["sic_id"] = $value;
                $effect_row = $this->delete("sns_info_comment", $where, $limit = 1);
            }
        } else {
            if ($sic_id) {
                $where["sic_id"] = $sic_id;
                $effect_row = $this->delete("sns_info_comment", $where, $limit = 1);
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if ($api) {
            if ($effect_row) {
                $return['code'] = '1';
                $return['data'] = array('delete_rows' => $effect_row);
            } else {
                $return['code'] = '0';
                $return['data'] = array();
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        } else {
            if ($effect_row) {
                showmessage('操作成功', 'index.php?mod=sns&action=info_comment&do=index');
            } else {
                showmessage('操作失败', 'index.php?mod=sns&action=info_comment&do=index');
            }
        }
    }

    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index() {
        global $_G;
        $page = empty($_REQUEST['page']) ? '1' : intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10' : intval($_REQUEST['limit']);
        $start = (($page - 1) * $perpage);
        $wheresql = "";
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if ($keyword) {
            $wheresql = " AND  LIKE '%" . $keyword . "%' ";
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        $sic_accept = isset($_REQUEST["sic_accept"]) ? $_REQUEST["sic_accept"] : "";
        if ($sic_accept) {
            $wheresql .= " AND sic_accept = '" . $sic_accept . "' ";
        }

        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        //循环筛选项目 扩展搜索字段筛选条件 结束
        if ($_G['member']['area_id1']) {
            $wheresql .= " AND area_id1 = '" . $_G['member']['area_id1'] . "' ";
        }
        if ($_G['member']['area_id2']) {
            $wheresql .= " AND area_id2 = '" . $_G['member']['area_id2'] . "' ";
        }
        if ($_G['member']['area_id3']) {
            $wheresql .= " AND area_id3 = '" . $_G['member']['area_id3'] . "' ";
        }
        if ($_G['member']['sm_id']) {
            $wheresql .= " AND sm_id = '" . $_G['member']['sm_id'] . "' ";
        }
        $orderby = " ORDER BY sic_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition = array(
            'page' => $page, //int 请求页面 页码
            'limit' => $limit, //int 每页请求个数
            'perpage' => $perpage, //int 每页显示个数
            'wheresql' => $wheresql, //string //条件SQL语句 
            'orderby' => $orderby, //string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
          $page_result = array(
          //int 返回结果总数
          'total_rows' => $total_rows,
          //array 一页数据数组
          'page_data' => $page_data
          );
         */
        $page_result = $this->index('sns_info_comment', $page_condition);
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if ($api) {
            $module_table_path = './source/module/' . $_G['gp_mod'] . '/dbtable/' . $_G['gp_mod'] . '_' . $_G['gp_action'] . '.json';
            $module_table = array();
            if (file_exists($module_table_path)) {
                $module_table = json_decode(file_get_contents($module_table_path), true);
            }
            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        } else {
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=sns&action=info_comment&do=index&keyword=" . $keyword);
            include template('sns/info_comment/index');
        }
    }

}

?>