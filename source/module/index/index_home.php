<?php

if (!defined('IN_SITE')) {
    exit('Access Denied');
}

class ctrl_index_home extends index_home {

    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    
    /*
    *  系统信息列表
    */
    public function do_console(){
        global $_G;
        $user_zt_task_count_array['wait'] = $user_zt_task_count_array['doing'] = $user_zt_task_count_array['done'] = $user_zt_task_count_array['closed'] = $user_zt_task_count_array['cancel'] = 0;
        $zt_task_count_array['wait'] = $zt_task_count_array['pause'] = $zt_task_count_array['doing'] = $zt_task_count_array['done'] = $zt_task_count_array['closed'] = 0;    
        //DEBUG 初始化访问量统计 如果本月未有统计记录 初始化本月0记录
        //TODO 入口累计方法中如果需要去除查询是否存在本月记录并初始化本月数据的话，此处后台可以一次初始化本年度的统计数据
        $sv_year = date('Y',TIMESTAMP);
        $sv_month = date('m',TIMESTAMP);
        $sql = "SELECT sv_id FROM ".DB::table('stat_visit')." WHERE sv_year='".$sv_year."' AND sv_month='".$sv_month."' LIMIT 1";
        $result = DB::result_first($sql);
        if(empty($result)){
            $data = array(
                'sv_year'=>$sv_year,
                'sv_month'=>$sv_month
            );
            @DB::insert('stat_visit', $data);
        }
        //工单系统表结构信息
        $component_ticket_structure = get_table_structure('component', 'ticket');
        $component_ticket_num_my = $component_ticket_num_all = array();
        foreach($component_ticket_structure["field"]['ct_status']["option"] AS $key => $value){
            $value['num'] = "0";
            $component_ticket_num_my[$value['v']] = $component_ticket_num_all[$value['v']] = $value;
        }
        //DEBUG 我的工单 统计数据
        $sql = "SELECT count(*) AS num,ct_status FROM ".DB::table('component_ticket')." WHERE ct_master='".$_G['user_name']."' GROUP BY ct_status";
        $result = DB::fetch_all($sql);
        foreach($result AS $key=>$value){
            if($value['num'] > 0){
                $component_ticket_num_my[$value['ct_status']]['num'] = (string)$value['num'];   
            }
        }
        //DEBUG 工单总量 统计数据
        $sql = "SELECT count(*) AS num,ct_status FROM ".DB::table('component_ticket')." GROUP BY ct_status";
        $result = DB::fetch_all($sql);
        foreach($result AS $key=>$value){
            if($value['num'] > 0){
                $component_ticket_num_all[$value['ct_status']]['num'] = (string)$value['num'];   
            }
        }
        include template('index/home/console');	
    }
}

?>