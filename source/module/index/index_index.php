<?php
class ctrl_index_index extends index_index {

    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();

    static function &instance() {
        static $object;
        if (empty($object)) {
            $object = new self();
        }
        return $object;
    }

    /*
     *  
     */
    public function do_index() {
        global $_G;
        if (empty($_G['user_id'])) {
            header("location:index.php?mod=user&action=main&do=login");
        }
        //获取iframe_src
        $return['data']['iframe_src'] = isset($_REQUEST['iframe_src']) ? $_REQUEST['iframe_src'] : '';
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';

        //DEBUG 调试输出
        if($api=='json'){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['school_role'] = $school_role;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('index/index');
        }
    }

}

?>