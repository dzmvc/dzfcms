<?php
/**
 * 模块信息事件类
* @filename source/module/ad_main.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_ad_main extends ad_main{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $am_id = isset($_REQUEST['am_id']) ? $_REQUEST['am_id'] : '';
        if($am_id){
            $detail = $this->one_info('ad_main','am_id',$am_id);   
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            include template('ad/main/detail');
        }
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['ad_main'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $ad_main = isset($_REQUEST['ad_main']) && !empty($_REQUEST['ad_main']) ? $_REQUEST['ad_main']:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($ad_main AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $ad_main[$key] = implode(",", $value);
                }
            }
            $ad_main['create_dateline'] = TIMESTAMP;
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach ($ad_main AS $key => $value) {
                if (is_array($value) && !empty($value)) {
                    $txwx_msgzs_hd[$key] = implode(",", $value);
                }
                if(strpos($key, '_img[') && !empty($value)){
                    $tmh_files[] = $value;
                    unset($ad_main[$key]);
                    $ad_main['am_img'] = implode(',', $tmh_files);
                }
            }
            if($_G['member']['area_id1']){
                $ad_main['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $ad_main['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $ad_main['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['sm_id']){
                $ad_main['sm_id'] = $_G['member']['sm_id'];
            }
            $insert_id = $this->add('ad_main',$ad_main);
            $info = $this->one_info('ad_main', 'am_id', $insert_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=ad&action=main&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=ad&action=main&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('ad/main/add');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['ad_main'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $ad_main = isset($_REQUEST['ad_main']) && !empty($_REQUEST['ad_main']) ? $_REQUEST['ad_main']:'';
        $am_id = isset($_REQUEST['am_id']) ? $_REQUEST['am_id'] : '';
        if($am_id){
            if('1'==$issubmit){
                $ad_main['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach ($ad_main AS $key => $value) {
                    if (is_array($value) && !empty($value)) {
                        $txwx_msgzs_hd[$key] = implode(",", $value);
                    }
                    if(strpos($key, '_img[') && !empty($value)){
                        $tmh_files[] = $value;
                        unset($ad_main[$key]);
                        $ad_main['am_img'] = implode(',', $tmh_files);
                    }
                }
                $where = array('am_id'=>$am_id);
                if($_G['member']['area_id1']){
                    $where['area_id1'] = $_G['member']['area_id1'];
                }
                if($_G['member']['area_id2']){
                    $where['area_id2'] = $_G['member']['area_id2'];
                }
                if($_G['member']['area_id3']){
                    $where['area_id3'] = $_G['member']['area_id3'];
                }
                if($_G['member']['sm_id']){
                    $where['sm_id'] = $_G['member']['sm_id'];
                }
                $effect_row = $this->edit('ad_main',$ad_main,$where);
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('ad_main','am_id',$am_id);
            //转换多文件字段为数组
            if($info['am_img']){
                $info['am_img'] = explode(',', $info['am_img']);
                foreach ($info['am_img'] as $key => $value) {
                    $info['am_img'][$key] = explode(';', $value);
                }   
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $info;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=ad&action=main&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=ad&action=main&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('ad/main/edit');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['am_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $am_id = isset($_REQUEST['am_id']) ? $_REQUEST['am_id'] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
		if(is_array($am_id)){
            foreach($am_id AS $key => $value){
                $where["am_id"]=$value;
                $effect_row = $this->delete("ad_main", $where, $limit=1);
            }
		}else{
			if($am_id){
                $where["am_id"]=$am_id;
                $effect_row = $this->delete("ad_main", $where, $limit=1);
			}
		}
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=ad&action=main&do=index');
            }else{
                showmessage('操作失败','index.php?mod=ad&action=main&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND am_name LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
               $am_status = isset($_REQUEST["am_status"]) ? $_REQUEST["am_status"] :"";
       if($am_status){
           $wheresql .= " AND am_status = '".$am_status."' ";
           $wherearray["am_status"] = "'".$am_status."' ";
        }
       $am_position = isset($_REQUEST["am_position"]) ? $_REQUEST["am_position"] :"";
       if($am_position){
           $wheresql .= " AND am_position = '".$am_position."' ";
           $wherearray["am_position"] = "'".$am_position."' ";
        }

        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
               $am_status = isset($_REQUEST["am_status"]) ? $_REQUEST["am_status"] :"";
       if($am_status){
           $wheresql .= " AND am_status = '".$am_status."' ";
           $wherearray["am_status"] = "'".$am_status."' ";
        }
       $am_position = isset($_REQUEST["am_position"]) ? $_REQUEST["am_position"] :"";
       if($am_position){
           $wheresql .= " AND am_position = '".$am_position."' ";
           $wherearray["am_position"] = "'".$am_position."' ";
        }

        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }
        $orderby = " ORDER BY am_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('ad_main',$page_condition);
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=ad&action=main&do=index&keyword=".$keyword);
            include template('ad/main/index');
        }
    }
}
?>