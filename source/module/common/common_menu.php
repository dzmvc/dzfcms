<?php
/**
 * 模块信息事件类
* @filename source/module/common_menu.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_common_menu extends common_menu{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $menu_id = isset($_REQUEST['menu_id']) ? $_REQUEST['menu_id'] : '';
        if($menu_id){
            $detail = $this->one_info('common_menu','menu_id',$menu_id);   
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            //include template('common/menu/detail');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['common_menu'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $common_menu = isset($_REQUEST['common_menu']) && !empty($_REQUEST['common_menu']) ? $_REQUEST['common_menu']:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($common_menu AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $common_menu[$key] = implode(",", $value);
                }
            }
            $common_menu['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $common_menu['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $common_menu['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $common_menu['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['sm_id']){
                $common_menu['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id']){
                $common_menu['create_user_id'] = $_G['member']['user_id'];
            }
            $insert_id = $this->add('common_menu',$common_menu);
            $info = $this->one_info('common_menu', 'menu_id', $insert_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=common&action=menu&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=common&action=menu&do=index');
                }
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('common/menu/add');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['common_menu'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $common_menu = isset($_REQUEST['common_menu']) && !empty($_REQUEST['common_menu']) ? $_REQUEST['common_menu']:'';
        $menu_id = isset($_REQUEST['menu_id']) ? $_REQUEST['menu_id'] : '';
        if($menu_id){
            if('1'==$issubmit){
                $common_menu['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($common_menu AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $common_menu[$key] = implode(",", $value);
                    }
                }
                $where = array('menu_id'=>$menu_id);
                if($_G['member']['area_id1']){
                    $where['area_id1'] = $_G['member']['area_id1'];
                }
                if($_G['member']['area_id2']){
                    $where['area_id2'] = $_G['member']['area_id2'];
                }
                if($_G['member']['area_id3']){
                    $where['area_id3'] = $_G['member']['area_id3'];
                }
                if($_G['member']['sm_id']){
                    $where['sm_id'] = $_G['member']['sm_id'];
                }
                if($_G['member']['user_id']){
                    $common_menu['modify_user_id'] = $_G['member']['user_id'];
                }
                $effect_row = $this->edit('common_menu',$common_menu,$where);
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('common_menu','menu_id',$menu_id);
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $info;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=common&action=menu&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=common&action=menu&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('common/menu/edit');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['menu_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $menu_id = isset($_REQUEST['menu_id']) ? $_REQUEST['menu_id'] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if(is_array($menu_id)){
            foreach($menu_id AS $key => $value){
                $where["menu_id"]=$value;
                $effect_row = $this->delete("common_menu", $where, $limit=1);
            }
        }else{
            if($menu_id){
                $where["menu_id"]=$menu_id;
                $effect_row = $this->delete("common_menu", $where, $limit=1);
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=common&action=menu&do=index');
            }else{
                showmessage('操作失败','index.php?mod=common&action=menu&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND name_var LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
               $menu_isopen = isset($_REQUEST["menu_isopen"]) ? $_REQUEST["menu_isopen"] :"";
       if($menu_isopen){
           $wheresql .= " AND menu_isopen = '".$menu_isopen."' ";
           $wherearray["menu_isopen"] = "'".$menu_isopen."' ";
        }

        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
               $menu_pid = isset($_REQUEST["menu_pid"]) ? $_REQUEST["menu_pid"] :"";
       if($menu_pid){
           $wheresql .= " AND menu_pid = '".$menu_pid."' ";
           $wherearray["menu_pid"] = "'".$menu_pid."' ";
        }

        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        }
        $orderby = " ORDER BY menu_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('common_menu',$page_condition);
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html' && $api !='xls'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach($format_return['data']['page_data'] AS $key => $value){
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($data['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($data['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $v);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $tmp[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[]=$tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=common&action=menu&do=index&keyword=".$keyword);
            //include template('common/menu/index');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['company_qygsbxgl'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_import() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($_FILES) {
//            $csv = new csv($_FILES['file']['tmp_name']);
//            $csv_data = $csv->csv_to_array_to_utf8();
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $excel_file_path = $_FILES['file']['tmp_name'];
            $excel = new excel();
            $excel_data = $excel->read_excel($excel_file_path);
            $sheet_data = $excel_data[0];//返回第一页
            //填写校验
//            foreach ($sheet_data as $key => $val) {
//                if (!$flag) {
//                    $msg = "";
//                    if ($nodate) {
//                        $nodatestr = implode($nodate, ',');
//                        $msg .= "第{$nodatestr}行没有填写;";
//                    }
//                    $result['code'] = 0;
//                    $result['msg'] = $msg;
//                    exit(json_encode($result));
//                }
//            }
            $insert_num = $effect_num = 0;
            $import_data = $insert_data = $effect_data = array();
            $table_structure = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($sheet_data as $sk => $sv) {
                if(($sv[0] !='#' && $sv[0] != $table_structure['table_key']) && $sk > 4){
                    $tmp = array();
                    foreach($sv AS $skk => $svv){
                        if(in_array($sheet_data[3][$skk], $trim_field)){
                            $svv = trim($svv);
                        }
                        $tmp[$sheet_data[3][$skk]]=$svv;
                    }
                    
                    $import_data[$sk] = $tmp;
                    //去除主键 开始
                    unset($tmp['#']);
                    unset($tmp[$table_structure['table_key']]);
                    //去除主键 结束
                    $table_name = $_G['gp_mod'].'_'.$_G['gp_action'];
                    if(empty($sv[0])){
                        $tmp['create_dateline'] = TIMESTAMP;
                        $insert_id = DB::insert($table_name, $tmp,true);
                        if($insert_id){
                            $insert_num++;
                            $insert_data[] = $import_data[$sk];
                        }
                    }else{
                        //编辑
                        $where = array(
                            $table_structure['table_key']=>$sv[0],
                        );
                        $effect_id = DB::update($table_name, $tmp, $where);
                        if($effect_id){
                            $tmp['modify_dateline'] = TIMESTAMP;
                            @DB::update($table_name, $tmp, $where);
                            $effect_num++;
                            $effect_data[] = $import_data[$sk];
                        }
                    }
                }
            }
            if ($insert_num || $effect_num) {
                $result['code'] = 1;
                if($insert_num){
                    $result['msg'] = "成功导入{$insert_num}条数据<br />";
                }
                if($effect_num){
                    $result['msg'] .= "成功更新{$effect_num}条数据<br />";
                }
                $result['msg'] .= "请核对导入是否正确<br />";
                //返回HTML
                $colgroup_col = '';
                $thead_tr_td = '';
                foreach ($sheet_data[4] as $hk => $hv) {
                    $colgroup_col .= '<col width="">';
                    $thead_tr_td .= '<th>'.$hv.'</th>';
                }
                $result['data']['dhtml']='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 28px;"><legend>已导入(更新)数据明细</legend></fieldset><table class="layui-table" lay-skin="line">
                    <colgroup>
                    '.$colgroup_col.'
                    <col>
                    </colgroup>
                  <thead>				
                    <tr>'.$thead_tr_td.'</tr> 
                  </thead>
                <tbody>';
                foreach($insert_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                foreach($effect_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                $result['data']['dhtml'] .='</tbody></table>';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            } else {
                $result['code'] = 0;
                $result['msg'] = '导入数据未有新增或修改<br />';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            }
            exit(json_encode($result));
        }else{
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
}
?>