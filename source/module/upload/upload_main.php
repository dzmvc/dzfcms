<?php

/**
 * @filename source_class.php 2013-11-11 02:10:56x
 * @author Huming Xu <huming17@126.com>
 * @version 1.0.0
 * @copyright DZF (c) 2013, Huming Xu
 */
if (!defined('IN_SITE')) {
    exit('Access Denied');
}

class ctrl_upload_main extends upload_main {

    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();

    static function &instance() {
        static $object;
        if (empty($object)) {
            $object = new self();
        }
        return $object;
    }

    /*
     * 	xheditor编辑器获取远程图片
     */

    public function do_get_remote_img() {
        global $_G;
        //DEBUG 调试输出
        if (!empty($_G['user_id'])) {
            //DEBUG xheditor编辑器获取远程图片
            $attachDir = 'data/upload'; //上传文件保存路径，结尾不要带/
            $upExt = "jpg,jpeg,gif,png";
            $arrUrls = explode('|', $_POST['urls']);
            $urlCount = count($arrUrls);
            for ($i = 0; $i < $urlCount; $i++) {
                $localUrl = $this->saveRemoteImg($arrUrls[$i]);
                if ($localUrl)
                    $arrUrls[$i] = $localUrl;
            }
            echo implode('|', $arrUrls);
        }
    }

    /*
     * 文件上传
     * 注1：本程序特别针对HTML5上传,加入了特殊处理
     * @author xuhm
     * @todo 安全校验
     */

    public function do_file() {
        global $_G;
        //ini_set("display_errors","On");
        //error_reporting(E_ALL);
        //@file_put_contents(SITE_ROOT.'./data/debug/file_request_log_'.date('Ymd').'.txt','RETURN : '.date('Y-m-d H:i:s').' '.  json_encode($_REQUEST).PHP_EOL,FILE_APPEND);
        //@file_put_contents(SITE_ROOT.'./data/debug/file_input_log_'.date('Ymd').'.txt','RETURN : '.date('Y-m-d H:i:s').' '.json_encode($_FILES).PHP_EOL,FILE_APPEND);
        //header('Content-Type: text/html; charset=UTF-8');
        @header("Content-type: application/json; charset=utf-8");
        $file_input_name = isset($_REQUEST['file_input_name']) && !empty($_REQUEST['file_input_name']) ? $_REQUEST['file_input_name'] : 'filedata';
        $return_path_key = isset($_REQUEST['return_path_key']) && !empty($_REQUEST['return_path_key']) ? $_REQUEST['return_path_key'] : 'url';
        $return_name_key = isset($_REQUEST['return_name_key']) && !empty($_REQUEST['return_name_key']) ? $_REQUEST['return_name_key'] : 'file_name';
        $inputName = $file_input_name; //表单文件域name
        $attachDir = !empty($_G['setting']['server_upload'][1]['dirpath']) ? SITE_ROOT . 'data/' . $_G['setting']['server_upload'][1]['dirpath'] : SITE_ROOT . 'data/upload'; //上传文件保存路径，结尾不要带/
        $dirType = !empty($_G['setting']['server_upload'][1]['dirtype']) ? $_G['setting']['server_upload'][1]['dirtype'] : 2; //1:按天存入目录 2:按月存入目录 3:按扩展名存目录  建议使用按天存
        $maxAttachSize = !empty($_G['setting']['server_upload'][1]['maxsize']) ? $_G['setting']['server_upload'][1]['maxsize'] : 20000000; //最大上传大小，默认是20M
        $upExt = !empty($_G['setting']['server_upload'][1]['upext']) ? $_G['setting']['server_upload'][1]['upext'] : 'txt,rar,zip,doc,docx,ppt,pptx,xls,xlsx,pdf,jpg,jpeg,png,jpg,mp3,mp4,mov,amr,svg,7z'; //上传扩展名
        $msgType = 2; //返回上传参数的格式：1，只返回url，2，返回参数数组
        $urltype = isset($_REQUEST['urltype']) && !empty($_REQUEST['urltype']) ? $_REQUEST['urltype'] : '2';//1返回全路径 2返回相对路径
        $chunked = isset($_REQUEST['chunked']) && !empty($_REQUEST['chunked']) ? $_REQUEST['chunked'] : '';//1 分片上传 其他不开启分片上传
        //ini_set('date.timezone','Asia/Shanghai');//时区
        /*
         * thumb 1 表示需要 缩略图; 2 表示不需要缩略图
         */
        $thumb = isset($_REQUEST['thumb']) && !empty($_REQUEST['thumb']) ? $_REQUEST['thumb'] : '';
        $thumb_width = isset($_REQUEST['thumb_width']) && !empty($_REQUEST['thumb_width']) ? $_REQUEST['thumb_width'] : '200';
        $thumb_height = isset($_REQUEST['thumb_height']) && !empty($_REQUEST['thumb_height']) ? $_REQUEST['thumb_height'] : '200';
        $files_store = isset($_REQUEST['files_store']) && !empty($_REQUEST['files_store']) ? $_REQUEST['files_store'] : ''; //1表示 需要数据表pre_file_source存储文件名 返回表fs_id 空或其他返回存储路径
        $err = "";
        $msg = array();
        $tempPath = $attachDir . '/' . date("YmdHis") . mt_rand(10000, 99999) . '_' . random(4) . '.tmp';
        $file_name = '';
        $convert = isset($_REQUEST['convert']) && !empty($_REQUEST['convert']) ? $_REQUEST['convert'] : '';
        $return_array = array("code" => 'e_0001', "msg" => lang('error', 'e_0001'), "data" => $msg);
        if (!empty($_G['user_id'])) {
            if (!is_dir($attachDir)) {
                @mkdir($attachDir, 0777);
                @fclose(fopen($attachDir . '/index.htm', 'w'));
            }
            //大文件上传独立处理逻辑 开始
            if($chunked==1){
                header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
                header("Cache-Control: no-store, no-cache, must-revalidate");
                header("Cache-Control: post-check=0, pre-check=0", false);
                header("Pragma: no-cache");
                // header("HTTP/1.0 500 Internal Server Error");
                // echo mymd5('upload/filename.pdf'); die;
                // Support CORS
                // header("Access-Control-Allow-Origin: *");
                // other CORS headers if any...
                if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
                    exit; // finish preflight CORS requests here
                }
                @set_time_limit(5 * 3600);//大文件上传最长5小时
                // Settings
                // $targetDir = ini_get("upload_tmp_dir") . '/' . "plupload";
                switch ($dirType) {
                    case 1: $attachSubDir = '/big_file/' . 'day_' . date('Ymd');
                        break;
                    case 2: $attachSubDir = '/big_file/' . 'month_' . date('Ym');
                        break;
                    case 3: $attachSubDir = '/big_file/' . 'ext_' . $extension;
                        break;
                }
                $attachDir = $attachDir . $attachSubDir;
                if (!is_dir($attachDir)) {
                    @dmkdir($attachDir, 0777);
                    @fclose(fopen($attachDir . '/index.htm', 'w'));
                }
                PHP_VERSION < '4.2.0' && mt_srand((double) microtime() * 1000000);
                // 获取文件
                if (isset($_REQUEST["name"])) {
                    $fileName = $_REQUEST["name"];
                } elseif (!empty($_FILES)) {
                    $fileName = $_FILES["file"]["name"];
                } else {
                    $fileName = uniqid("file_");
                }
                //获取文件属性
                $fileInfo = pathinfo($fileName);
                $extension = $fileInfo['extension'];
                //定义临时文件名和新文件名
                $newFilename = date("YmdHis") . mt_rand(1000, 9999) . '_' . random(4) . '.' . $extension;
                $newFilenameTmp = md5(date("YmdH") . $fileName . $_G['user_id']) . '.' . $extension;
                //定时临时存储路径和存储路径
                $targetPath = $attachDir . '/' . $newFilename;
                $targetDir = SITE_ROOT.'data/cache/upload_tmp';
                $uploadDir = $attachDir;

                $cleanupTargetDir = true; // Remove old files
                $maxFileAge = 5 * 3600; // Temp file age in seconds

                // Create target dir
                if (!file_exists($targetDir)) {
                    @mkdir($targetDir);
                }
                // Create target dir
                if (!file_exists($uploadDir)) {
                    @mkdir($uploadDir);
                }
                
                $md5File = @file($targetDir.'/md5list2.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $md5File = $md5File ? $md5File : array();

                if (isset($_REQUEST["md5"]) && array_search($_REQUEST["md5"], $md5File ) !== FALSE ) {
                    die('{"jsonrpc" : "2.0", "result" : null, "id" : "id", "exist": 1}');
                }

                $filePath = $targetDir . '/' . $newFilenameTmp;
                $uploadPath = $uploadDir . '/' . $newFilename;

                // Chunking might be enabled
                $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
                $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;

                // Remove old temp files
                if ($cleanupTargetDir) {
                    if (!is_dir($targetDir) || !$dir = opendir($targetDir)) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
                    }
                    while (($file = readdir($dir)) !== false) {
                        $tmpfilePath = $targetDir . '/' . $file;
                        // If temp file is current file proceed to the next
                        if ($tmpfilePath == "{$filePath}.part") {
                            continue;
                        }

                        // Remove temp file if it is older than the max age and is not the current file
                        if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge)) {
                            @unlink($tmpfilePath);
                        }
                    }
                    closedir($dir);
                }
                // Open temp file
                if (!$out = @fopen("{$filePath}.part", $chunks ? "ab" : "wb")) {
                    die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
                }
                if (!empty($_FILES)) {
                    if ($_FILES["file"]["error"] || !is_uploaded_file($_FILES["file"]["tmp_name"])) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
                    }
                    // Read binary input stream and append it to temp file
                    if (!$in = @fopen($_FILES["file"]["tmp_name"], "rb")) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    }
                } else {
                    if (!$in = @fopen("php://input", "rb")) {
                        die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
                    }
                }
                while ($buff = fread($in, 4096)) {
                    fwrite($out, $buff);
                }
                @fclose($out);
                @fclose($in);
                // Check if file has been uploaded
                if (!$chunks || $chunk == $chunks - 1) {
                    // Strip the temp .part suffix off
                    rename("{$filePath}.part", $filePath);
                    rename($filePath, $uploadPath);
                    array_push($md5File, mymd5($uploadPath));
                    $md5File = array_unique($md5File);
                    file_put_contents($targetDir.'/md5list2.txt', join($md5File, "\n"));
                    //返回前端文件信息数据
                    $target_path = str_replace(SITE_ROOT, '', $uploadPath);
                    if($urltype==1){
                        $file_url = file_url($target_path);
                    }
                    if($urltype==2){
                        $file_url = $target_path;
                    }
                    $msg = array($return_path_key => $file_url, $return_name_key => $fileName,'ext'=>strtolower($extension));
                    $return_array = array(
                        "jsonrpc" => "2.0",
                        "result" => null,
                        "id" => "id",
                        "code" => 'e_2000',
                        "msg" => lang('error', 'e_2000'),
                        "data" => $msg
                    );
                    echo json_encode($return_array);die;
                    //die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
                }
                // Return Success JSON-RPC response
                die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
            }
            //大文件上传独立处理逻辑 结束
            if (isset($_SERVER['HTTP_CONTENT_DISPOSITION']) && preg_match('/attachment;\s+name="(.+?)";\s+filename="(.+?)"/i', $_SERVER['HTTP_CONTENT_DISPOSITION'], $info)) {//HTML5上传
                file_put_contents($tempPath, file_get_contents("php://input"));
                $file_name = urldecode($info[2]);
            } else {//标准表单式上传
                $upfile = @$_FILES[$inputName];
                if (!isset($upfile)) {
                    $return_array = array("code" => 'e_2001', "msg" => lang('error', 'e_2001'), "data" => $msg);
                } elseif (!empty($upfile['error'])) {
                    switch ($upfile['error']) {
                        case '1':
                            $return_array = array("code" => 'e_2002', "msg" => lang('error', 'e_2002'), "data" => $msg);
                            break;
                        case '2':
                            $return_array = array("code" => 'e_2003', "msg" => lang('error', 'e_2003'), "data" => $msg);
                            break;
                        case '3':
                            $return_array = array("code" => 'e_2004', "msg" => lang('error', 'e_2004'), "data" => $msg);
                            break;
                        case '4':
                            $return_array = array("code" => 'e_2005', "msg" => lang('error', 'e_2005'), "data" => $msg);
                            break;
                        case '6':
                            $return_array = array("code" => 'e_2006', "msg" => lang('error', 'e_2006'), "data" => $msg);
                            break;
                        case '7':
                            $return_array = array("code" => 'e_2007', "msg" => lang('error', 'e_2007'), "data" => $msg);
                            break;
                        case '8':
                            $return_array = array("code" => 'e_2008', "msg" => lang('error', 'e_2008'), "data" => $msg);
                            break;
                        case '999':
                        default:
                            $return_array = array("code" => 'e_2009', "msg" => lang('error', 'e_2009'), "data" => $msg);
                    }
                } elseif (empty($upfile['tmp_name']) || $upfile['tmp_name'] == 'none') {
                    $return_array = array("code" => 'e_2005', "msg" => lang('error', 'e_2005'), "data" => $msg);
                } else {
                    $this->upload_file($upfile['tmp_name'], $tempPath);
                    if (!file_exists($tempPath)) {
                        $return_array = array("code" => 'e_2010', "msg" => lang('error', 'e_2010'), "data" => $msg);
                    }
                    $file_name = $upfile['name'];
                }
            }

            if (empty($err)) {
                $fileInfo = pathinfo($file_name);
                $extension = $fileInfo['extension'];
                if (preg_match('/^(' . str_replace(',', '|', $upExt) . ')$/i', $extension)) {
                    $bytes = filesize($tempPath);
                    if ($bytes > $maxAttachSize) {
                        $return_array = array("code" => 'e_2011', "msg" => str_replace('###XXX###', $this->formatBytes($maxAttachSize), lang('error', 'e_2011')), "data" => $msg);
                    } else {
                        switch ($dirType) {
                            case 1: $attachSubDir = '/' . 'day_' . date('Ymd');
                                break;
                            case 2: $attachSubDir = '/' . 'month_' . date('Ym');
                                break;
                            case 3: $attachSubDir = '/' . 'ext_' . $extension;
                                break;
                        }
                        $attachDir = $attachDir . $attachSubDir;
                        if (!is_dir($attachDir)) {
                            @mkdir($attachDir, 0777);
                            @fclose(fopen($attachDir . '/index.htm', 'w'));
                        }
                        PHP_VERSION < '4.2.0' && mt_srand((double) microtime() * 1000000);
                        $newFilename = date("YmdHis") . mt_rand(1000, 9999) . '_' . random(4) . '.' . $extension;
                        $targetPath = $attachDir . '/' . $newFilename;
                        //DEBUG 阿里云OSS处理 TODO 待优化示例 http://help.aliyun.com/manual?spm=0.0.0.0.vVdtHC&helpId=181
                        if ($_G['setting']['aliyun_oss']) {
                            $this->upload_file($tempPath, $targetPath);
                        } else {
                            rename($tempPath, $targetPath);
                            @chmod($targetPath, 0755);
                        }
                        //DEBUG 生成缩略图
                        /*
                          if ($thumb == 1 && !empty($thumb_width) && !empty($thumb_height)) {
                          list($img_width, $img_height) = getimagesize($targetPath);
                          $thumbfile = '../' . $attachSubDir . '/' . $newFilename . '_small.jpg';
                          if ($img_width > $thumb_width || $img_height > $thumb_height) {
                          require_once libfile('class/image');
                          $img = new image;
                          if (!$img->Thumb($targetPath, $thumbfile, $thumb_width, $thumb_height)) {
                          $return_array = array("code" => 'e_2013', "msg" => lang('error', 'e_2013'), "data" => $msg);
                          echo json_ext($return_array);
                          //echo "{'err':'".lang('error','e_2013')."','msg':".$msg."}";
                          die();
                          }
                          } else {
                          $this->upload_file($targetPath, $targetPath . '_small.jpg');
                          }
                          }
                         */
                        if (!empty($thumb) && !empty($thumb_width) && !empty($thumb_height) && $files_store != 1) {
                            if (strpos($thumb_width, ",")) {
                                $thumb_width_array = explode(',', $thumb_width);
                                $thumb_height_array = explode(',', $thumb_height);
                            } else {
                                $thumb_width_array[0] = $thumb_width;
                                $thumb_height_array[0] = $thumb_height;
                            }
                            $thumb_num = count($thumb_width_array) - 1;
                            for ($i = 0; $i <= $thumb_num; $i++) {
                                list($img_width, $img_height) = getimagesize($targetPath);
                                $thumbfile = '../' . $attachSubDir . '/' . $newFilename . '_thumb_' . $thumb_width_array[$i] . '.jpg';
                                if (($img_width > $thumb_width_array[$i]) || ($img_height > $thumb_height_array[$i])) {
                                    require_once libfile('class/image');
                                    $img = new image;
                                    $thumb_result = $img->Thumb($targetPath, $thumbfile, $thumb_width_array[$i], $thumb_height_array[$i]);
                                    if (!$thumb_result) {
                                        $return_array = array("errcode" => 'e_2013', "errmsg" => lang('error', 'e_2013'), "data" => $msg);
                                    }
                                } else {
                                    $this->upload_file($targetPath, $targetPath . '_thumb_' . $thumb_width_array[$i] . '.jpg');
                                }
                            }
                            sleep(1);
                        }
                        $target_path = str_replace(SITE_ROOT, '', $targetPath);
                        $target_path_json = $this->jsonString($target_path);
                        if ($files_store != 1) {
                            if ($msgType == 1) {
                                $msg = "'$target_path_json'";
                            } else {
                                if($urltype==1){
                                    $file_url = $_G['config']['file']['default']['host'].'/'.$target_path;
                                }
                                if($urltype==2){
                                    $file_url = $target_path;
                                }
                                $msg = array($return_path_key => $file_url, $return_name_key => $file_name,'ext'=>strtolower($extension));
                            }
                        } else {
                            //DEBUG 需要数据表pre_file_source存储文件名 返回是文件 fs_id
                            $grade_id = isset($_REQUEST['grade_id']) ? $_REQUEST['grade_id'] : '';
                            $subject_id = isset($_REQUEST['subject_id']) ? $_REQUEST['subject_id'] : '';
                            $file_path_array = explode('/', $target_path);
                            $count_key = count($file_path_array) - 1;
                            $file_name_array = pathinfo($file_name);
                            $data = array(
                                'user_id' => $_G['user_id'],
                                'fs_name' => str_replace('.' . $file_name_array['extension'], '', $file_name),
                                'fs_extension' => strtolower($file_name_array['extension']),
                                'fs_store_path' => trim($attachSubDir, '/'), //TODO 优化各种路径 随配置而变化 现在默认存储路劲是 4
                                'fs_real_name' => str_replace('.' . $file_name_array['extension'], '', $file_path_array[$count_key]),
                                'user_id' => $_G['user_id'],
                                'fs_grade_id' => $grade_id,
                                'fs_subject_id' => $subject_id,
                                'fs_size' => filesize(SITE_ROOT . './' . $target_path),
                                'create_dateline' => TIMESTAMP
                            );
                            $insert_id = DB::insert('file_source', $data, 1);
                            if ($insert_id) {
                                if($urltype==1){
                                    $file_url = $_G['config']['file']['default']['host'].'/'.$target_path;
                                }
                                if($urltype==2){
                                    $file_url = $target_path;
                                }
                                $msg = array('url' => $file_url, 'file_name' => $file_name,'ext'=>strtolower($extension), 'fs_id' => $insert_id);
                            }
                        }
                    }
                } else {
                    $return_array = array("code" => 'e_2014', "msg" => lang('error', 'e_2014') . $upExt, "data" => $msg);
                }
                @unlink($tempPath);
            }
            if (file_exists($targetPath)) {
                //DEBUG 转换JPG 开始
//                {
//                    "flag": "true",
//                    "msg": "解析成功",
//                    "data": {
//                        "filepage": "5",
//                        "filename": "test.doc",
//                        "filelist": [
//                            "http://122.225.51.165:8008/upload/0000photo1.jpg",
//                            "http://122.225.51.165:8008/upload/0000photo2.jpg",
//                            "http://122.225.51.165:8008/upload/0000photo3.jpg",
//                            "http://122.225.51.165:8008/upload/0000photo4.jpg",
//                            "http://122.225.51.165:8008/upload/0000photo5.jpg"
//                        ]
//                    }
//                }
                if ($convert == 'jpg') {
                    $filepage = 0;
                    $filelist = array();
                    $return_array = array(
                        "flag" => 'false',
                        "msg" => "解析失败",
                        "data" => array(
                            "filepage" => $filepage,
                            "filename" => $file_name,
                            "filelist" => array()
                        )
                    );
                    $file_ext = substr($targetPath, -3);
                    $file_ext = strtolower($file_ext);
                    if ($file_ext == 'pdf') {
                        $command_pdf = "mv " . $targetPath . " " . $targetPath . '.pdf';
                        exec($command_pdf, $return_pdf);
                    } else {
                        $command_pdf = "/opt/openoffice4/program/python /opt/openoffice4/program/DocumentConvert.py " . $targetPath . " " . $targetPath . '.pdf';
                        exec($command_pdf, $return_pdf);
                    }
                    if ($return_pdf[0] == 1 || $file_ext == 'pdf' || 1 == 1) {
                        $command_jpg = "gs -dUseCIEColor -dSAFER -dBATCH -dNOPAUSE -dPDFSETTINGS=/screen -sDEVICE=jpeg -dJPEGQ=60 -dTextAlphaBits=4 -dGraphicsAlphaBits=4 -r144 -sOutputFile=" . $targetPath . '.pdf_' . "%03d.jpg " . $targetPath . '.pdf';
                        exec($command_jpg, $return_jpg);
                        $page_max = 999;
                        for ($i = 1; $i < $page_max; $i++) {
                            $file_path_array = explode('/', $target_path);
                            $count_key = count($file_path_array) - 1;
                            if ($i < 10) {
                                $site_path = 'data/upload' . $attachSubDir . '/' . $file_path_array[$count_key] . '.pdf_' . "00" . $i . ".jpg";
                            } elseif ($i < 100) {
                                $site_path = 'data/upload' . $attachSubDir . '/' . $file_path_array[$count_key] . '.pdf_' . "0" . $i . ".jpg";
                            } else {
                                $site_path = 'data/upload' . $attachSubDir . '/' . $file_path_array[$count_key] . '.pdf_' . "" . $i . ".jpg";
                            }
                            $sys_path = SITE_ROOT . $site_path;
                            if (file_exists($sys_path)) {
                                $filelist[] = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $site_path;
                            } else {
                                break;
                            }
                        }
                        $return_array = array(
                            "flag" => 'true',
                            "msg" => "解析成功",
                            "data" => array(
                                "filepage" => ($i - 1),
                                "filename" => $file_name,
                                "filelist" => $filelist
                            )
                        );
                        echo json_ext($return_array);
                        die;
                    }
                } else {
                    //DEBUG 转化JPG 结束
                    $ui = empty($_GET['ui']) ? '' : $_GET['ui'];
                    if ($ui == 'layui') {
                        $code = "0";
                    } else {
                        $code = "1";
                    }
                    $return_array = array("code" => $code, "msg" => lang('error', 'e_2000'), "data" => $msg);
                    //echo "{'err':'".$this->jsonString($err)."','msg':".$msg."}";
                }
                //echo "{'err':'".$this->jsonString($err)."','msg':".$msg."}";
            } else {
                $return_array = array("code" => 'e_2015', "msg" => lang('error', 'e_2015'), "data" => $msg);
            }
        } else {
            //DEBUG 无权限上传
            $return_array = array("code" => 'e_2016', "msg" => lang('error', 'e_2016'), "data" => $msg);
        }
        if (empty($_G['gp_api'])) {
            $_G['gp_api'] = 'json';
        }
        echo format_data($return_array, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        //echo json_ext($return_array);
    }

    public function upload_file($src_file, $target_file) {
        @copy($src_file, $target_file);
        if (!file_exists($target_file) && function_exists('move_uploaded_file')) {
            @move_uploaded_file($src_file, $target_file);
        }
        if (!file_exists($target_file)) {
            if (@is_readable($src_file) && (@$fp_s = fopen($src_file, 'rb')) && (@$fp_t = fopen($target_file, 'wb'))) {
                while (!feof($fp_s)) {
                    $s = @fread($fp_s, 1024 * 512);
                    @fwrite($fp_t, $s);
                }
                fclose($fp_s);
                fclose($fp_t);
            }
        }
    }

    /*
     * ajax 返回信息
     */

    public function return_json($error, $message, $output = 1) {
        $return_array = array(
            "error" => $error,
            "message" => $message
        );
        if ($output) {
            echo json_ext($return_array);
            die;
        } else {
            return json_ext($return_array);
        }
    }

}

?>