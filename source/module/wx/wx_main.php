<?php
/**
* 应用接口控制事件模块文件
* 此应用接口控制事件文件或模块事件程序作用描述
* 
* @filename source/module/wx/wx_main.php 2013-11-11 02:10:56x
* @author DZF <info@dzmvc.com>
* @version 1.0.0
* @since 1.0.0
* @copyright DZF (c) 2013
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
* 应用接口控制事件类
* 应用接口控制事件类名称类的含义说明
* 
* @author 开发者名称 <info@mail.com>
* @version 1.0
* @since 1.0 （可选）
*/
class ctrl_wx_main extends wx_main{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    public $_table = 'wx_main';
    public $_pk    = 'wm_id';
    public $_field_pre    = '';
    public $_keyword    = 'wm_name';
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    
    /**
    * 微信入口
    * 可以 通过数据表主键对应数据的详情信息
    *
    * @api          {get} /wx_main/app/?wm_id=id
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {get} {Number} wm_id 获取数据唯一编号 必传
    * @apiSuccess   {json}
    */
    public function do_app() {
        global $_G;
        
        include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/'.$_G['gp_do'].'');
    }
    
    /**
     * 微信设置 微信信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['common_setting'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_config() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $common_setting = isset($_REQUEST['wx_main']) && !empty($_REQUEST['wx_main']) ? $_REQUEST['wx_main']:'';
        if(!empty($common_setting)){
            $update_row = 0;
            $common_setting['modify_dateline'] = TIMESTAMP;
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($common_setting AS $key => $value){
                $update_data = array(
                    'svalue'=>$value,
                    'modify_dateline'=>TIMESTAMP
                );
                $where=array(
                    'skey'=>$key
                );
                $effect_row = $this->edit('common_setting',$update_data,$where);
                if($effect_row){
                    $update_row++;
                }
            }
            //扩展逻辑 开始
            updatecache('setting');
            //扩展逻辑 结束
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['update_info'] = $common_setting;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($update_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = (string)$update_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "0";
                }
            }
            if($update_row){
               $return['data']['update_info'] = $common_setting; 
            }else{
               $return['data']['update_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($update_row){
                    showmessage('操作成功','index.php?mod=wx&action=main&do=config');
                }else{
                    showmessage('操作失败','index.php?mod=wx&action=main&do=config');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('wx/main/config');
        }
    }
}
?>