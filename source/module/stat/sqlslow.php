<?php

if(!file_exists('./config/config_global.php')){
    header('location: install/index.php');
}
@set_time_limit(0);
@set_magic_quotes_runtime(0);
header('Access-Control-Allow-Origin:*');
require_once './dz_framework/init.php';

$filename = "/www/wwwroot/sql.bozedu.net/mysql-slow.log";
$filecontent = @file_get_contents($filename);
$fileline_array = explode(PHP_EOL, $filecontent);
/*
Time：执行查询的日期时间
User@Host：执行查询的用户和客户端IP
Id：是执行查询的线程Id
Query_time：SQL执行所消耗的时间
Lock_time：执行查询对记录锁定的时间
Rows_sent：查询返回的行数
Rows_examined：为了返回查询的数据所读取的行数
示例:
# Time: 190801 13:38:04
# User@Host: root[root] @  [172.16.4.121]
# Query_time: 1.072833  Lock_time: 0.000022 Rows_sent: 0  Rows_examined: 3628
SET timestamp=1564637884;
UPDATE  des_school_member_create SET `createid`='199219' , `username`='lisha' WHERE classid='4781';
*/
$sqlslow_stat = array();
foreach ($fileline_array as $key => $value) {
    //获取起始行 Time:
//    echo '@@@@@@'.$value.'@@@@@@';
//    echo $fileline_array[0];
//    echo '<br />';
//    $fileline_count = count($fileline_array)-4;
//    echo $fileline_array[$fileline_count];
//    die('ok');
    if(strpos($value,'User@Host')){
        //执行时间
        $do_timestamp = str_replace(array('SET timestamp=',';'), array('',''), $fileline_array[($key+2)]);
        $date = date('Y-m-d',$do_timestamp);
        $datetime = date('H:i:s',$do_timestamp);
        $sqlslow_stat_day['sss_date']= $date;
        $sqlslow_stat_day['sss_time']= $datetime;
        //执行效率
        $do_time = str_replace(array('# Query_time: ','  Lock_time: ',' Rows_sent: ',' Rows_examined: '), array('',',',',',','), $fileline_array[($key+1)]);
        $do_time = explode(',', $do_time);
        $sqlslow_stat_day['sss_query_time']= $do_time[0];
        $sqlslow_stat_day['sss_lock_time']= $do_time[1];
        $sqlslow_stat_day['sss_rows_sent']= $do_time[2];
        $sqlslow_stat_day['sss_rows_examined']= $do_time[3];
        //执行SQL内容
        $sqlslow_stat_day['sss_content']= trim($fileline_array[($key+3)]);
        if(strpos($sqlslow_stat_day['sss_content'],'where')){
            $sss_content_array = explode('where', $sqlslow_stat_day['sss_content']);
        }
        if(strpos($sqlslow_stat_day['sss_content'],'WHERE')){
            $sss_content_array = explode('WHERE', $sqlslow_stat_day['sss_content']);
        }
        if(empty($sss_content_array[0])){
            $sss_content_array[0] = $sqlslow_stat_day['sss_content'];
        }
        //执行SQL类型
        $sqlslow_stat_day['sss_short']= $sss_content_array[0];
        //echo var_dump($sqlslow_stat_day);
        $code = @DB::insert('stat_sqlslow',$sqlslow_stat_day,true);
        if($code){
            $code_count++;
        }
        $sqlslow_stat[$date][] = $sqlslow_stat_day;
        if($code_count == 5 && 1==2){
           break(2); 
        }
        
    }
}
echo '@@@'.$key.'@@@_###'.$code_count.'###';