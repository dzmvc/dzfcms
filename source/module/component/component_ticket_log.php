<?php
/**
 * 模块信息事件类
* @filename source/module/component/component_ticket_log.php 2013-11-11 02:10:56x
* @author DZF <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_component_ticket_log extends component_ticket_log{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    public $_table = 'component_ticket_log';
    public $_pk    = 'ctl_id';
    public $_field_pre    = '';
    public $_keyword    = 'ctl_content';
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_pk){
            if($_G['member']['area_id1']){
                $where['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $where['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $where['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['area_id4']){
                $where['area_id4'] = $_G['member']['area_id4'];
            }
            if($_G['member']['sm_id']){
                $where['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
                $where['create_user_id'] = $_G['member']['user_id'];
            }
            $where[$this->_pk] = $_pk;
            $detail = $this->get_field_value_by_mids($this->_table, $where, '*' ,'', 1);
            $detail = $detail[0];
            //$detail = $this->one_info($this->_table,$_pk,$_pk);   
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $detail;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            //include template('component/ticket_log/detail');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['component_ticket_log'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $table_data = isset($_REQUEST[$this->_table]) && !empty($_REQUEST[$this->_table]) ? $_REQUEST[$this->_table]:'';
        //DEBUG 插入数据
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($table_data AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $table_data[$key] = implode(",", $value);
                }
            }
            //更新工单状态
            if($table_data['ct_id']){
                $ct_status = 1;//处理中
                //如果是开发人员回复 工单状态改为 待回复
                $bzkf1 = array('bzkf1','bzkf2','bzkf3','bzkf4','bzkf5','bzkf6','bzkf7','bzkf8','bzkf9','bzkf10','bzkf11','bzkf12','bzkf13','bzkf14','bzkf15','bzkf16','bzkf17','bzkf18','bzkf19','bzkf20','bzkf21','bzkf22','bzkf23','bzkf24','bzkf25','bzkf26','bzkf27','bzkf28','bzkf29','bzkf30');
                $bzkf2 = array('bzkf31','bzkf32','bzkf33','bzkf34','bzkf35','bzkf36','bzkf37','bzkf38','bzkf39','bzkf40','bzkf41','bzkf42','bzkf43','bzkf44','bzkf45','bzkf46','bzkf47','bzkf48','bzkf49','bzkf50','bzkf51','bzkf52','bzkf53','bzkf54','bzkf55','bzkf56','bzkf57','bzkf58','bzkf59','bzkf60');
                if($_G['user_role_id']=='5'){
                    $ct_status = 1;//待回复 开发者回复后 状态变更为 待提问者回复
                }else{
                    if($_G['user_role_id'] < 10 || in_array($_G['user_name'], $bzkf1)){
                        $ct_status = '1';              
                    }else{
                        $ct_status = '0';
                    }
                }
                $data = array(
                    'ct_status' => $ct_status
                );
                if($_G['user_name'] != 'bzkf10' && in_array($_G['user_name'], $bzkf1) && $_G['user_name']){
                    $data['ct_master'] = $_G['user_name'];
                }
                $condition = array(
                    'ct_id'=>$table_data['ct_id']
                );
                @DB::update('component_ticket', $data, $condition);
            }
            $table_data['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $table_data['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $table_data['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $table_data['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['area_id4']){
                $table_data['area_id4'] = $_G['member']['area_id4'];
            }
            if($_G['member']['sm_id']){
                $table_data['sm_id'] = $_G['member']['sm_id'];
            }
            if($_G['member']['user_id']){
                $table_data['create_user_id'] = $_G['member']['user_id'];
            }
            $insert_id = $this->add($this->_table,$table_data);
            $detail = $this->one_info($this->_table, $this->_pk, $insert_id);
            $component_ticket = $this->one_info('component_ticket', 'ct_id', $table_data['ct_id']);
            //开发回复通知问题创建人 问题创建人回复通知开发
            if(in_array($_G['user_name'], $bzkf1) && $_G['user_name'] !='bzkf10'){
                //开发回复 通知问题创建人
                $user = $this->one_info('user', 'user_id', $component_ticket['create_user_id']);
            }else{
                //非开发回复 通知问题创建人
                //获取工单master对应的openid
                if($component_ticket['ct_master']){
                    $user = $this->one_info('user', 'user_name', $component_ticket['ct_master']);
                }
            }
            if($insert_id && $user['openid']){
               //引入微信方法
                $file_function_wx = SITE_ROOT.'./source/function/function_wx.php';
                include  $file_function_wx;
                //测试微信消息提醒的发送
                /**
                * 发送工单进度通知消息
                * @param $touser 用户openid
                * @param $title 工单标题
                * @param $bandnum 工单编号
                * @param $timeline 工单时间
                * @param $content 工单内容
                * @param $redirect 跳转url
                * @return mixed
                */
                $touser = $user['openid'];
                $title = '您负责的开发任务,有新的回复信息';
                $bandnum = $table_data['ct_id'];
                $timeline = date('Y-m-d H:i:s',TIMESTAMP);
                $content = '请尽快进一步回复和处理该问题,'.$table_data['ctl_content'];
                $redirect = $_G['setting']['sitedomain_mobile'].'';
                $return_wxtpl_workband = @wxtpl_workband($touser,$title,$bandnum,$timeline,$content,$redirect);
            }
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($api) {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($detail){
               $return['data']['one_info'] = $detail;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=component&action=ticket_log&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=component&action=ticket_log&do=index');
                }
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('component/ticket_log/add');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['component_ticket_log'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $return['code'] = '1';
        $return['msg'] = '';
        $table_data = isset($_REQUEST[$this->_table]) && !empty($_REQUEST[$this->_table]) ? $_REQUEST[$this->_table]:'';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_pk){
            $where = array($this->_pk=>$_pk);
//            if($_G['member']['area_id1']){
//                $where['area_id1'] = $_G['member']['area_id1'];
//            }
//            if($_G['member']['area_id2']){
//                $where['area_id2'] = $_G['member']['area_id2'];
//            }
//            if($_G['member']['area_id3']){
//                $where['area_id3'] = $_G['member']['area_id3'];
//            }
//            if($_G['member']['area_id4']){
//                $where['area_id4'] = $_G['member']['area_id4'];
//            }
//            if($_G['member']['sm_id']){
//                $where['sm_id'] = $_G['member']['sm_id'];
//            }
//            if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
//                $where['create_user_id'] = $_G['member']['user_id'];
//            }
            if('1'==$issubmit){
                $table_data['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($table_data AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $table_data[$key] = implode(",", $value);
                    }
                }
                $table_data['modify_user_id'] = $_G['member']['user_id'];
                $effect_row = $this->edit($this->_table,$table_data,$where);
            }
            //DEBUG 获取操作对象信息
            $detail = $this->get_field_value_by_mids($this->_table, $where, '*' ,'', 1);
            $detail = $detail[0];
            $detail = $this->one_info($this->_table,$this->_pk,$_pk);
            if($detail){
               $return['data']['one_info'] = $detail;
            }else{
               $return['data']['one_info'] = array();  
            }
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['data']['one_info'] = $detail;
        if ($api) {
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=component&action=ticket_log&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=component&action=ticket_log&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            //include template('component/ticket_log/edit');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['ctl_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $_pk = isset($_REQUEST[$this->_pk]) ? $_REQUEST[$this->_pk] : '';
        if($_G['member']['area_id1']){
            $where['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $where['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $where['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['area_id4']){
            $where['area_id4'] = $_G['member']['area_id4'];
        }
        if($_G['member']['sm_id']){
            $where['sm_id'] = $_G['member']['sm_id'];
        }
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $where['create_user_id'] = $_G['member']['user_id'];
        }
        if(is_array($_pk)){
            foreach($_pk AS $key => $value){
                $where[$this->_pk]=$value;
                $effect_row = $this->delete($this->_table, $where, $limit=1);
            }
        }else{
            if($_pk){
                $where[$this->_pk]=$_pk;
                $effect_row = $this->delete($this->_table, $where, $limit=1);
            }
        }
        //DEBUG 返回信息
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if($api){
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=component&action=ticket_log&do=index');
            }else{
                showmessage('操作失败','index.php?mod=component&action=ticket_log&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND ".$this->_keyword." LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        $ct_id = isset($_REQUEST['ct_id']) ? $_REQUEST['ct_id'] : '';
        if($ct_id){
            $wheresql = " AND ct_id = '".$ct_id."' ";
            $wherearray['ct_id'] = $ct_id;
        }
        //循环筛选项目 扩展搜索字段筛选条件 结束
        $bzkf1 = array('bzkf1','bzkf2','bzkf3','bzkf4','bzkf5','bzkf6','bzkf7','bzkf8','bzkf9','bzkf10','bzkf11','bzkf12','bzkf13','bzkf14','bzkf15','bzkf16','bzkf17','bzkf18','bzkf19','bzkf20','bzkf21','bzkf22','bzkf23','bzkf24','bzkf25','bzkf26','bzkf27','bzkf28','bzkf29','bzkf30');
        $bzkf2 = array('bzkf31','bzkf32','bzkf33','bzkf34','bzkf35','bzkf36','bzkf37','bzkf38','bzkf39','bzkf40','bzkf41','bzkf42','bzkf43','bzkf44','bzkf45','bzkf46','bzkf47','bzkf48','bzkf49','bzkf50','bzkf51','bzkf52','bzkf53','bzkf54','bzkf55','bzkf56','bzkf57','bzkf58','bzkf59','bzkf60');
//        if($_G['user_role_id'] < 10 || in_array($_G['user_name'], $bzkf1)){
//
//        }else{
//            if($_G['member']['area_id1']){
//                $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
//                $wherearray['area_id1'] = $_G['member']['area_id1'];
//            }
//            if($_G['member']['area_id2']){
//                $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
//                $wherearray['area_id2'] = $_G['member']['area_id2'];
//            }
//            if($_G['member']['area_id3']){
//                $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
//                $wherearray['area_id3'] = $_G['member']['area_id3'];
//            }
//            if($_G['member']['area_id4']){
//                $wheresql .= " AND area_id4 = '".$_G['member']['area_id4']."' ";
//                $wherearray['area_id4'] = $_G['member']['area_id4'];
//            }
//            if($_G['member']['sm_id']){
//                $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
//                $wherearray['sm_id'] = $_G['member']['sm_id'];
//            }
//        }
        $orderby = " ORDER BY ".$this->_pk." DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index($this->_table,$page_condition);
        $page_data = array();
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $wherearray = array('ct_id'=>$ct_id);
        $detail = $this->get_field_value_by_mids('component_ticket', $wherearray, '*' ,'', 1);
        $detail = format_info($detail[0], $_G['gp_api'], $_G['gp_mod'], 'ticket', $_G['gp_do']);
        if(empty($detail['ct_files']) || is_null($detail['ct_files'])){
            $detail['ct_files'] = array();
        }else{
            if($api=='json'){
                $ct_files = explode(',', $detail['ct_files']);
               foreach($ct_files AS $k => $v){
                   $ct_files[$k] = $_G['setting']['site_oss_api'].'/'.$v;
               }
               $detail['ct_files'] = array_values($ct_files);
            }
        }
        $detail['create_dateline'] = date('Y-m-d H:i:s',$detail['create_dateline']);
        $page_result['component_ticket'] = $detail;
        foreach($page_result['page_data'] AS $key => $value){
            if(empty($value['ctl_files']) || is_null($value['ctl_files'])){
                $value['ctl_files'] = '';
            }else{
                if($api=='json'){
                    $files = explode(',', $value['ctl_files']);
                   foreach($files AS $k => $v){
                       $files[$k] = $_G['setting']['site_oss_api'].'/'.$v;
                   }
                   $value['ctl_files'] = array_values($files);
                }
            }
            $value['create_dateline'] = date('Y-m-d H:i:s',$value['create_dateline']);
            $page_data[] = $value;
        }
        $page_result['page_data'] = $page_data;
        //DEBUG 返回信息
        if($site){
            $page_data = array();
            foreach($page_result['page_data'] AS $key => $value){
                $tmp = array();//格式化选择性输出
                $tmp = remove_field_pre($value, $this->_field_pre);
                $page_data[] = $tmp;
            }
            $page_result['page_data'] = $page_data;   
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        $return['template']['wheresql']=$wherearray;
        if($api && $api !='html' && $api !='xls'){
//            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
//            $module_table = array();
//            if(file_exists($module_table_path)){
//                $module_table = json_decode(file_get_contents($module_table_path),true);
//            }
//            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($format_return['data']['page_data'] AS $key => $value) {
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($format_return['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($format_return['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $value[$k]);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $value[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[] = $tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=component&action=ticket_log&do=index&keyword=".$keyword);
            //include template('component/ticket_log/index');
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }

    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['company_qygsbxgl'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_import() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 0;
        $return['template']['hidden_left'] = 0;
        $return['template']['hidden_footer'] = 0;
        $return['template']['hidden_user_menu'] = 0;
        $return['template']['hidden_logo_link'] = 0;
        if ($_FILES) {
//            $csv = new csv($_FILES['file']['tmp_name']);
//            $csv_data = $csv->csv_to_array_to_utf8();
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $excel_file_path = $_FILES['file']['tmp_name'];
            $excel = new excel();
            $excel_data = $excel->read_excel($excel_file_path);
            $sheet_data = $excel_data[0];//返回第一页
            //填写校验
//            foreach ($sheet_data as $key => $val) {
//                if (!$flag) {
//                    $msg = "";
//                    if ($nodate) {
//                        $nodatestr = implode($nodate, ',');
//                        $msg .= "第{$nodatestr}行没有填写;";
//                    }
//                    $result['code'] = 0;
//                    $result['msg'] = $msg;
//                    exit(json_encode($result));
//                }
//            }
            $insert_num = $effect_num = 0;
            $import_data = $insert_data = $effect_data = array();
            $table_structure = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($sheet_data as $sk => $sv) {
                if(($sv[0] !='#' && $sv[0] != $table_structure['table_key']) && $sk > 4){
                    $tmp = array();
                    foreach($sv AS $skk => $svv){
                        if(in_array($sheet_data[3][$skk], $trim_field)){
                            $svv = trim($svv);
                        }
                        $tmp[$sheet_data[3][$skk]]=$svv;
                    }
                    
                    $import_data[$sk] = $tmp;
                    //去除主键 开始
                    unset($tmp['#']);
                    unset($tmp[$table_structure['table_key']]);
                    //去除主键 结束
                    $table_name = $_G['gp_mod'].'_'.$_G['gp_action'];
                    if(empty($sv[0])){
                        $tmp['create_dateline'] = TIMESTAMP;
                        $insert_id = DB::insert($table_name, $tmp,true);
                        if($insert_id){
                            $insert_num++;
                            $insert_data[] = $import_data[$sk];
                        }
                    }else{
                        //编辑
                        $where = array(
                            $table_structure['table_key']=>$sv[0],
                        );
                        $effect_id = DB::update($table_name, $tmp, $where);
                        if($effect_id){
                            $tmp['modify_dateline'] = TIMESTAMP;
                            @DB::update($table_name, $tmp, $where);
                            $effect_num++;
                            $effect_data[] = $import_data[$sk];
                        }
                    }
                }
            }
            if ($insert_num || $effect_num) {
                $result['code'] = 1;
                if($insert_num){
                    $result['msg'] = "成功导入{$insert_num}条数据<br />";
                }
                if($effect_num){
                    $result['msg'] .= "成功更新{$effect_num}条数据<br />";
                }
                $result['msg'] .= "请核对导入是否正确<br />";
                //返回HTML
                $colgroup_col = '';
                $thead_tr_td = '';
                foreach ($sheet_data[4] as $hk => $hv) {
                    $colgroup_col .= '<col width="">';
                    $thead_tr_td .= '<th>'.$hv.'</th>';
                }
                $result['data']['dhtml']='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 28px;"><legend>已导入(更新)数据明细</legend></fieldset><table class="layui-table" lay-skin="line">
                    <colgroup>
                    '.$colgroup_col.'
                    <col>
                    </colgroup>
                  <thead>				
                    <tr>'.$thead_tr_td.'</tr> 
                  </thead>
                <tbody>';
                foreach($insert_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                foreach($effect_data as $key => $value){
                    $result['data']['dhtml'] .= '<tr><td>'.implode('</td><td>', $value).'</td></tr>';
                }
                $result['data']['dhtml'] .='</tbody></table>';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            } else {
                $result['code'] = 0;
                $result['msg'] = '导入数据未有新增或修改<br />';
                $result['data']['dhtml']='<h2 style="text-align:center;color:red;font-weight:bold;">'.$result['msg'].'<br /><a class="layui-btn" href="javascript:history.go(-1);">返回</a></h2>';
            }
            exit(json_encode($result));
        }else{
            include template(''.$_G['gp_mod'].'/'.$_G['gp_action'].'/global/'.$_G['gp_do'].'');
        }
    }
}
?>