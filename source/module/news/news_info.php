<?php
/**
 * 模块信息事件类
* @filename source/module/news_info.php 2013-11-11 02:10:56x
* @author Huming Xu <info@dzmvc.com>
* @version 1.0.0
* @copyright DZF (c) 2013, Huming Xu
*/
if(!defined('IN_SITE')) {
    exit('Access Denied');
}
/**
 * 模块信息事件类
 * @author yangw <2441069162@qq.com>
 * @copyright (c) 2017-11-06 api.bozedu.net $
 * @version 1.0
 */
class ctrl_news_info extends news_info{
    public $info_array = array();
    public $page_array = array();
    public $tree_array = array();
    
    static function &instance() {
        static $object;
        if(empty($object)) {
                $object = new self();
        }
        return $object;
    }
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_detail() {
        global $_G;
        $detail = array();
        $id = isset($_REQUEST['id']) ? $_REQUEST['id'] : '';
        if($id){
            $ni_id = $id;
        }else{
            $ni_id = isset($_REQUEST['ni_id']) ? $_REQUEST['ni_id'] : '';
        }
        if($ni_id){
            if($ni_id > 6){
                $where['area_id1'] = !empty($_G['member']['area_id1']) ? $_G['member']['area_id1']:"0";
                $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
                $where['area_id2'] = !empty($_G['member']['area_id2']) ? $_G['member']['area_id2']:"0";
                $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
                $where['area_id3'] = !empty($_G['member']['area_id3']) ? $_G['member']['area_id3']:"0";
                $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
                $where['sm_id'] = !empty($_G['member']['sm_id']) ? $_G['member']['sm_id']:"0";
                $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
            }
            $where['ni_id'] = $ni_id;
            $detail = $this->get_field_value_by_mids('news_info', $where, '*', 1);
            $detail = $detail[0];
            $info = array();
            if($detail){
                //$detail = $this->one_info('news_info','ni_id',$ni_id);
                $detail['ni_img'] = file_url($detail['ni_img']);
                $detail['ni_content'] = stripslashes($detail['ni_content']);
                $detail['ni_content'] = str_replace('<img src="data/', '<img src="'.$_G['setting']['site_oss_api'].'data/', $detail['ni_content']);
                $detail['ni_content'] = str_replace('<img src="/', '<img src="'.$_G['setting']['site_oss_api'].'/', $detail['ni_content']);
                $detail['create_dateline_format'] = date('Y-m-d',$detail['create_dateline']);   
                //格式化详情信息
                $info = array(
                    "id"=> $detail['ni_id'],//信息编号
                    "cate_name"=> $detail['ni_tag'],
                    "name"=> $detail['ni_title'],
                    "img"=> $detail['ni_img'],
                    "content"=> $detail['ni_content'],
                    "fbr"=> $detail['ni_fbr'],
                    "create_dateline_format"=> $detail['create_dateline_format'],
                    "icon"=> "",
                    "cate"=> "1",
                    "type"=> "1",
                    "action"=> "",
                    "do"=> "",
                    "url"=> ""
                );
            }
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['one_info'] = $info;
            echo fast_format_data($return);
        }else{
            include template('news/info/detail');
        }
    }
    
    /**
     * 添加一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['news_info'] 需要添加的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_add() {
        global $_G;
        //TODO issubmit value 增加 _CSRF HASH 校验
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $news_info = isset($_REQUEST['news_info']) && !empty($_REQUEST['news_info']) ? $_REQUEST['news_info']:'';
        //DEBUG 插入数据
        
        if('1'==$issubmit){
            //TODO 后端字段校验 根据具体业务逻辑添加
            //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
            foreach($news_info AS $key => $value){
                if(is_array($value) && !empty($value)){
                    $news_info[$key] = implode(",", $value);
                }
            }
            unset($news_info['file']);
            $news_info['create_dateline'] = TIMESTAMP;
            if($_G['member']['area_id1']){
                $news_info['area_id1'] = $_G['member']['area_id1'];
            }
            if($_G['member']['area_id2']){
                $news_info['area_id2'] = $_G['member']['area_id2'];
            }
            if($_G['member']['area_id3']){
                $news_info['area_id3'] = $_G['member']['area_id3'];
            }
            if($_G['member']['sm_id']){
                $news_info['sm_id'] = $_G['member']['sm_id'];
            }

            if($_G['user_role_id'] >= 72){
                $type = 2;
                $content = $news_info['ni_title'];
                $target[] = 'sm_id_'.$_G['member']['sm_id'];
                $return = jpush_send($type, $target, $content);
            }


            $insert_id = $this->add('news_info',$news_info);
            $info = $this->one_info('news_info', 'ni_id', $insert_id);
        }
        //信息关联菜单设置
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        if ($api && $api!='html') {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($insert_id){
                    $return['code'] = '1';
                    $return['data']['insert_id'] = $insert_id; 
                }else{
                    $return['code'] = '0';
                    $return['data']['insert_id'] = "";  
                }
            }
            if($info){
               $return['data']['one_info'] = $info;  
            }else{
               $return['data']['one_info'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($insert_id){
                    showmessage('操作成功','index.php?mod=news&action=info&do=index');  
                }else{
                    showmessage('操作失败','index.php?mod=news&action=info&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('news/info/add');
        }
    }
    
    /**
     * 编辑一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST string $issubmit 是否表单提交校验 issubmit value 增加 _CSRF HASH 校验 必填
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST array $_REQUEST['news_info'] 需要编辑的表单数据 数组key可以直接对应字段名称 就不用二次名称转换 必填
     */
    public function do_edit() {
        global $_G;
        $issubmit = isset($_REQUEST['issubmit']) && !empty($_REQUEST['issubmit']) ? $_REQUEST['issubmit']:'';
        $news_info = isset($_REQUEST['news_info']) && !empty($_REQUEST['news_info']) ? $_REQUEST['news_info']:'';
        $ni_id = isset($_REQUEST['ni_id']) ? $_REQUEST['ni_id'] : '';
        if($ni_id){
            if('1'==$issubmit){
                unset($news_info['file']);
                $news_info['modify_dateline'] = TIMESTAMP;
                //DEBUG 判断是否数组字段,如果是数组转换为逗号间隔 字符串存贮
                foreach($news_info AS $key => $value){
                    if(is_array($value) && !empty($value)){
                        $news_info[$key] = implode(",", $value);
                    }
                }
                $where = array('ni_id'=>$ni_id);
                $where['area_id1'] = !empty($_G['member']['area_id1']) ? $_G['member']['area_id1']:"0";
                $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
                $where['area_id2'] = !empty($_G['member']['area_id2']) ? $_G['member']['area_id2']:"0";
                $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
                $where['area_id3'] = !empty($_G['member']['area_id3']) ? $_G['member']['area_id3']:"0";
                $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
                $where['sm_id'] = !empty($_G['member']['sm_id']) ? $_G['member']['sm_id']:"0";
                $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
                if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
                    $where['create_user_id'] = $_G['member']['user_id'];
                }

//                if($_G['user_role_id'] >= 72){
//                    $type = 2;
//                    $content = $news_info['ni_title'];
//                    $target[] = 'sm_id_'.$_G['member']['sm_id'];
//                    $return = jpush_send($type, $target, $content);
//                }

                $effect_row = $this->edit('news_info',$news_info,$where);
            }
            //DEBUG 获取操作对象信息
            $info = $this->one_info('news_info','ni_id',$ni_id);
            $info['ni_content'] = stripslashes($info['ni_content']);
            //DEBUG 默认设置发送部门初始化JSON
            $nic_id_tree = array();
            $nic_id = explode(',',$info['nic_id']);
            $nic_id_name = explode(',',$info['nic_id']);
            foreach($nic_id AS $key => $value){
                $nic_id_tree[] = array(
                    'name'=>$nic_id_name[$key],
                    'value'=>$value
                );
            }
            $info['nic_id_tree']=json_encode($nic_id_tree);
            if($info){
               $return['data']['one_info'] = $info; 
            }else{
               $return['data']['one_info'] = array();  
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        if ($api && $api!='html') {
            $return['code'] = '1';
            $return['msg'] = '';
            if ($issubmit == 1) {
                if($effect_row){
                    $return['code'] = '1';
                    $return['data']['update_row'] = $effect_row;
                }else{
                    $return['code'] = '0';
                    $return['data']['update_row'] = "";
                }
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($issubmit==1){
                if($effect_row){
                    showmessage('操作成功','index.php?mod=news&action=info&do=index');
                }else{
                    showmessage('操作失败','index.php?mod=news&action=info&do=index');
                } 
            }
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            include template('news/info/edit');
        }
    }
    
    /**
     * 删除一条数据信息
     * @author wangdi <834261229@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP POST/GET int $_REQUEST['ni_id'] 需要删除的数据主键编号 必填
     */
    public function do_delete() {
        global $_G;
        $ni_id = isset($_REQUEST['ni_id']) ? $_REQUEST['ni_id'] : '';
        $where['area_id1'] = !empty($_G['member']['area_id1']) ? $_G['member']['area_id1']:"0";
        $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        $where['area_id2'] = !empty($_G['member']['area_id2']) ? $_G['member']['area_id2']:"0";
        $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        $where['area_id3'] = !empty($_G['member']['area_id3']) ? $_G['member']['area_id3']:"0";
        $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        $where['sm_id'] = !empty($_G['member']['sm_id']) ? $_G['member']['sm_id']:"0";
        $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $where['create_user_id'] = $_G['member']['user_id'];
        }
        if (is_array($ni_id)) {
            foreach ($ni_id AS $key => $value) {
                $where["ni_id"] = $value;
                $effect_row = $this->delete("news_info", $where, $limit = 1);
            }
        } else {
            if ($ni_id) {
                $where["ni_id"] = $ni_id;
                $effect_row = $this->delete("news_info", $where, $limit = 1);
            }
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        if ($api && $api!='html') {
            if($effect_row){
                $return['code'] = '1';
                $return['data'] = array('delete_rows'=>$effect_row);
            }else{
                $return['code'] = '0';
                $return['data'] = array();  
            }
            echo format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            if($effect_row){
                showmessage('操作成功','index.php?mod=news&action=info&do=index');
            }else{
                showmessage('操作失败','index.php?mod=news&action=info&do=index');
            }
        }
    }
    
    /**
     * 获取一页数据列表信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET string $_REQUEST['api'] 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     * HTTP GET int $_REQUEST['page'] 当前页 选填 默认 第一页 选填
     * HTTP POST string $_REQUEST['keyword'] 查询搜索关键字 选填
     */
    public function do_index(){
        global $_G;
        $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $wheresql = "";
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND ni_title LIKE '%".$keyword."%' ";
        }
        //循环筛选项目 扩展搜索字段筛选条件 开始
        $ni_fbr = isset($_REQUEST["ni_fbr"]) ? $_REQUEST["ni_fbr"] :"";
        if($ni_fbr){
            $wheresql .= " AND ni_fbr = '".$ni_fbr."' ";
        }
        $ni_cate = isset($_REQUEST["ni_cate"]) ? $_REQUEST["ni_cate"] :"";
        if($ni_cate){
            $wheresql .= " AND ni_cate = '".$ni_cate."' ";
        }
        $where['area_id1'] = !empty($_G['member']['area_id1']) ? $_G['member']['area_id1']:"0";
        $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
        $where['area_id2'] = !empty($_G['member']['area_id2']) ? $_G['member']['area_id2']:"0";
        $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
        $where['area_id3'] = !empty($_G['member']['area_id3']) ? $_G['member']['area_id3']:"0";
        $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
        $where['sm_id'] = !empty($_G['member']['sm_id']) ? $_G['member']['sm_id']:"0";
        $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $wheresql .= " AND create_user_id = '".$_G['member']['user_id']."' ";
            $wherearray['create_user_id'] = $_G['member']['user_id'];
        }
        $nic_id = isset($_REQUEST["nic_id"]) ? $_REQUEST["nic_id"] :"";
        if($nic_id){
            $wheresql .= " AND FIND_IN_SET(".$nic_id.",nic_id) ";
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束

        //循环筛选项目 扩展搜索字段筛选条件 结束

        $orderby = " ORDER BY ni_id DESC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('news_info',$page_condition);
        if($site){
            $page_data = array();
            foreach($page_result['page_data'] AS $key=>$value){
                $page_data[] = array(
                    'id'=>$value['ni_id'],//唯一编号 ni_id
                    'cate_name'=>$value['ni_tag'],//ni_tag
                    'name'=>$value['ni_title'],//ni_title
                    'datetime'=>date('Y年m月d日 H：i',$value['create_dateline']),//ni_title
                    'brief'=> cutstr(strip_tags($value['ni_content']), 256, ''),//ni_title
                    'icon'=>'',//预留暂不处理 系统图标网址 http 打头
                    "cate"=>"1",//预留暂不处理 系统分类
                    "type"=>"1",//预留暂不处理 系统类型 1 原生 2 嵌入网址H5 APP
                    'action'=>'',//预留暂不处理
                    'do'=>'',//预留暂不处理
                    'url'=>''//系统服务API或H5嵌入网址 根据 role_system.type 判断 1 API 通信域名 2嵌入网址 
                );
            }
            $page_result['page_data'] = $page_data;  
        }
        //DEBUG 返回信息
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        $return['template']['page_condition'] = $page_condition;
        if($api && $api!='html'){
            $module_table_path = './source/module/'.$_G['gp_mod'].'/dbtable/'.$_G['gp_mod'].'_'.$_G['gp_action'].'.json';
            $module_table = array();
            if(file_exists($module_table_path)){
                $module_table = json_decode(file_get_contents($module_table_path),true);
            }
            $page_result['module_table'] = $module_table['module_table'];
            $return['code'] = '1';
            $return['data'] = $page_result;
            echo $format_return = format_data($return, $_G['gp_api'], $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
        }else{
            $format_return = format_data($return, '', $_G['gp_mod'], $_G['gp_action'], $_G['gp_do']);
            $multipage = multi($page_result['total_rows'], $perpage, $page, "index.php?mod=news&action=info&do=index&keyword=".$keyword);
            include template('news/info/index');
        }
    }
    
    /**
    * 列表数据
    * 获取一页数据列表信息
    *
    * @api          {post} /user_org/index
    * @author       作者名称 <info@mail.com>
    * @version      1.0
    * @since        1.0
    * @apiParam     {post} {Object}  必传 user_org
    * @apiSuccess   {json} 返回JSON内容
    */
    public function do_api_tree(){
        global $_G;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $site = isset($_REQUEST['site']) ? $_REQUEST['site'] : '';
        $page = empty($_REQUEST['page']) ? '1':intval($_REQUEST['page']);
        $perpage = $limit = empty($_REQUEST['limit']) ? '10':intval($_REQUEST['limit']);
        $start=(($page-1) * $perpage);
        $type = empty($_REQUEST['type']) ? 'table_tree':$_REQUEST['type'];//根据参数控制返回不同前端插件需要的格式 table_tree xm_select
        $wheresql = "";
        $wherearray = array();
        $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
        if($keyword){
            $wheresql = " AND nic_name LIKE '%".$keyword."%' ";
            $wherearray['keyword'] = $keyword;
        }
        $nic_pid = isset($_REQUEST['nic_pid']) ? $_REQUEST['nic_pid'] : '0';
        if($nic_pid !=''){
            $wheresql = " AND nic_pid = '".$nic_pid."' ";
            $wherearray['nic_pid'] = $nic_pid;
        }
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 开始
        
        //循环筛选项目 主要是循环字段定义的 select、radio、checkbox 结束
        //循环筛选项目 扩展搜索字段筛选条件 开始
        
        //循环筛选项目 扩展搜索字段筛选条件 结束
        if($_G['member']['area_id1']){
            $wheresql .= " AND area_id1 = '".$_G['member']['area_id1']."' ";
            $wherearray['area_id1'] = $_G['member']['area_id1'];
        }
        if($_G['member']['area_id2']){
            $wheresql .= " AND area_id2 = '".$_G['member']['area_id2']."' ";
            $wherearray['area_id2'] = $_G['member']['area_id2'];
        }
        if($_G['member']['area_id3']){
            $wheresql .= " AND area_id3 = '".$_G['member']['area_id3']."' ";
            $wherearray['area_id3'] = $_G['member']['area_id3'];
        }
        if($_G['member']['sm_id']){
            $wheresql .= " AND sm_id = '".$_G['member']['sm_id']."' ";
            $wherearray['sm_id'] = $_G['member']['sm_id'];
        }
        if($_G['member']['user_id'] && $_G['member']['user_level_id']==1 && $api =='layui'){
            $wheresql .= " AND create_user_id = '".$_G['member']['user_id']."' ";
            $wherearray['create_user_id'] = $_G['member']['user_id'];
        }
        $orderby = " ORDER BY nic_sort ASC,nic_id ASC ";
        //DEBUG 初始化请求请求获取一页列表数据的参数
        $page_condition=array(
            'page' => $page,//int 请求页面 页码
            'limit' => $limit,//int 每页请求个数
            'perpage' => $perpage,//int 每页显示个数
            'wheresql' => $wheresql,//string //条件SQL语句 
            'orderby' => $orderby,//string 排序规则
        );
        //DEBUG 列表数据返回结构
        /*
        $page_result = array(
            //int 返回结果总数
            'total_rows' => $total_rows,
            //array 一页数据数组
            'page_data' => $page_data 
        );
        */
        $page_result = $this->index('news_info_cate',$page_condition);
        //DEBUG 返回信息
        $page_data = array();
        foreach($page_result['page_data'] AS $key => $value){
            switch ($type) {
                case 'table_tree':
                    //获取所有子集数数据
                    $sql = "SELECT * FROM ".DB::table('news_info_cate')." WHERE FIND_IN_SET(".$value['nic_id'].",nic_path) ORDER BY nic_sort desc,nic_id desc";
                    $subtree = DB::fetch_all($sql);
                    $treeList = $this->dbarray2tree($subtree, $value['nic_id']);
                    $value['treeList'] = $treeList; 
                    break;

                case 'xm_select':
                    //获取所有子集数数据
                    $tmp = array(
                        'name'=>$value['nic_name'],
                        'value'=>$value['nic_id'],
                    );
                    $sql = "SELECT nic_id AS value,nic_name AS name,nic_pid AS pid FROM ".DB::table('news_info_cate')." WHERE FIND_IN_SET(".$value['nic_id'].",nic_path) ORDER BY nic_sort desc,nic_id desc";
                    $subtree = DB::fetch_all($sql);
                    $treeList = $this->dbarray2tree($subtree, $tmp['value'],'value', 'pid', 'children');
                    if(!empty($treeList)){
                        $tmp['children'] = $treeList;
                    }
                    $value = $tmp;
                    break;
            }
            $page_data[] = $value;
        }
        $page_result['page_data'] = $page_data;
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        if($api && $api !='html' && $api !='xls'){
            $return['code'] = 0;
            $return['msg'] = '操作成功';
            $return['count'] = (int)$page_result['total_rows'];
            $return['data'] = $page_result['page_data'];
            @header("Content-type: application/json; charset=utf-8");
            $return_string = json_encode($return);
            echo $return_string;die();
        }elseif($api =='xls'){
            $format_return['code'] = '1';
            $format_return['data'] = $page_result;
            require SITE_ROOT.'./source/lib/excel/class_excel.php';
            $format_return['data']['table_name'] =$_G['gp_mod'].'_'.$_G['gp_action'];
            $format_return['data']['table_title'] = get_table_title($format_return['data']['table_name']);
            $format_return['data']['table_structure'] = get_table_structure($_G['gp_mod'], $_G['gp_action']);
            $excel_file_name = $excel_sheet_title = $format_return['data']["table_structure"]["table_brief"]["table_title"];
            $excel_data[] = array($format_return['data']['table_name']);
            $excel_data[] = array($format_return['data']['table_structure']['table_brief']['table_title']);
            $excel_data[] = array("填写说明:数据从第6行开始填写,批量新增数据第一列(#)留空");
            //去除不到处字段 开始
            unset($format_return['data']['table_title']['不导出的字段名称']);
            //去除不到处字段 结束
            $excel_data[] = $table_key =  array_keys($format_return['data']['table_title']);
            $excel_data[] = array_values($format_return['data']['table_title']);
            $trim_field = array('身份证等需要加空格的字段');
            foreach ($format_return['data']['page_data'] AS $key => $value) {
                $tmp = array();
                foreach ($format_return['data']['table_title'] AS $k => $v) {
                    if(!empty($format_return['data']['table_structure']['field'][$k]['option'])){
                        $tmp2 = array();
                        foreach ($format_return['data']['table_structure']['field'][$k]['option'] AS $kk => $vv) {
                            $v_array = explode(',', $value[$k]);
                            foreach ($v_array AS $kkk => $vvv) {
                                if ($vv['v'] == $vvv) {
                                    $tmp2[$kkk] = $vv['n'];
                                }
                            }
                        }
                        $value[$k]= implode(' ', $tmp2);
                    }
                    if(isset($format_return['data']['table_title'][$k])){
                        if(in_array($k,$trim_field)){
                            $tmp[$k]=" ".$value[$k];
                        }else{
                            $tmp[$k]=$value[$k];
                        }   
                    }
                }
                $excel_data[] = $tmp;
            }
            $excel = new excel($excel_file_name, $excel_sheet_title, $excel_data,'Excel5',1,'',2);
            $excel->output_excel();die;
        }
    }
    
    public function dbarray2tree($tree, $rootId = 0, $idname = 'nic_id', $pidname = 'nic_pid', $subname = 'treeList') {
        $return = array();
        foreach ($tree as $leaf) {
            if ($leaf[$pidname] == $rootId) {
                foreach ($tree as $subleaf) {
                    if ($subleaf[$pidname] == $leaf[$idname]) {
                        $leaf[$subname] = $this->dbarray2tree($tree, $leaf[$idname],$idname,$pidname,$subname);
                        break;
                    }
                }
                $return[] = $leaf;
            }
        }
        return $return;
    }
    
    
    /**
     * 获取一条数据信息
     * @author yangw <2441069162@qq.com>
     * @copyright (c) 2017-11-06 api.bozedu.net $
     * @version 1.0
     * HTTP POST/GET int $key_id 获取数据主键编号 必填
     * HTTP POST/GET string api 如果是 json(小写) 返回JSON数据 如果为空或其他加载网页模版显示页面 选填
     */
    public function do_home() {
        global $_G;
        $home_info = array(
            'slider'=>array(),
            'content'=>array(),
            'recommend'=>array()
        );
        $slider = $content = $recommend = array();
        //获取首页推荐栏目模块
        $sql = "SELECT nic_id,nic_name FROM ".DB::table('news_info_cate')." WHERE nic_position = 'home' ORDER BY nic_sort DESC LIMIT 8";
        $recommend = DB::fetch_all($sql);
        $home_info['recommend'] = $recommend;
        $sql = "SELECT * FROM ".DB::table('news_info_cate')."";
        $result = DB::fetch_all($sql);
        $result = $this->dbarray2tree($result, 0);
        foreach ($result as $tkey => $tvalue) {
            $home_info['content']['nic_id'.$tvalue['nic_id']]['name'] = $tvalue['nic_name'];
            $home_info['content']['nic_id'.$tvalue['nic_id']]['data'] = array();
            $tcontent = array();
            foreach ($tvalue['treeList'] as $key => $value) {
                $s = "SELECT * FROM ".DB::table('news_info')." WHERE FIND_IN_SET(".$value['nic_id'].",nic_id) ORDER BY ni_id DESC LIMIT 8";
                $r = DB::fetch_all($s);
                foreach ($r as $k => $v) {
                    if($v){
                        $in_slider = $v['ni_img'];
                        $v['ni_img'] = file_url($v['ni_img']);
                        $v['create_dateline_format'] = date('m-d',$v['create_dateline']);
                        $v['create_dateline_format2'] = date('Y-m-d',$v['create_dateline']);
                        //格式化详情信息
                        $vinfo = array(
                            "id"=> $v['ni_id'],//信息编号
                            "tag_name"=> $v['ni_tag'],
                            "name"=> $v['ni_title'],
                            "img"=> $v['ni_img'],
                            "fbr"=> $v['ni_fbr'],
                            "create_dateline_format"=> $v['create_dateline_format'],
                            "create_dateline_format2"=> $v['create_dateline_format2'],
                            "icon"=> "",
                            "cate"=> "1",
                            "type"=> "1",
                            "action"=> "",
                            "do"=> "",
                            "url"=> ""
                        );
                        //加入返回数组
                        if($in_slider){
                            $home_info['slider'][] = $vinfo;
                        }
                        $tcontent['nic_id'.$value['nic_id']]['name'] = $value['nic_name'];
                        $tcontent['nic_id'.$value['nic_id']]['data'][] = $vinfo;
                    }
                }
            }
            $home_info['content']['nic_id'.$tvalue['nic_id']]['data'][] = $tcontent;
        }
        $api = isset($_REQUEST['api']) ? $_REQUEST['api'] : '';
        $return['template']['hidden_header'] = 1;
        $return['template']['hidden_left'] = 1;
        $return['template']['hidden_footer'] = 1;
        $return['template']['hidden_user_menu'] = 1;
        $return['template']['hidden_logo_link'] = 1;
        //DEBUG 调试输出
        if($api){
            $return['code'] = '1';//1表示成功 其他为错误编码
            $return['data']['home_info'] = $home_info;
            echo fast_format_data($return);
        }else{
            include template('news/info/detail');
        }
    }
}
?>