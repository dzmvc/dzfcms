<?php
include_once '../../dz_framework/init.php';
// ---------------------------引入接口参数类(以用户实际路径为准)---------------------------------
include_once ('com/umeng/uapp/param/UmengUappGetTodayDataParam.class.php');
include_once ('com/umeng/uapp/param/UmengUappGetTodayDataResult.class.php');
// ---------------------------引入SDK工具类(以用户实际路径为准)---------------------------------
include_once ('com/alibaba/openapi/client/policy/RequestPolicy.class.php');
include_once ('com/alibaba/openapi/client/entity/ByteArray.class.php');
include_once ('com/alibaba/openapi/client/util/DateUtil.class.php');
include_once ('com/alibaba/openapi/client/policy/ClientPolicy.class.php');
include_once ('com/alibaba/openapi/client/policy/ClientPolicy.class.php');
include_once ('com/alibaba/openapi/client/APIRequest.class.php');
include_once ('com/alibaba/openapi/client/APIId.class.php');
include_once ('com/alibaba/openapi/client/SyncAPIClient.class.php');
// ---------------------------example start---------------------------------
try {
    // ---------------------------example start---------------------------------
    // 请替换第一个参数apiKey和第二个参数apiSecurity
    $clientPolicy = new ClientPolicy ('8678675', 'FwfQSVdNPx', 'gateway.open.umeng.com');
    $syncAPIClient = new SyncAPIClient ( $clientPolicy );
    $reqPolicy = new RequestPolicy ();
    $reqPolicy->httpMethod = "POST";
    $reqPolicy->needAuthorization = true;
    $reqPolicy->requestSendTimestamp = true;
    // 测试环境只支持http
    // $reqPolicy->useHttps = false;
    $reqPolicy->useHttps = true;
    $reqPolicy->useSignture = true;
    $reqPolicy->accessPrivateApi = false;
    // --------------------------构造参数 - 苹果版 ----------------------------------
    $param = new UmengUappGetTodayDataParam();
    $param->setAppkey("5e3a68e20cafb26361000485");
    //青岛教育云空间安卓版 AppKey：5e3a2875570df368a900013b
    //青岛教育云空间苹果版 AppKey：5e3a68e20cafb26361000485
    //桐乡网校 安卓版本 AppKey:59de1575a40fa32c57000fa4
    //桐乡网校 苹果版本 AppKey:59dae03da40fa30a4e00002a
    // --------------------------构造请求 - 苹果版 ----------------------------------
    $request = new APIRequest ();
    $apiId = new APIId ("com.umeng.uapp", "umeng.uapp.getTodayData", 1 );
    $request->apiId = $apiId;
    $request->requestEntity = $param;
    // --------------------------构造结果 - 苹果版 ----------------------------------
    $result = new UmengUappGetTodayDataResult();
    $return = $syncAPIClient->send($request, $result, $reqPolicy);
    $return_i = $return = (array)$return;
    $return_i = json_encode($return);
    $return_i = json_decode($return_i,true);
    foreach($return_i AS $key => $value){
        if($value['todayData']){
            $return_i_todayData = $value['todayData'];
        }
    }
    //echo json_encode($return);
    // --------------------------构造参数 - 安卓版 ----------------------------------
    $param = new UmengUappGetTodayDataParam();
    $param->setAppkey("5e3a2875570df368a900013b");
    //青岛教育云空间安卓版 AppKey：5e3a2875570df368a900013b
    //青岛教育云空间苹果版 AppKey：5e3a68e20cafb26361000485
    //桐乡网校 安卓版本 AppKey:59de1575a40fa32c57000fa4
    //桐乡网校 苹果版本 AppKey:59dae03da40fa30a4e00002a
    // --------------------------构造请求 - 安卓版 ----------------------------------
    $request = new APIRequest ();
    $apiId = new APIId ("com.umeng.uapp", "umeng.uapp.getTodayData", 1 );
    $request->apiId = $apiId;
    $request->requestEntity = $param;
    // --------------------------构造结果 - 安卓版 ----------------------------------
    $result = new UmengUappGetTodayDataResult();
    $return = $syncAPIClient->send($request, $result, $reqPolicy);
    //var_dump($return);
    $return_a = $return = (array)$return;
    $return_a = json_encode($return);
    $return_a = json_decode($return_a,true);
    foreach($return_a AS $key => $value){
        if($value['todayData']){
            $return_a_todayData = $value['todayData'];
        }
    }
    var_dump($return_i_todayData);
    var_dump($return_a_todayData);
    echo $date = $return_i_todayData['date'];
    echo $qc_student_login_app = $return_i_todayData['totalUsers']+$return_a_todayData['totalUsers'];
    // ----------------------------example end-------------------------------------
} catch ( OceanException $ex ) {
    echo "Exception occured with code[";
    echo $ex->getErrorCode ();
    echo "] message [";
    echo $ex->getMessage ();
    echo "].";
}
